//
//  ImageFunc.cpp
//  odometry code 3
//
//  Created by Himani Arora on 13/08/15.
//  Copyright (c) 2015 Himani Arora. All rights reserved.
//

#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>

#include <cstdio>
#include<ctime>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
#include <sstream>

#include "UserDefinedFunc.h"
#include "IterationFunc.h"
#include "ExternVariable.h"
#include "DisplayFunc.h"
#include "Pyramid.h"
#include "ImageFunc.h"

using namespace std;
using namespace cv;


vector<float> GetImagePoseEstimate(frame* prev_frame, frame* current_frame, int frame_num,depthMap* currDepthMap,frame* tminus1_prev_frame,bool homo)
{
    
    //Mat rotFrmHomo=performHomography(prev_frame, current_frame);
    
    PRINTF("\nCalculating Image Pose Estimate using current frame: %d and previous frame: %d", current_frame->frameId ,prev_frame->frameId);
    
    float pose[6];
    pose[0]=0.0f;
    pose[1]=0.0f;
    pose[2]=0.0f;
    pose[3]=0.0f;
    pose[4]=0.0f;
    pose[5]=0.0f;
    
    //initializing flags for image display
    int display_initial_img_flag=0;
    int display_orig_img_flag=0;
    
    int iter_counter;
  //*******ENTER PYRAMID LOOP*******//

for (int level_counter=(util::MAX_PYRAMID_LEVEL-1); level_counter>=0; level_counter--)
{
    
    //*******PRECOMPUTATION BEGINS*******//
    
    
    //printf("\nPyramid level: %d",level_counter);
    
    prev_frame->updationOnPyrChange(level_counter);
    current_frame->updationOnPyrChange(level_counter,false);

    Pyramid workingPyramid(prev_frame, current_frame, pose,currDepthMap);
    
    workingPyramid.putPreviousPose(tminus1_prev_frame);
    workingPyramid.pose=pose;
    
    //Perform precomputation--Calculation of steepest descent,hessian inverse & world points.
    workingPyramid.performPrecomputation();
    
    display_orig_img_flag=0; //reset flag
    
    
    //*******ENTER ITERATION LOOP*******//
    
    for(iter_counter=0; iter_counter<util::MAX_ITER;  ++iter_counter)
    {
        
        //Perform iteration steps-- Calculation of warped points,warped image & residual. Also updates pose and checks condition for loop termination.
        float check_termination=workingPyramid.performIterationSteps();
        
        if(workingPyramid.weightedPose<1.0f)iter_counter=util::MAX_ITER-1;
        
        //PRINTF("\nupdated pose: %f , %f , %f , %f , %f , %f ",pose[0],pose[1],pose[2],pose[3],pose[4],pose[5]);
        
        //to display current iteration residual
        
       if(util::FLAG_DISPLAY_IMAGES)
       {
        DisplayIterationRes(prev_frame, workingPyramid.residual, "Iteration Residual", frame_num,homo);
        DisplayWeights(prev_frame, workingPyramid.weights, "Weight Image", frame_num);
         
         //to display original image residual and original image
         if(display_initial_img_flag==0)
         {
             //display initial residual
             DisplayInitialRes(current_frame,prev_frame,"Initial Residual", frame_num,homo);
             
             display_initial_img_flag=1; //set flag
         }
        
        
        //diplay original image
        if(display_orig_img_flag==0)
        {
            DisplayOriginalImg(workingPyramid.saveImg,prev_frame,"Original Image", frame_num,homo);
            display_orig_img_flag=1; //set flag
        }
        
        
         //to display warped image
         DisplayWarpedImg(workingPyramid.warpedImage, prev_frame, "Warped Image", frame_num,homo);
        
       }
    
         //waitKey(0);
    
    
    } //exit iteration loop
    

    PRINTF("\nSummary: For previous frameId: %d, current frameId: %d ", prev_frame->frameId, current_frame->frameId);
    PRINTF("\nAt Pyramid Level: %d , Max Iteration: %d ",level_counter,iter_counter+1);
    
} //exit pyramid loop

    PRINTF("\nUpdated pose: %f ,%f , %f , %f , %f , %f ",pose[0],pose[1],pose[2],pose[3],pose[4],pose[5]);
    
    current_frame->calculatePoseWrtOrigin(prev_frame,pose);
    current_frame->calculatePoseWrtWorld(prev_frame,pose);
    current_frame->calculateRandT();
    
    
    PRINTF("\nExiting frame loop..");
    vector<float> posevec(pose, pose+6);
    
    return posevec;
    
}

