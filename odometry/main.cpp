 
#include <cstdio>
#include<ctime>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>

#include "DepthPropagation.h"
#include "DisplayFunc.h"
#include "ImageFunc.h"
#include "ExternVariable.h"
#include "Frame.h"
#include "EigenInitialization.h"

using namespace std;
using namespace cv;

ifstream my_file;
ofstream pose_file;

int main(int argc, char *argv[])
{
    clock_t start, end;
    
    //add path to samplefile.txt
    my_file.open("/Users/himaniarora/Desktop/samplefile.txt");
    
    vector<frame*> frameptr_vector;// vector of pointers to each frame currently in memory
    
    string vid_path;
    getline(my_file,  vid_path);
    string pose_path;
    getline(my_file,  pose_path);
    pose_file.open(pose_path);
    string depthmap_path;
    getline(my_file,  depthmap_path);
    
    //*******VIDEO INITIALIZATIONS*******//
    VideoCapture bgr_capture(vid_path);
    
    //check if successful
    if (!bgr_capture.isOpened()) // check if we succeeded with BGR
    { printf("\nUnable to open Frame File!");
        return -1;
       
    }
    
    
    //*******PARAMETER INITIALIZATIONS*******//
    
    //get no of frames in video , no of rows and no of columns
    float no_of_frames = bgr_capture.get(CV_CAP_PROP_FRAME_COUNT); //get the number of frames in depth
    
    vector<float>pose_vec;
    
    printf("\nNo of frames:%f",no_of_frames);
    
    frame* activeKeyFrame=NULL;
        
    depthMap* currentDepthMap=new depthMap();
    bool switch_std_dev=false;
    
    //*******ENTER FRAME LOOP*******//
    
    for (int frame_counter = 1; frame_counter <= no_of_frames; frame_counter++)
    {
       // start = clock();
        frameptr_vector.push_back(new frame(bgr_capture));
    
        // to bootstrap the first image in the sequence
        if(frame_counter==1)
        {
            
           activeKeyFrame=frameptr_vector.back();
           currentDepthMap->formDepthMap(frameptr_vector.back());
           currentDepthMap->updateDepthImage();
            continue;
        }
        

        pose_vec=GetImagePoseEstimate(activeKeyFrame, frameptr_vector.back(), frame_counter, currentDepthMap, frameptr_vector[frameptr_vector.size()-2]);
        
        
        // PUSH VALUES IN HISTOGRAM TILL ITS COMPLETE
        float seeds_num= currentDepthMap->calculate_no_of_Seeds();
        
        if (pose_file.is_open() && util::FLAG_WRITE_POSE)
        {
            pose_file<<frameptr_vector.back()->frameId<<" "<<frameptr_vector.back()->poseWrtWorld[0]<<" "<<frameptr_vector.back()->poseWrtWorld[1]<<" "<<frameptr_vector.back()->poseWrtWorld[2]<<" "<<frameptr_vector.back()->poseWrtWorld[3]<<" "<<frameptr_vector.back()->poseWrtWorld[4]<<" "<<frameptr_vector.back()->poseWrtWorld[5]<<" "<<activeKeyFrame->rescaleFactor<<" "<<seeds_num<<"\n";
            
        }
            
        
        cout<<"\n"<<frameptr_vector.back()->frameId<<" "<<frameptr_vector.back()->poseWrtWorld[0]<<" "<<frameptr_vector.back()->poseWrtWorld[1]<<" "<<frameptr_vector.back()->poseWrtWorld[2]<<" "<<frameptr_vector.back()->poseWrtWorld[3]<<" "<<frameptr_vector.back()->poseWrtWorld[4]<<" "<<frameptr_vector.back()->poseWrtWorld[5]<<" "<<activeKeyFrame->rescaleFactor<<" "<<seeds_num;
        
        PRINTF("\n\n\nDEPTH MAP:");
        currentDepthMap->formDepthMap(frameptr_vector.back());
        
        //// Checking for keyframe propagation
        if (frame_counter%util::KEYFRAME_SWITCH_INTERVAL==0)
        {
            //printf("\nSwitching to new keyframe with id: %d",frameptr_vector.back()->frameId);
            currentDepthMap->finaliseKeyframe();
            currentDepthMap->createKeyFrame(frameptr_vector.back());
            activeKeyFrame=frameptr_vector.back();
            unsigned long size=frameptr_vector.size();
            for (unsigned long i = 0; i < size-1; ++i) {
                delete frameptr_vector  [i]; // Calls ~object and deallocates *tmp[i]
            }
            frameptr_vector.erase(frameptr_vector.begin(),frameptr_vector.begin()+size-1);
            continue;
            //waitKey(0);
        }
        currentDepthMap->updateKeyFrame();
        currentDepthMap->observeDepthRow();
        currentDepthMap->doRegularization();
        currentDepthMap->updateDepthImage();
        
        end = clock();
        
        //cout <<"\nTime: "<<float( end - start) / CLOCKS_PER_SEC<<endl;

    } //exit frame loop
    return 0;
} //exit main








