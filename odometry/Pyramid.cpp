//
//  Pyramid.cpp
//  odometry
//
//  Created by Himanshu Aggarwal on 9/4/15.
//  Copyright (c) 2015 Himanshu Aggarwal. All rights reserved.
//

#include "Pyramid.h"
#include "UserDefinedFunc.h"
#include "ExternVariable.h"
#include "DisplayFunc.h"

using namespace cv;

Pyramid::Pyramid(frame* prevframe,frame* currentframe,float* pose,depthMap* currDepthMap){

    currentDepthMap=currDepthMap;
    prev_frame=prevframe;
    current_frame=currentframe;
    //this->pose=pose;
    steepestDescent=Mat(prev_frame->no_nonZeroDepthPts,6,CV_32FC1);//steepest descent matrix
    saveImg= Mat(1, prev_frame->no_nonZeroDepthPts, CV_32FC1); //save intensity of image at non zero depth points for current pyramid level
    worldPoints=Mat(3,prev_frame->no_nonZeroDepthPts,CV_32FC1); //saves world points to be warped with estimate of e
    transformedWorldPoints=Mat(3,prev_frame->no_nonZeroDepthPts,CV_32FC1); //saves world points to be warped with estimate of e
    warpedPoints= Mat(2,prev_frame->no_nonZeroDepthPts, CV_32FC1);//stores warped points
    warpedImage=Mat::zeros(1,prev_frame->no_nonZeroDepthPts,CV_32FC1);
    warpedGradientx=Mat(1,prev_frame->no_nonZeroDepthPts, CV_32FC1);//gradientx for current img
    warpedGradienty=Mat(1,prev_frame->no_nonZeroDepthPts, CV_32FC1);//gradienty for current img
    //weight_diag=Mat::zeros(prevframe->no_nonZeroDepthPts, prev_frame->no_nonZeroDepthPts, CV_32FC1); //diag matrix of wts.
    weights=Mat(1,prev_frame->no_nonZeroDepthPts, CV_32FC1);//weights for residual
   
    covarianceDiagonalWts[0]=100.0f; //w1
    covarianceDiagonalWts[1]=100.0f;
    covarianceDiagonalWts[2]=100.0f;
    covarianceDiagonalWts[3]=0.01f; //v1
    covarianceDiagonalWts[4]=0.01f;
    covarianceDiagonalWts[5]=0.01f;
    calCovarianceMatrixInv(covarianceDiagonalWts);
    
    

}

/*
 FUNCTION: CALCULATE STEEPEST DESCENT
 
 PURPOSE: RETURNS STEEPEST DESCENT MATRIX FOR A FRAME AT A PARTICULAR PYRAMID LEVEL AND POPULATES WORLD POINT MATRIX WITH IMAGE COORDINATES         AND DEPTH
 INPUTS: DEPTH MATRIX, MASK OF DEPTH MATRIX, GRADIENT IN X DIRECTION OF FRAME, GRADIENT IN Y DIRECTION OF FRAME, WORLD POINT MATRIX
 NUMBER OF ROWS, NUMBER OF COLUMNS, PYRAMID LEVEL
 OUTPUT: STEEPEST DESCENT MATRIX
 */

void Pyramid::calculateSteepestDescent()
{
    //printf("\nCalculating Steepest Descent for frame: %d ", prev_frame->frameId);
    int nRows=prev_frame->currentRows;
    int nCols=prev_frame->currentCols;
    
    //*******INITIALIZE VAR*******//
    
    Mat jacobian_top(prev_frame->no_nonZeroDepthPts,6, CV_32FC1);
    Mat jacobian_bottom(prev_frame->no_nonZeroDepthPts,6, CV_32FC1);
    
    //cout<<"\n\n\nGRADIENT POINTER:  "<<(gradx.type()==CV_32FC1);
    
    
    //calculate resized intrinsic focal parameters for pyrlevel
    vector<float> resized_intrinsic= GetIntrinsic(prev_frame->pyrLevel);
    float resized_fx=resized_intrinsic[0];
    float resized_fy=resized_intrinsic[1];
    float resized_cx=resized_intrinsic[2];
    float resized_cy=resized_intrinsic[3];
    
    
    //*******INITIALIZE POINTERS*******//
    
    //pointers to access elements
    float* depth_ptr;
    float* jacob_top_ptr;
    float* jacob_bottom_ptr;
    uchar* mask_ptr;
    
    
    int jac_rows;
    //check if matrix stored continuously
    if(jacobian_top.isContinuous() & jacobian_bottom.isContinuous())
        jac_rows= -1;
    else
        jac_rows= 0;
    
    int jac_counter =0;
    
    jacob_top_ptr = jacobian_top.ptr<float>(0);
    jacob_bottom_ptr = jacobian_bottom.ptr<float>(0);
    
    //*******LOOP TO CALCULATE JACOBIAN*******//
    
    int m,n; //loop variables
    float i,j;
    int idx=0;
    float* x_warped=warpedPoints.ptr<float>(0);
    float* y_warped=warpedPoints.ptr<float>(1);
    float* gradxwarp=warpedGradientx.ptr<float>(0);
    float* gradywarp=warpedGradienty.ptr<float>(0);
    float* warpedimg_ptr=warpedImage.ptr<float>(0);
    depth_ptr=transformedWorldPoints.ptr<float>(2); //Z

    float gradx,grady;
   // for( m = 0; m < nRows; ++m)
   // {
    for(m=0;m<prev_frame->no_nonZeroDepthPts;m++)
    {
        
    {
        
            i=y_warped[idx];
            j=x_warped[idx];
            
            gradx=current_frame->getInterpolatedElement(x_warped[idx], y_warped[idx], "gradx");
            grady=current_frame->getInterpolatedElement(x_warped[idx], y_warped[idx], "grady");
            
            gradxwarp[idx]=gradx;
            gradywarp[idx]=grady;
            
            jacob_bottom_ptr[jac_counter]  = grady*(-(resized_fy + (pow((-resized_cy + i),2) / resized_fy)));
            jacob_top_ptr[jac_counter++] = gradx*(-((-resized_cy + i)*(-resized_cx + j)) / resized_fy);
            
            jacob_bottom_ptr[jac_counter]=grady*(((-resized_cy + i)*(-resized_cx + j)) / resized_fx);
            jacob_top_ptr[jac_counter++] = gradx*(resized_fx + (pow((-resized_cx + j),2) / resized_fx));
            
            jacob_bottom_ptr[jac_counter]=grady*((resized_fy*(-resized_cx + j)) / resized_fx);
            jacob_top_ptr[jac_counter++]  = gradx*(-(resized_fx*(-resized_cy + i) / resized_fy));
            
            jacob_bottom_ptr[jac_counter]=0;
            jacob_top_ptr[jac_counter++]  =gradx*( resized_fx*(pow(depth_ptr[idx],-1)));
            
            jacob_bottom_ptr[jac_counter]= grady*(resized_fy*(pow(depth_ptr[idx],-1)));
            jacob_top_ptr[jac_counter++]  = 0;
            
            jacob_bottom_ptr[jac_counter]= grady*(-(-resized_cy + i) *(pow(depth_ptr[idx],-1)));
            jacob_top_ptr[jac_counter++]  = gradx*(-(-resized_cx + j) *(pow(depth_ptr[idx],-1)));
            
            
        }

            if(jac_rows>-1)
            {   jac_rows=jac_rows+1;
                jacob_bottom_ptr=jacobian_bottom.ptr<float>(jac_rows);
                jacob_top_ptr=jacobian_top.ptr<float>(jac_rows);
                jac_counter=0;
            }
            idx++;
        
    }
   
    //*******CALCULATE STEEPEST DESCENT*******//
    steepestDescent=jacobian_top+jacobian_bottom;
    
    return;
    
}



/*
 FUNCTION: CALCULATE HESSIAN INVERSE
 
 PURPOSE: RETURNS HESSIAN INVERSE MATRIX FOR A FRAME
 INPUTS: STEEPEST DESCENT  MATRIX
 OUTPUTS: HESSIAN INVERSE MATRIX
 */

void Pyramid::calculateHessianInv()
{
    //printf("\nCalculating Hessian ");
    float* weight_ptr=weights.ptr<float>(0);
    
    Mat temp= steepestDescent.t();
    
    weight_ptr=weights.ptr<float>(0);
    float* temp_ptr0=temp.ptr<float>(0);
    float* temp_ptr1=temp.ptr<float>(1);
    float* temp_ptr2=temp.ptr<float>(2);
    float* temp_ptr3=temp.ptr<float>(3);
    float* temp_ptr4=temp.ptr<float>(4);
    float* temp_ptr5=temp.ptr<float>(5);
    
    for(int y=0; y<prev_frame->no_nonZeroDepthPts; y++)
    {
       
    
        temp_ptr0[y]=temp_ptr0[y]*weight_ptr[y];
        temp_ptr1[y]=temp_ptr1[y]*weight_ptr[y];
        temp_ptr2[y]=temp_ptr2[y]*weight_ptr[y];
        temp_ptr3[y]=temp_ptr3[y]*weight_ptr[y];
        temp_ptr4[y]=temp_ptr4[y]*weight_ptr[y];
        temp_ptr5[y]=temp_ptr5[y]*weight_ptr[y];
        
        if(isnan(float(weight_ptr[y] )))
        {
            cout<<"\nweight nan: "<<weight_ptr[y];
        }
        
        
    }
    Mat hessian = Mat::zeros(6, 6, CV_32FC1);
    Mat new_temp=temp.t();
    
    float* new_temp_ptr=new_temp.ptr<float>(0);
    
    //float* temp_ptr = temp.ptr<float>(0);
    float* steep_ptr=steepestDescent.ptr<float>(0);
    for(int y=0; y<prev_frame->no_nonZeroDepthPts; y++ )
    {
        new_temp_ptr = new_temp.ptr<float>(y);
        steep_ptr=steepestDescent.ptr<float>(y);
        Mat temp_T= (Mat_<float>(6,1)<<new_temp_ptr[0], new_temp_ptr[1],new_temp_ptr[2], new_temp_ptr[3], new_temp_ptr[4], new_temp_ptr[5] );
        Mat steep_T=(Mat_<float>(1,6)<<steep_ptr[0], steep_ptr[1], steep_ptr[2], steep_ptr[3], steep_ptr[4], steep_ptr[5]);
        hessian+= ((temp_T*steep_T));//+ covarianceMatrixInv);
       // hessian+=covarianceMatrixInv;
        
    }
    
    
    //cout<<"\nhessian: \n"<<hessian;
   
    hessianInv=hessian.inv();
    
    return;
    
}

void Pyramid::calculateWorldPoints()
{
    //printf("\nCalculating World Points for previous image: %d", prev_frame->frameId);
    int pyrlevel=prev_frame->pyrLevel;
   int nRows=prev_frame->currentRows;
    int nCols=prev_frame->currentCols;
    //*******INITIALIZE POINTERS*******//
    
    //worldpoint pointers
    float* worldpoint_ptr0; //pointer to access row 0
    float* worldpoint_ptr1; //pointer to access row 1
    float* worldpoint_ptr2; //pointer to access row 2
    uchar* img_ptr;
    float* saveimg_ptr;
    uchar* mask_ptr;
    float* depth_ptr;
    
    worldpoint_ptr0=worldPoints.ptr<float>(0); //initialize to row0
    worldpoint_ptr1=worldPoints.ptr<float>(1); //initialize to row1
    worldpoint_ptr2=worldPoints.ptr<float>(2); //initialize to row2
    
    saveimg_ptr=saveImg.ptr<float>(0); //initialize to row 0 (single row Mat)
    
    //*******CALCULATE RESIZED INTRINSIC PARAMETERS*******//
    
    vector<float> resized_intrinsic= GetIntrinsic(prev_frame->pyrLevel);
    float resized_fx=resized_intrinsic[0];
    float resized_fy=resized_intrinsic[1];
    float resized_cx=resized_intrinsic[2];
    float resized_cy=resized_intrinsic[3];
    
    //*******INITIALIZE COUNTERS*******//
    
    int world_rows0=0; //counter to access columns in row 0
    int world_rows1=0; //counter to access columns in row 1
    int world_rows2=0; //counter to access columns in row 2
    int saveimg_row=0; //counter to store in ro0 of saveimg
    
    int i,j;//loop variables
    for(i = 0; i < nRows; ++i)
    {
        
        //get pointer for current row i;
        img_ptr = prev_frame->image_pyramid[pyrlevel].ptr<uchar>(i);
        mask_ptr=prev_frame->mask.ptr<uchar>(i);
        depth_ptr = prev_frame->depth_pyramid[pyrlevel].ptr<float>(i);
        
        for (j = 0; j < nCols; ++j)
            
        {   //check for zero depth point
            if(mask_ptr[j]==0)
                continue;  //skip loop for non zero depth points
            
            //save image intensity at non zero depth point
            saveimg_ptr[saveimg_row++]=img_ptr[j];
            
            //populating world point with homogeneous coordinate ( X ;Y ;depth ; 1)
            worldpoint_ptr0[world_rows0++]=(j-resized_cx)*depth_ptr[j]/resized_fx; //X-coordinate
            worldpoint_ptr1[world_rows1++]=(i-resized_cy)*depth_ptr[j]/resized_fy; //Y-coordinate
            worldpoint_ptr2[world_rows2++]=depth_ptr[j]; //depth of point at (u,v)
            //worldpoint_ptr3[world_rows3++]=1; //1 for making homogeneous
            
        }
    }
    return;
}

/*
 FUNCTION: CALCULATE WARPED POINTS
 
 PURPOSE: CALCULATES THE POSE MATRIX AND WARPED POINTS FOR THE POSE
 INPUT: POSE VECTOR, WORLDPOINT MATRIX, NUMBER OF NON ZERO DEPTH POINTS, PYRAMID LEVEL
 OUTPUT: WARPED POINT MATRIX
 */

void Pyramid::calculateWarpedPoints ()
{
    //printf("\nCalculating warped points for previous frame: %d ", prev_frame->frameId);
    //*******CALCULATE POSE MATRIX *******//
    
    int nonZeroPts=prev_frame->no_nonZeroDepthPts;
    
    //Create matrix in OpenCV
    Mat se3=(Mat_<float>(4, 4) << 0,-pose[2],pose[1],pose[3], pose[2],0,-pose[0],pose[4], -pose[1],pose[0],0,pose[5],0,0,0,0);
    
    // Map the OpenCV matrix with Eigen:
    Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> se3_Eigen(se3.ptr<float>(), se3.rows, se3.cols);
    
    // Take exp in Eigen and store in new Eigen matrix
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> SE3_Eigen = se3_Eigen.exp();
    
    // create an OpenCV Mat header for the Eigen data:
    Mat SE3(4, 4, CV_32FC1, SE3_Eigen.data());
    
    //*******INITIALIZE POINTERS*******//
    
    //warpedpoint pointers
    float* warpedpoint_ptrx; //pointer to access row 0=> x image coordinate
    float* warpedpoint_ptry; //pointer to access row 1=> y image coordinate
    //worldpoint pointers
    float* worldpoint_ptrX; //pointer to access row 0=> X
    float* worldpoint_ptrY; //pointer to access row 1=> Y
    float* worldpoint_ptrZ; //pointer to access row 2=> Z
    float* trfm_worldpoint_ptrX;
    float* trfm_worldpoint_ptrY;
    float* trfm_worldpoint_ptrZ;

    
    warpedpoint_ptrx=warpedPoints.ptr<float>(0); //initialize to row0
    warpedpoint_ptry=warpedPoints.ptr<float>(1); //initialize to row1
    
    worldpoint_ptrX=worldPoints.ptr<float>(0); //initialize to row0
    worldpoint_ptrY=worldPoints.ptr<float>(1); //initialize to row1
    worldpoint_ptrZ=worldPoints.ptr<float>(2); //initialize to row2
    
    trfm_worldpoint_ptrX=transformedWorldPoints.ptr<float>(0); //initialize to row0
    trfm_worldpoint_ptrY=transformedWorldPoints.ptr<float>(1); //initialize to row1
    trfm_worldpoint_ptrZ=transformedWorldPoints.ptr<float>(2); //initialize to row2

    
    //*******CALCULATE RESIZED INTRINSIC PARAMETERS*******//
    
    vector<float> resized_intrinsic= GetIntrinsic(prev_frame->pyrLevel);
    float resized_fx=resized_intrinsic[0];
    float resized_fy=resized_intrinsic[1];
    float resized_cx=resized_intrinsic[2];
    float resized_cy=resized_intrinsic[3];
    
    //*******STORE SE3 PARAMETERS*******//
    
    float* SE3_ptr;
    float SE3_vec[12];  //r11, r12, r13, t1, r21 r22, r23, t2, r31, r32, r33, t3
    int vec_counter=0;
    int i; //loop variables
    
    for(i=0; i<3; ++i)
    {
        SE3_ptr=SE3.ptr<float>(i); //get pointer to first row of SE3
        
        SE3_vec[vec_counter++]=SE3_ptr[0];
        SE3_vec[vec_counter++]=SE3_ptr[1];
        SE3_vec[vec_counter++]=SE3_ptr[2];
        SE3_vec[vec_counter++]=SE3_ptr[3];
        
    }
    
    //enter loop to calculate warped points
    int j;
    
    for( j = 0; j < nonZeroPts; ++j)
    {
        
        
        if (SE3_vec[1]==0)
        {
            trfm_worldpoint_ptrX[j]=((SE3_vec[0]*worldpoint_ptrX[j])+(SE3_vec[1]*worldpoint_ptrY[j])+(SE3_vec[2]*worldpoint_ptrZ[j])+(SE3_vec[3]));
            trfm_worldpoint_ptrY[j]=((SE3_vec[4]*worldpoint_ptrX[j])+(SE3_vec[5]*worldpoint_ptrY[j])+(SE3_vec[6]*worldpoint_ptrZ[j])+(SE3_vec[7]));
            trfm_worldpoint_ptrZ[j]=((SE3_vec[8]*worldpoint_ptrX[j])+(SE3_vec[9]*worldpoint_ptrY[j])+(SE3_vec[10]*worldpoint_ptrZ[j])+(SE3_vec[11]));
            
            trfm_worldpoint_ptrZ[j]=UNZERO(trfm_worldpoint_ptrZ[j]);
            
            warpedpoint_ptrx[j]=((trfm_worldpoint_ptrX[j]/trfm_worldpoint_ptrZ[j])*resized_fx)+resized_cx;
            
            warpedpoint_ptry[j]=((trfm_worldpoint_ptrY[j]/trfm_worldpoint_ptrZ[j])*resized_fy)+resized_cy;
            
        }
        
        else
        {
            trfm_worldpoint_ptrX[j]=(float(SE3_vec[0]*worldpoint_ptrX[j])+float(SE3_vec[1]*worldpoint_ptrY[j])+float(SE3_vec[2]*worldpoint_ptrZ[j])+float(SE3_vec[3]));
            trfm_worldpoint_ptrY[j]=(float((SE3_vec[4]*worldpoint_ptrX[j]))+float((SE3_vec[5]*worldpoint_ptrY[j]))+float((SE3_vec[6]*worldpoint_ptrZ[j]))+float((SE3_vec[7])));
            trfm_worldpoint_ptrZ[j]=(float(SE3_vec[8]*worldpoint_ptrX[j])+float(SE3_vec[9]*worldpoint_ptrY[j])+float(SE3_vec[10]*worldpoint_ptrZ[j])+float(SE3_vec[11]));
            
            trfm_worldpoint_ptrZ[j]=UNZERO(trfm_worldpoint_ptrZ[j]);
            
            warpedpoint_ptrx[j]=float(((trfm_worldpoint_ptrX[j]/trfm_worldpoint_ptrZ[j])*resized_fx)+resized_cx);
            
            warpedpoint_ptry[j]=float(((trfm_worldpoint_ptrY[j]/trfm_worldpoint_ptrZ[j])*resized_fy)+resized_cy);
            

            
        }
        
    }
    
    
    return;
}


/*
 FUNCTION: CALCULATE WARPED IMAGE
 
 PUROSE: CALCULATES THE INTERPOLATED WAARPED IMAGE POINTS
 INPUT: WARPEDPOINT MATRIX, ORIGINAL IMAGE MATRIX, NUMBER OF ROWS, NUMBER OF COLUMNS, NUMBER OF NON ZERO DEPTH POINTS
 OUTPUT: WARPED IMAGE MATRIX
 */

void Pyramid::calculateWarpedImage()
{
    //printf("\nCalculating Warped Image for previous frame: %d, current frame: %d", prev_frame->frameId, current_frame->frameId);
    int nonZeroPts=prev_frame->no_nonZeroDepthPts;
    int pyrlevel=prev_frame->pyrLevel;
    
    
    //*******INITIALIZE POINTERS*******//
    
    //initialize pointers
    uchar* img_ptr0; //pointer0 to access original image
    uchar* img_ptr1; //pointer1 to acess original image
    float* warpedimg_ptr; //pointer to store in warped image
    float* warpedpoint_ptrx; //pointer to acess warped point x => column
    float* warpedpoint_ptry; //pointer to access warped point y=> row
    
    warpedpoint_ptrx=warpedPoints.ptr<float>(0); //initialize to row0 to acess x coordinate
    warpedpoint_ptry=warpedPoints.ptr<float>(1); //initialize to row1 to acess y coordinate
    warpedimg_ptr=warpedImage.ptr<float>(0); //initialize to store in row0 of warped image
    
    
    //*******DECLARE VARIABLES*******//
    
    //cout<<"\n\nWARPED POINTS   "<<warpedpoint;
    
    
    int j; //for loop variables
    float yx[2]; //to store warped points for current itertion
    float wt[2]; //to store weight
    float y,x; //to store x and y coordinate to acess original image
    uchar pixVal1, pixVal2; //to store intensity value
    float interTop, interBtm;
    
    int nCols=prev_frame->currentCols-1; //maximum valid x coordinate
    int nRows=prev_frame->currentRows-1; //maximum valid y coordinate
    
    for(j=0; j<nonZeroPts; j++ )
    {
        int countOutOfBounds=0;
        yx[0]=warpedpoint_ptry[j]; //store current warped point y
        yx[1]=warpedpoint_ptrx[j]; //store warped point x
        
        
        wt[0]=yx[0]-floor(yx[0]); //weight for y
        wt[1]=yx[1]-floor(yx[1]); //weight for x
        
        //Case 1
        y=floor(yx[0]); //floor of y
        x=floor(yx[1]); //floor of x
        
        //cout<<"\n\n"<<int(x);
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        {   pixVal1=0; //if outside boundary pixel value is 0
            countOutOfBounds++;
        }
        else
        {
            img_ptr0=current_frame->image_pyramid[pyrlevel].ptr<uchar>(y); //initialize image pointer0 to row floor(y)
            //cout<<"\nx: "<<x<<" int x"<<int(x);
            //cout<<", img_ptr0[int(x)]:  "<<int(img_ptr0[int(x)]);
            pixVal1=img_ptr0[int(x)]; //move pointer to get value at pixel (floor(x), floor(y))
        }
        
        
        
        x=yx[1]; //warped point x
        
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        {    pixVal2=0; //if outside boundary pixel value is 0
            countOutOfBounds++;
        }
        else
        {
            img_ptr0=current_frame->image_pyramid[pyrlevel].ptr<uchar>(y); //initialize image pointer0 to row floor(y)
            pixVal2=img_ptr0[int(ceil(x))]; //move pointer to get value at pixel (ceil(x), floor(y))
        }
        
        interTop=((1-wt[1])*pixVal1)+(wt[1]*pixVal2);
        
        
        //Case 2
        y=yx[0]; //warped point y
        
        x=floor(yx[1]); //floor of x
        
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        {    pixVal1=0; //if outside boundary pixel value is 0
            countOutOfBounds++;
        }
        else
        {
            img_ptr1=current_frame->image_pyramid[pyrlevel].ptr<uchar>(ceil(y)); //initialize image pointer1 to row ceil(y)
            pixVal1=img_ptr1[int(x)]; //move pointer to get value at pixel (floor(x), ceil(y))
        }
        
        x=yx[1]; //warped point x
        
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        {    pixVal2=0; //if outside boundary pixel value is 0
            countOutOfBounds++;
        }
        else
        {
            img_ptr1=current_frame->image_pyramid[pyrlevel].ptr<uchar>(ceil(y)); //initialize image pointer1 to row ceil(y)
            pixVal2=img_ptr1[int(ceil(x))]; //move pointer to get value at pixel (ceil(x), ceil(y))
        }
        
        if(countOutOfBounds==4)
        {
            warpedimg_ptr[j]=-1.0f;
        }
        else
        {
            interBtm=((1-wt[1])*pixVal1)+(wt[1]*pixVal2);
            warpedimg_ptr[j]=((1-wt[0])*interTop)+(wt[0]*interBtm); //calculate interpolated value to get warped image intensity
        }
        
    }
    
    return;
    
}

/*
 FUNCTION: UPDATE POSE
 
 PURPOSE: CALCULATE RESIDUAL,  UPDATE CURRENT ESTIMATE OF POSE VECTOR AND CHECK CONDITION FOR TERMINATION OF ITERATION LOOP
 INPUT: SAVED ORIGINAL IMAGE OF NON ZERO DEPTH POINTS, WARPED IMAGE , STEEPEST DESCENT MATRIX, POSE POINTER, NUMBER OF NON ZERO DEPTH POINTS,
 WEIGHT POINTER OF POSE PARAMETERS, THRESHOLD VALUE TO TERMINATE LOOP
 OUTPUT: RETURNS -1 IF LOOP NEEDS TO BE TERMINATED OTHERWISE 0
 */

void Pyramid::updatePose()
{
    //printf("\nUpdating pose ");
    
    //printf("\n\nBefore update: %f , %f , %f , %f , %f , %f", pose[0],pose[1],pose[2],pose[3],pose[4],pose[5]);

    Mat temp=residual.mul(weights);

    Mat sd_param= temp*steepestDescent; //calculate steepest descnet parameters

    
    Mat deltapose1= ((hessianInv*(sd_param.t())).t()); //calculate delta pose (change in pose)
    deltapose1=-deltapose1;

    Mat deltapose2=(hessianInv*(motionPrior.t())).t();
    deltapose=deltapose1;
    
    
    float* deltapose1_ptr=deltapose1.ptr<float>(0); //initialize pointer to delta pose
    float* deltapose2_ptr=deltapose2.ptr<float>(0); //initialize pointer to delta pose
    //printf("\n\nDelta Pose1: %f , %f , %f , %f , %f , %f", deltapose1_ptr[0],deltapose1_ptr[1],deltapose1_ptr[2],deltapose1_ptr[3],deltapose1_ptr[4],deltapose1_ptr[5]);
    
     //printf("\nDelta Pose2: %f , %f , %f , %f , %f , %f", deltapose2_ptr[0],deltapose2_ptr[1],deltapose2_ptr[2],deltapose2_ptr[3],deltapose2_ptr[4],deltapose2_ptr[5]);
    
    
    float* deltapose_ptr=deltapose.ptr<float>(0); //initialize pointer to delta pose
    
    //printf("\n\nFinal Delta Pose: %f , %f , %f , %f , %f , %f", deltapose_ptr[0],deltapose_ptr[1],deltapose_ptr[2],deltapose_ptr[3],deltapose_ptr[4],deltapose_ptr[5]);
    
    //cout<<"\nWeighted delta Pose "<<abs(deltapose_ptr[0]*util::weight[0])<<" , "<<abs(deltapose_ptr[1]*util::weight[1])<<" , "<<abs(deltapose_ptr[2]*util::weight[2])<<" , "<<abs(deltapose_ptr[3]*util::weight[3])<<" , "<<abs(deltapose_ptr[4]*util::weight[4])<<" , "<<abs(deltapose_ptr[5]*util::weight[5])<<" , ";
    
    
    //calculate weighted pose value
    float weighted_pose = abs(deltapose_ptr[0]*util::weight[0])+abs(deltapose_ptr[1]*util::weight[1])+abs(deltapose_ptr[2]*util::weight[2])+abs(deltapose_ptr[3]*util::weight[3])+abs(deltapose_ptr[4]*util::weight[4])+abs(deltapose_ptr[5]*util::weight[5]);
    
    //printf("\n\nTotal Weighted Pose: %f ",weighted_pose);
    weightedPose=weighted_pose;
    
    //check condition for termination
    /*if( weighted_pose < util::threshold1)
    {
        PRINTF("\nWeighted pose below threshold! Terminating");
        return -1;  //terminate iteration loop
    }*/
    
    current_frame->concatenateRelativePose(deltapose_ptr, pose, pose);
    
}




float Pyramid::calResidualAndWeights()
{
    //printf("\ncalculating weights and residual");
    vector<float> resized_intrinsic= GetIntrinsic(prev_frame->pyrLevel);
    float resized_fx=resized_intrinsic[0];
    float resized_fy=resized_intrinsic[1];
    float resized_cx=resized_intrinsic[2];
    float resized_cy=resized_intrinsic[3];
    
    float weightedSumOfResidual=0.0f;
    residual=(warpedImage-saveImg); //calculate residual
    
    Mat maskOutOfBounds=warpedImage!=-1.0f;
    //cout<<"\n\n"<<maskOutOfBounds;
    
    uchar* mask_ptr=maskOutOfBounds.ptr<uchar>(0);
    float* residual_ptr=residual.ptr<float>(0);
    
    for(int j=0 ; j<maskOutOfBounds.cols ; j++)
    {
        if(mask_ptr[j]==0)
            residual_ptr[j]=0.0f;
    }

    
    float* trfmpoint_x=transformedWorldPoints.ptr<float>(0);
    float* trfmpoint_y=transformedWorldPoints.ptr<float>(1);
    float* trfmpoint_z=transformedWorldPoints.ptr<float>(2);
    
    uchar* maskptr=prev_frame->mask.ptr<uchar>(0);
    
    float* weightptr=weights.ptr<float>(0);
    float* residualptr=residual.ptr<float>(0);
    float* depth_ptr=prev_frame->depth_pyramid[prev_frame->pyrLevel].ptr<float>(0);
    
    float* gradxwarp=warpedGradientx.ptr<float>(0);
    float* gradywarp=warpedGradienty.ptr<float>(0);
    
    int idx=0;
   
    Mat se3=(Mat_<float>(4, 4) << 0,-pose[2],pose[1],pose[3], pose[2],0,-pose[0],pose[4], -pose[1],pose[0],0,pose[5],0,0,0,0);
    
    // Map the OpenCV matrix with Eigen:
    Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> se3_Eigen(se3.ptr<float>(), se3.rows, se3.cols);
    
    // Take exp in Eigen and store in new Eigen matrix
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> SE3_Eigen = se3_Eigen.exp();
    
    float tx=SE3_Eigen(0,3);
    float ty=SE3_Eigen(1,3);
    float tz=SE3_Eigen(2,3);
    
    int width= prev_frame->currentCols;
    
    float usageCount=0;
    for(int y=0;y<prev_frame->currentRows;y++)
    {
        maskptr=prev_frame->mask.ptr<uchar>(y);
        depth_ptr=prev_frame->depth_pyramid[prev_frame->pyrLevel].ptr<float>(y);
        
        for (int x=0; x<prev_frame->currentCols; x++)
        {
            if(maskptr[x]==0)
            {   //weightptr[idx]=1;
                continue;
            }
            
            float px = trfmpoint_x[idx];	// x'
            float py = trfmpoint_y[idx];	// y'
            float pz = trfmpoint_z[idx];	// z'
            float d = 1.0f/depth_ptr[x];	// d
            float rp = residualptr[idx]; // r_p
            float gx = resized_fx * gradxwarp[idx];	// \delta_x I
            float gy = resized_fy * gradywarp[idx];  // \delta_y I
            float s = 1.0f * (*(currentDepthMap->depthvararrptr[prev_frame->pyrLevel]+x+width*y));	// \sigma_d^2
            // calc dw/dd (first 2 components):
            
            float g0 = (tx * pz - tz * px) / (pz*pz*d);
            float g1 = (ty * pz - tz * py) / (pz*pz*d);
            
            
            // calc w_p
            float drpdd = gx * g0 + gy * g1;	// ommitting the minus
            float w_p = 1.0f / (util::CAMERA_PIXEL_NOISE_2 + s * drpdd * drpdd);
            float weighted_rp = fabs(rp*sqrtf(w_p));
            
            float wh = fabs(weighted_rp < (util::HUBER_D/2) ? 1 : (util::HUBER_D/2) / weighted_rp);
            
            //sumRes += wh * w_p * rp*rp;
            
            weightptr[idx] = wh * w_p;
            
            if (isnan(weightptr[idx]))
            {
               printf("\n\nWeights:%f , wh: %f, wp: %f, weighted_rp: %f, rp: %f, residualptr[idx]: %f, s: %f, drpdd: %f; ",weightptr[idx], wh, w_p, weighted_rp, rp, residualptr[idx], s, drpdd);
            }
            
            
            if(w_p < 0)
            {    printf("\n\n!!!!!!!!!w_p: %f, x =%d, y=%d",w_p, x, y);
               // waitKey(0);
            }
            
            weightedSumOfResidual+=weightptr[idx]*rp*rp;
            
            
            idx++;
            float depthChange = 1.0f/ d*pz;	// if depth becomes larger: pixel becomes "smaller", hence count it less.
            usageCount += depthChange < 1 ? depthChange : 1;
            
        }
        
        
    }
    pointUsage=usageCount/float(prev_frame->no_nonZeroDepthPts);
    
    return weightedSumOfResidual/float(prev_frame->no_nonZeroDepthPts);
}



void Pyramid::performPrecomputation(){
    
    //printf("\nPerforming pre-computation..");
    
    calculateWorldPoints();
    calculateWarpedPoints();
    calculateWarpedImage();
    calculateSteepestDescent();
    lastErr=calResidualAndWeights();

    return;
}

float Pyramid::performIterationSteps(){

    calculateHessianInv();
    calMotionPrior();
    updatePose();
    calculateWarpedPoints();
    calculateWarpedImage();
    calculateSteepestDescent();
    error=calResidualAndWeights();
    float temp=lastErr;
    lastErr=error;
    return error/temp;
}

void Pyramid::putPreviousPose(frame* tminus1_prev_frame)
{
    prevPose=tminus1_prev_frame->poseWrtOrigin;
    


}

void Pyramid::calCovarianceMatrixInv(float* covar_wts)
{
    Mat covarianceMatrix=Mat::zeros(6, 6, CV_32FC1);
    
    float* covarmat_ptr=covarianceMatrix.ptr<float>(0);
    for(int y=0;y<6;y++)
    {
        covarmat_ptr=covarianceMatrix.ptr<float>(y);
        covarmat_ptr[y]=covar_wts[y];
    }

    covarianceMatrixInv=covarianceMatrix.inv();
}

void Pyramid::calMotionPrior()
{
    //printf("\nIn motion prior");
    Mat diffPose=Mat::zeros(1, 6, CV_32FC1);
    float* diffpose_ptr=diffPose.ptr<float>(0);
    
    for(int i=0;i<6; i++)
    {
        diffpose_ptr[i]=prevPose[i]-pose[i];
        //printf("\nprev: %f, current: %f, diff: %f", prevPose[i], pose[i], diffpose_ptr[i]);
    }
    motionPrior=Mat::zeros(1, 6, CV_32FC1);
    
    for(int i=0;i<prev_frame->no_nonZeroDepthPts; i++)
        motionPrior+=(covarianceMatrixInv*(diffPose.t())).t(); //1x6
    
    //cout<<"\n\nMotion Prior: \n "<<motionPrior;
}
                      
                      
                      
                      
                      


