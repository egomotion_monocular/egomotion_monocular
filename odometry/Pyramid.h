//
//  Pyramid.h
//  odometry
//
//  Created by Himanshu Aggarwal on 9/4/15.
//  Copyright (c) 2015 Himanshu Aggarwal. All rights reserved.
//

#ifndef __odometry__Pyramid__
#define __odometry__Pyramid__

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "Frame.h"
#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>

#include <cstdio>
#include<ctime>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "ExternVariable.h"
#include <cmath>
#include "DepthPropagation.h"
#include "DepthHypothesis.h"



using namespace cv;

class Pyramid{
    
public:
    
    int level;
    float *pose;
    float lastErr;
    float error;
    float pointUsage;
    float weightedPose;
    float* prevPose;
    float covarianceDiagonalWts[6];
    
    
    Mat steepestDescent;
    Mat hessianInv;
    Mat worldPoints;
    Mat transformedWorldPoints;
    Mat saveImg;
    Mat warpedPoints;
    Mat warpedImage;
    Mat residual;
    Mat warpedGradientx;
    Mat warpedGradienty;
    Mat weights;
    Mat covarianceMatrixInv;
    Mat motionPrior;
    Mat deltapose;
   // Mat weight_diag;
    
    
    frame* prev_frame;
    frame* current_frame;
    depthMap* currentDepthMap;
    
public:
    
    Pyramid(frame* prevframe,frame* currentframe,float* pose,depthMap* currDepthMap);
    
    void performPrecomputation();
    float performIterationSteps();
    void calculateSteepestDescent();
    void calculateHessianInv();
    void calCovarianceMatrixInv(float* covar_wts);
    void calculateWorldPoints();
    void calculateWarpedPoints();
    void calculateWarpedImage();
    void updatePose();
    void putPreviousPose(frame* tminus1_prev_frame);
    
    float calResidualAndWeights();
    void calMotionPrior();
    
    void calculatePixelWise();
    
};

#endif /* defined(__odometry__Pyramid__) */
