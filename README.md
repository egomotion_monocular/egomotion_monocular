# README #

### What is this repository for? ###

* Camera Pose Estimation for Egocentric Videos 
* Version 1.0
* Demo video on: http://himaniarora.com/pose-estimation/
* Still under construction !!

### How do I get set up? ###

*** Prerequisites ***

* Requires [OpenCV](http://opencv.org/) and [Eigen Library](http://eigen.tuxfamily.org/index.php?title=Main_Page)
* Tested on [Xcode](https://developer.apple.com/xcode/) (Version 6.4)

*** Instructions for Setting Up ***

* Download source code (Folder name: odometry) 
* Change paths in samplefile.txt (Given in repository for reference)
* Set intrinsic camera calibration parameters and image sizes in ExternVariable.h 
* Configure pyramid levels, maximum iterations, keyframe propagation interval if required

*** View Results ***

* View images, save depth map and write estimated pose to text file
* Toggle Flags in ExternVariable.h to display images and save estimated pose
* Color code of Depth map image: Blue->Near, Yellow->Intermediate, Red->Far
* Format of Pose file (Given in repository for reference) : All lie algebra poses are w.r.t first frame   

### Contact ###

* Himanshu Aggarwal - himanshub43gmail.com
* Himani Arora - h94arora@gmail.com