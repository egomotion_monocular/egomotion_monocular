
#include <cstdio>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "ImageClass.h"
#include "UserDefinedFunc.h"

#include "DisplayFunc.h"

namespace ego{
	void DisplayIterationRes(Mat maskimg, Mat residual, int nRows, int nCols, string name)
	{
		Mat displayimg = maskimg.clone();

<<<<<<< HEAD
void DisplayIterationRes(Mat maskimg, Mat residual, int nRows, int nCols, string name, int framenum )
{
    
    Mat displayimg=maskimg.clone();
    
    uchar* displayimg_ptr;
    float* residual_ptr;
    int residual_counter=0;
    
    residual_ptr=residual.ptr<float>(residual_counter);

    
    int i,j;
    for(i=0;i<nRows;++i)
    {
        displayimg_ptr=displayimg.ptr<uchar>(i); //initialize to row 0 (single row Mat)
        
        for(j=0;j<nCols;++j)
        {
            if(displayimg_ptr[j]==0)
                continue;
            
            else
                // cout<<"\n\n"<<residual_ptr[residual_counter]<<" = "<<int((abs(residual_ptr[residual_counter])));
                displayimg_ptr[j]=uchar((abs(residual_ptr[residual_counter++])));
            
        }

    }
   // namedWindow( name, WINDOW_AUTOSIZE );// Create a window for display.
   imshow(name, displayimg);
    
    //save image
    stringstream ss;
    string str = "final_res_";
    string type = ".jpg";
    
    ss<<"/Users/himaniarora/Desktop/Test_images/"<<str<<(framenum-1)<<type;
    string filename = ss.str();
    ss.str("");
    
    imwrite(filename, displayimg);
    
    //waitKey(0);
    
    return;
};




void DisplayInitialRes(Mat prevdepth, Mat currentframe, Mat prevframe ,int nRows, int nCols, string name, int framenum )
{
    
    Mat maskimg(util::ORIG_ROWS,util::ORIG_COLS,CV_8UC1);//stores depth mask used to display original image residual
    maskimg=FormImageMask(prevdepth); //original resolution depth
    
    Mat residual(util::ORIG_ROWS,util::ORIG_COLS,CV_8UC1);
    residual=abs(currentframe-prevframe);
    
    Mat displayimg=maskimg.clone();
    
    uchar* displayimg_ptr;
    uchar* residual_ptr;
    //int residual_counter=0;
    
    //residual_ptr=residual.ptr<float>(residual_counter);
    
    
    int i,j;
    for(i=0;i<nRows;++i)
    {
        displayimg_ptr=displayimg.ptr<uchar>(i); //initialize to row 0 (single row Mat)
        residual_ptr=residual.ptr<uchar>(i);
        
        for(j=0;j<nCols;++j)
        {
            if(displayimg_ptr[j]==0)
                continue;
            
            else
                displayimg_ptr[j]=uchar((abs(residual_ptr[j])));
        }
        
    }
    //namedWindow( name, WINDOW_AUTOSIZE );// Create a window for display.
    imshow(name, displayimg);
    
    //save image
    stringstream ss;
    string str = "initial_res_";
    string type = ".jpg";
    
    ss<<"/Users/himaniarora/Desktop/Test_images/"<<str<<(framenum-1)<<type;
    string filename = ss.str();
    ss.str("");
    
    imwrite(filename, displayimg);
    
    //waitKey(0);
    
    return;
};




void DisplayImg(Mat warpedimg, Mat maskimg, int nRows, int nCols, string name, int framenum)
{
    
    Mat displayimg=maskimg.clone();
    
    uchar* displayimg_ptr;
    float* warpedimg_ptr;
    int warpedimg_counter=0;
    
    warpedimg_ptr=warpedimg.ptr<float>(warpedimg_counter);
    
    
    int i,j;
    for(i=0;i<nRows;++i)
    {
        displayimg_ptr=displayimg.ptr<uchar>(i); //initialize to row 0 (single row Mat)
       // warpedimg_ptr=warpedimg.ptr<uchar>(i);
        
        for(j=0;j<nCols;++j)
        {
            if(displayimg_ptr[j]==0)
                continue;
            
            else
                displayimg_ptr[j]=uchar((abs(warpedimg_ptr[warpedimg_counter++])));
        }
        
    }
    //namedWindow( name, WINDOW_AUTOSIZE );// Create a window for display.
    imshow(name, displayimg);
    
    //save image
    stringstream ss;
    string str = "warped_img_";
    string type = ".jpg";
    
    ss<<"/Users/himaniarora/Desktop/Test_images/"<<str<<(framenum-1)<<type;
    string filename = ss.str();
    ss.str("");
    
    imwrite(filename, displayimg);
    
    //waitKey(0);
    
    return;
    
}
=======
		uchar* displayimg_ptr;
		float* residual_ptr;
		int residual_counter = 0;

		residual_ptr = residual.ptr<float>(residual_counter);


		int i, j;
		for (i = 0; i < nRows; ++i)
		{
			displayimg_ptr = displayimg.ptr<uchar>(i); //initialize to row 0 (single row Mat)

			for (j = 0; j < nCols; ++j)
			{
				if (displayimg_ptr[j] == 0)
					continue;

				else
					// cout<<"\n\n"<<residual_ptr[residual_counter]<<" = "<<int((abs(residual_ptr[residual_counter])));
					displayimg_ptr[j] = uchar((abs(residual_ptr[residual_counter++])));

			}

		}
		// namedWindow( name, WINDOW_AUTOSIZE );// Create a window for display.
		imshow(name, displayimg);

		//waitKey(0);

		return;
	};




	void DisplayInitialRes(Mat prevdepth, Mat currentframe, Mat prevframe, int nRows, int nCols, string name)
	{

		Mat maskimg(util::ORIG_ROWS, util::ORIG_COLS, CV_8UC1);//stores depth mask used to display original image residual
		maskimg = FormImageMask(prevdepth); //original resolution depth

		Mat residual(util::ORIG_ROWS, util::ORIG_COLS, CV_8UC1);
		residual = abs(currentframe - prevframe);

		Mat displayimg = maskimg.clone();

		uchar* displayimg_ptr;
		uchar* residual_ptr;
		//int residual_counter=0;

		//residual_ptr=residual.ptr<float>(residual_counter);


		int i, j;
		for (i = 0; i < nRows; ++i)
		{
			displayimg_ptr = displayimg.ptr<uchar>(i); //initialize to row 0 (single row Mat)
			residual_ptr = residual.ptr<uchar>(i);

			for (j = 0; j < nCols; ++j)
			{
				if (displayimg_ptr[j] == 0)
					continue;

				else
					displayimg_ptr[j] = uchar((abs(residual_ptr[j])));
			}

		}
		//namedWindow( name, WINDOW_AUTOSIZE );// Create a window for display.
		imshow(name, displayimg);

		//waitKey(0);

		return;
	};




	void DisplayWarpedImg(Mat warpedimg, Mat maskimg, int nRows, int nCols, string name)
	{

		Mat displayimg = maskimg.clone();

		uchar* displayimg_ptr;
		float* warpedimg_ptr;
		int warpedimg_counter = 0;

		warpedimg_ptr = warpedimg.ptr<float>(warpedimg_counter);


		int i, j;
		for (i = 0; i < nRows; ++i)
		{
			displayimg_ptr = displayimg.ptr<uchar>(i); //initialize to row 0 (single row Mat)
			// warpedimg_ptr=warpedimg.ptr<uchar>(i);

			for (j = 0; j < nCols; ++j)
			{
				if (displayimg_ptr[j] == 0)
					continue;

				else
					displayimg_ptr[j] = uchar((abs(warpedimg_ptr[warpedimg_counter++])));
			}

		}
		//namedWindow( name, WINDOW_AUTOSIZE );// Create a window for display.
		imshow(name, displayimg);

		//waitKey(0);

		return;

	}
}//end namespace ego
>>>>>>> 750d79c5311be37f104c5ee83e92c12bea64b3d5









