//
//  UserDefinedFunc.h
//  odometry code 3
//
//  Created by Himani Arora on 18/07/15.
//  Copyright (c) 2015 Himani Arora. All rights reserved.
//

#ifndef __odometry_code_3__UserDefinedFunc__
#define __odometry_code_3__UserDefinedFunc__

#include <cstdio>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "ImageClass.h"
#include "ExternVariable.h"


using namespace std;
using namespace cv;

//form image mask
Mat FormImageMask(Mat depth_img);

//Find image gradient in a particular direction
void FindGradient(Mat Source, Mat& dst, char direction);

//returns steepest descent
Mat CalculateSteepestDescent( Mat depth, Mat mask, Mat gradx, Mat grady, int nRows, int nCols, int pyrlevel);

//returns hessian
Mat CalculateHessian(Mat steepest_desc);


//returns intrinsic focal parameters
float* GetIntrinsic(int pyrlevel);















#endif /* defined(__odometry_code_3__UserDefinedFunc__) */
