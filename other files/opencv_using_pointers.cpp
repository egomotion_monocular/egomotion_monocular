#include <cstdio>
#include<ctime>
#include <cstdlib>
#include <Eigen/Dense>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <unsupported/Eigen/MatrixFunctions>

using namespace std;
using namespace cv;
using namespace Eigen;

//*******CONSTANTS*******//

static const int MAX_PYRAMID_LEVEL = 4;
static const int rows=480;
static const int cols=640;

static const float orig_fx = 481.20;
static const float orig_fy = -480.00;
static const float orig_cx = 319.5;
static const float orig_cy = 239.5;


int main(int argc, char *argv[])
{
    clock_t start, end;
    
    
    //*******VIDEO INITIALIZATIONS*******//
    
    //capture BGR and depth videos from file
    VideoCapture bgr_capture("/Users/himaniarora/Desktop/vids/rgb1.avi");// open the BGR video sequence file
    VideoCapture depth_capture("/Users/himaniarora/Desktop/vids/depth1.avi"); // open the associated depth map file
    
    
    //*******CHECKS*******//
    
    //check if successful
    if (!bgr_capture.isOpened()) // check if we succeeded with BGR
        return -1;
    if (!depth_capture.isOpened()) // check if we succeeded with depth
        return -1;
    
    
    //*******PARAMETER INITIALIZATIONS*******//
    
    //get no of frames in video , no of rows and no of columns
    double no_of_frames = depth_capture.get(CV_CAP_PROP_FRAME_COUNT); //get the number of frames in depth
    
    
    
    //*******MATRIX INITIALIZATIONS*******//
    
    //initialize frame and depth matrices
    Mat current_frame(rows, cols, CV_8UC3); //stores current frame,3 channel for BGR, 16 bit unsigned char
    Mat current_depth(rows, cols, CV_8UC1); //stores current depth map,1 channel,16 bit unsigned char
    
    Mat prev_frame(rows, cols, CV_8UC1); //stores prev frame, 1 channel for grayscale, 16 bit unsigned char
    Mat prev_depth(rows, cols, CV_8UC1); //stores prev depth map, 1 channel,  16 bit unsigned char
    
    Mat resized_current_frame[MAX_PYRAMID_LEVEL];
    Mat resized_current_depth[MAX_PYRAMID_LEVEL];
    Mat resized_prev_frame[MAX_PYRAMID_LEVEL];
    Mat resized_prev_depth[MAX_PYRAMID_LEVEL];
    
    
    
    
    //*******WINDOW FOR DISPLAY*******//
    
    //Create window for display
    //namedWindow( "Display video", WINDOW_AUTOSIZE );// Create a window to display grayscale video.
    //namedWindow( "Display depth", WINDOW_AUTOSIZE );// Create a window to display depth sequence.
    
    
    //*******ENTER FRAME LOOP*******//
    
    
    
    for (int frame_counter = 1; frame_counter < 3 ; frame_counter++)
    {    //start = clock();
        
        
        
        //*******INITIAL TASKS*******//
        
        //get current frame
        bgr_capture >> current_frame; // get a new frame from bgr, store in current_frame
        depth_capture >> current_depth; // get a new frame from depth, store in current_depth
        
        
        //convert frame to grayscale
        cvtColor(current_frame, current_frame, CV_BGR2GRAY); //3 channel BGR is converted to single channel gray scale
        cvtColor(current_depth, current_depth, CV_BGR2GRAY); //3 channel BGR is converted to single channel gray scale
        
        
        //*******BOOTSTRAPPING FOR FIRST FRAME*******//
        
        // to bootstrap the first image in the sequence
        if(frame_counter==1)
        {
            //make new copies of current frame and depth
            prev_frame=current_frame.clone();
            prev_depth=current_depth.clone();
            
            //*******IMAGE PYRAMIDS FOR PREV FRAME*******//
            
            resized_prev_frame[0]=prev_frame.clone();
            resized_prev_depth[0]=prev_depth.clone();
            
            pyrDown(resized_prev_frame[0], resized_prev_frame[1]);
            pyrDown(resized_prev_depth[0], resized_prev_depth[1]);
            
            pyrDown(resized_prev_frame[1], resized_prev_frame[2]);
            pyrDown(resized_prev_depth[1], resized_prev_depth[2]);
            
            pyrDown(resized_prev_frame[2], resized_prev_frame[3]);
            pyrDown(resized_prev_depth[2], resized_prev_depth[3]);
            
            continue; //jump to next iteration
        }
        
        
        //*******IMAGE PYRAMIDS FOR CURRENT FRAME*******//
        
        //Level 0 corresponds to highest resolution that is, original resolution, Level 4 is minimum resolution
        
        resized_current_frame[0]=current_frame.clone();
        resized_current_depth[0]=current_depth.clone();
        
        pyrDown(resized_current_frame[0], resized_current_frame[1]);
        pyrDown(resized_current_depth[0], resized_current_depth[1]);
        
        
        pyrDown(resized_current_frame[1], resized_current_frame[2]);
        pyrDown(resized_current_depth[1], resized_current_depth[2]);
        
        
        pyrDown(resized_current_frame[2], resized_current_frame[3]);
        pyrDown(resized_current_depth[2], resized_current_depth[3]);
        
        
        
        //*******ENTER PRECOMPUTATION LOOP*******//
        
        
        for (int level_counter=3; level_counter>=0; level_counter--)
            
        {
            //*******CALCULATE PARAMETERS*******//
            
            //calculate no of ros and cols
            int resized_rows=resized_current_frame[level_counter].rows;
            int resized_cols=resized_current_frame[level_counter].cols;
            
            //calculate maximum no of iterations for a level
            int MAX_ITER = 12 * (6 - level_counter);
            
            //calculate resized  focal length and camera centre
            float resized_fx=orig_fx/pow(2,level_counter);
            float resized_fy=orig_fy/pow(2,level_counter);
            float resized_cx=orig_cx/pow(2,level_counter);
            float resized_cy=orig_cy/pow(2,level_counter);
            
            
            //*******CALCULATE MASK*******//
            
            //Forming image mask where 255 means depth is no zeros, and 0 means depth is also 0
            Mat mask=resized_prev_depth[level_counter]>10;
            
            
            //*******CALCULATE GRADIENT*******//
            
            Mat grad_x, grad_y;
            
            // Gradient X
            //Sobel(src, dst, ddepth=16S, dx=1, dy=0, ksize=1 (for no smoothing), scale=1,delta=0, borderType=BORDER_DEFAULT )¶
            Sobel( resized_current_frame[level_counter], grad_x, CV_16S, 1, 0, 1, 1, 0, BORDER_DEFAULT );
            
            //Gradient Y
            //Sobel(src, dst, ddepth=16S, dx=0, dy=1, ksize=1 (for no smoothing), scale=1,delta=0, borderType=BORDER_DEFAULT )¶
            Sobel( resized_current_frame[level_counter], grad_y, CV_16S, 0, 1, 1, 1, 0, BORDER_DEFAULT );
            
            
            
            //*******CALCULATE JACOBIAN*******//
            
          
            
            //*******INITIALIZE VAR*******//
            
            //number of non zero depth points
            int no_nonzero_mask=countNonZero(mask);
            
            Mat jacobian_top(no_nonzero_mask,6, CV_32FC1);
            Mat jacobian_bottom(no_nonzero_mask,6, CV_32FC1);
            
            Mat temp_resized_prev_depth=resized_prev_depth[level_counter]; //temporary ref to access depth at current level
            
            int nRows = resized_rows;
            int nCols = resized_cols;
            int i,j; //to access element
            
            
            //*******INITIALIZE POINTERS*******//
            
            //pointers to access elements
            uchar* depth_ptr;
            
            short* gradx_ptr;
            short* grady_ptr;
            
            float* jacob_top_ptr;
            float* jacob_bottom_ptr;
            
            uchar* mask_ptr;
            
            
            //check if matrix stored continuously
            if(jacobian_top.isContinuous() & jacobian_bottom.isContinuous())
                int jac_rows=1;
            else
                //    int jac_rows= no_nonzero_mask;
                return -1;
            
            
            jacob_top_ptr = jacobian_top.ptr<float>(0);
            jacob_bottom_ptr = jacobian_bottom.ptr<float>(0);
            
            int jac_counter =0;
            
            
            
            //*******LOOP TO CALCULATE JACOBIAN*******//
            
         
            
            for( i = 0; i < nRows; ++i)
            {
                
                depth_ptr = temp_resized_prev_depth.ptr<uchar>(i);
                
                gradx_ptr = grad_x.ptr<short>(i);
                grady_ptr = grad_y.ptr<short>(i);
                
                mask_ptr=mask.ptr<uchar>(i);
                
                
                for ( j = 0; j < nCols; ++j)
                {
                    
                    if(mask_ptr[j]==0)
                        continue;  //skip loop for non zero depth points
                    
                    //Calculate value of Jacobian for a point
                    
                    jacob_bottom_ptr[jac_counter]  = grady_ptr[j]*(-(resized_fy + (pow((resized_cy + i),2) / resized_fy)));
                    jacob_top_ptr[jac_counter++] = gradx_ptr[j]*(-((resized_cy + i)*(resized_cx + j)) / resized_fy);
                    
                    jacob_bottom_ptr[jac_counter]=grady_ptr[j]*(((resized_cy + i)*(resized_cx + j)) / resized_fx);
                    jacob_top_ptr[jac_counter++] = gradx_ptr[j]*(resized_fx + (pow((resized_cx + j),2) / resized_fx));
                    
                    jacob_bottom_ptr[jac_counter]=grady_ptr[j]*((resized_fy*(resized_cx + j)) / resized_fx);
                    jacob_top_ptr[jac_counter++]  = gradx_ptr[j]*(-(resized_fx*(resized_cy + i) / resized_fy));
                    
                    jacob_bottom_ptr[jac_counter]=0;
                    jacob_top_ptr[jac_counter++]  =gradx_ptr[j]*( resized_fx*(pow(depth_ptr[j],-1)));
                    
                    jacob_bottom_ptr[jac_counter]= grady_ptr[j]*(resized_fy*(pow(depth_ptr[j],-1)));
                    jacob_top_ptr[jac_counter++]  = 0;
                    
                    jacob_bottom_ptr[jac_counter]= grady_ptr[j]*(-(resized_cx + j) *(pow(depth_ptr[j],-1)));
                    jacob_top_ptr[jac_counter++]  = gradx_ptr[j]*(-(resized_cx + j) *(pow(depth_ptr[j],-1)));
                    
                    
                }
            }
            
            
            //*******CALCULATE STEEPEST DESCENT*******//
            
            
            
            Mat steepest_desc(no_nonzero_mask,6, CV_64FC1);
            steepest_desc=jacobian_top+jacobian_bottom;
            
           
            
            //*******CALCULATE HESSIAN*******//
            
            start = clock();
            
            Mat hessian=steepest_desc.t()*steepest_desc;
            
            end = clock();
            cout <<"\nTime: "<<double( end - start) / CLOCKS_PER_SEC<<endl;
            

       
            
            
            
        } //exit precomputation loop
        
        
        //end = clock();
        //cout <<"\nTime: "<<double( end - start) / CLOCKS_PER_SEC<<endl
        
       

        
    } //exit frame loop
    

    
   
    
    return 0;
} //exit main






