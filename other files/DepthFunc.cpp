//
//  DepthFunc.cpp
//  odometry code 3
//
//  Created by Himani Arora on 25/08/15.
//  Copyright (c) 2015 Himani Arora. All rights reserved.
//


#include <cstdio>
#include <cstdlib>
#include <Eigen/Dense>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>
#include <fstream>

/*
 
 #include "UserDefinedFunc.h"
 #include "IterationFunc.h"
 
 */


#include "ImageClass.h"
#include "ExternVariable.h"

#include "DepthFunc.h"




void InitializeRandomly(ImageClass* new_frame, int height, int width, Mat gradx, Mat grady, Mat new_depth)
{
    /*
    activeKeyFramelock = new_frame->getActiveLock();
    activeKeyFrame = new_frame;
    activeKeyFrameImageData = activeKeyFrame->image(0);
    activeKeyFrameIsReactivated = false;
    const float* maxGradients = new_frame->maxGradients();
     */
    
    float* gradx_ptr;
    float* grady_ptr;
    float* new_depth_ptr;
    
    for(int y=1;y<height-1;y++)
    {
        gradx_ptr=gradx.ptr<float>(y);
        grady_ptr=grady.ptr<float>(y);
        new_depth_ptr=new_depth.ptr<float>(y);
        
        for(int x=1;x<width-1;x++)
        {
            if(max(gradx_ptr[x],grady_ptr[x]) > util::MIN_ABS_GRAD_CREATE)
            {
                float idepth = 0.5f + 1.0f * ((rand() % 100001) / 100000.0f);
                new_depth_ptr[x]=1/idepth; //depth
                //new_depth_ptr[x+1]=idepth; //inverse depth
                //new_depth_ptr[x+2]=util::VAR_RANDOM_INIT_INITIAL; //variance
            }
            
            else
            {
               new_depth_ptr[x]=0.0f;
            }
        }
    }
    
 
}
