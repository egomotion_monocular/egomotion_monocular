#include <cstdio>
#include<ctime>
#include <cstdlib>
#include <Eigen/Dense>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <unsupported/Eigen/MatrixFunctions>

#include "ImageClass.h"
#include "UserDefinedFunc.h"
#include "ExternVariable.h"


using namespace std;
using namespace cv;
using namespace Eigen;

int main(int argc, char *argv[])
{
    clock_t start, end;
    
    
    //*******VIDEO INITIALIZATIONS*******//
    
    //capture BGR and depth videos from file
    VideoCapture bgr_capture("/Users/himaniarora/Desktop/vids/rgb1.avi");// open the BGR video sequence file
    VideoCapture depth_capture("/Users/himaniarora/Desktop/vids/depth1.avi"); // open the associated depth map file
    
    
    //*******CHECKS*******//
    
    //check if successful
    if (!bgr_capture.isOpened()) // check if we succeeded with BGR
        return -1;
    if (!depth_capture.isOpened()) // check if we succeeded with depth
        return -1;
    
    
    //*******PARAMETER INITIALIZATIONS*******//
    
    //get no of frames in video , no of rows and no of columns
    double no_of_frames = depth_capture.get(CV_CAP_PROP_FRAME_COUNT); //get the number of frames in depth
    
    
    
    //*******MATRIX INITIALIZATIONS*******//
    
    //initialize frame and depth matrices
    ImageClass current_frame;//stores current frame, invokes default constructor
    ImageClass current_depth; //stores current depth map,invokes default constructor
    
    ImageClass prev_frame; //stores prev frame, invokes default constructor
    ImageClass prev_depth; //stores prev depth map, invokes default constructor
    
    Mat frame(orig_rows,orig_cols,CV_8UC3);
    Mat depth(orig_rows,orig_cols,CV_8UC3);
    

    
    
    //*******WINDOW FOR DISPLAY*******//
    //Create window for display
    //namedWindow( "Display video", WINDOW_AUTOSIZE );// Create a window to display grayscale video.
    //namedWindow( "Display depth", WINDOW_AUTOSIZE );// Create a window to display depth sequence.
    
    
    
    //*******ENTER FRAME LOOP*******//
    
    for (int frame_counter = 1; frame_counter < 3 ; frame_counter++)
    {
        start = clock();
        
  
        //*******INITIAL TASKS*******//
        
        //get current frame and convert to grayscale;

        bgr_capture >> frame; // get a new frame from bgr
        current_frame.ConvertImageToGray(frame);
        

        depth_capture >> depth; // get a new frame from depth
        current_depth.ConvertImageToGray(depth);
        

        
         //*******IMAGE PYRAMIDS FOR CURRENT FRAME*******//
        
        //form current frame and depth pyramids
        current_frame.PopulatePyramid();
        current_depth.PopulatePyramid();
        
        
        
        //*******BOOTSTRAPPING FOR FIRST FRAME*******//
        
        // to bootstrap the first image in the sequence
        if(frame_counter==1)
        {
            
            
            //make new copies of current frame and depth
            current_frame.ClonePyramid(prev_frame);
            current_depth.ClonePyramid(prev_depth);

            continue; //jump to next iteration
        }
        
    
        
        
        //*******ENTER PYRAMID LOOP*******//
        
        
        for (int level_counter=3; level_counter>=0; level_counter--)
        {
           
            
            //*******PRECOMPUTATION BEGINS*******//
            
            
            //*******CALCULATE PARAMETERS*******//
            
            //get number of rows and columns at a particular level
            int nRows=prev_depth.GetNumRows(level_counter);
            int nCols=prev_depth.GetNumCols(level_counter);
            
           // cout<<"\n\n\nFrame: "<<frame_counter;
           // cout<<"\nPyramid level: "<<level_counter;
           // cout<<"\n nRows : nCols = "<<nRows<<" : "<<nCols;
            
            //calculate maximum no of iterations for a level
            int MAX_ITER = 12 * (6 - level_counter);
            
 
            
            
            //*******CALCULATE MASK*******//
            
            //Forming image mask
            Mat mask(nRows,nCols,CV_8UC1);
            mask=FormImageMask(prev_depth.pyramid[level_counter]);
            
           // cout<<"\nType: "<<(mask.type()== CV_8UC1);
            
            
                    
            //*******CALCULATE GRADIENT*******//
            
            Mat grad_x, grad_y;
            
            // Calculate Gradient X
            FindGradient(prev_frame.pyramid[level_counter], grad_x, 'x');
            
            //Calculate Gradient Y
            FindGradient(prev_frame.pyramid[level_counter], grad_y, 'y');

   
            //*******CALCULATE JACOBIAN AND STEEPEST DESCENT*******//
            
            Mat steepest_desc, hessian;
            
            steepest_desc=CalculateSteepestDescent(prev_depth.pyramid[level_counter], mask, grad_x ,grad_y, nRows, nCols,level_counter);
            
            
            
            //*******CALCULATE HESSIAN*******//
            
            hessian=CalculateHessian(steepest_desc);
            
           // cout<<"\n\nIn main"<<hessian;
            
            //HERE ITERATION SHOULD START RIGHT?? => FOR A PARTICULAR PYRAMID LEVEL
            
           
            
        } //exit pyramid loop
        
        
        
        
        //*******UPDATE MATRICES*******//
        
        //copying current frame and pyramids to previous frame and pyramid
        current_frame.ClonePyramid(prev_frame);
        
        //copying curent depth and pyramids to previous depth and pyramid
        current_depth.ClonePyramid(prev_depth);
        
        
        
        
        end = clock();
        cout <<"\nTime: "<<double( end - start) / CLOCKS_PER_SEC<<endl;
        


        
    } //exit frame loop
    
   
    
    
    
    return 0;
} //exit main







