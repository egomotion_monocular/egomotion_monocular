#ifndef odometry_code_3_ExternVariable_h
#define odometry_code_3_ExternVariable_h

#include <cstdlib>
#include <iostream>

//*******CONSTANTS*******//

static const int MAX_PYRAMID_LEVEL = 4;
static const int orig_rows = 480;
static const int orig_cols= 640;

static const float orig_fx = 481.20;
static const float orig_fy = -480.00;
static const float orig_cx = 319.5;
static const float orig_cy = 239.5;


#endif
