//
//  UserDefinedFunc.cpp
//  odometry code 3
//
//  Created by Himani Arora on 18/07/15.
//  Copyright (c) 2015 Himani Arora. All rights reserved.
//


#include <cstdio>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "UserDefinedFunc.h"
#include "ImageClass.h"
#include "ExternVariable.h"

using namespace std;
using namespace cv;

/*
 FUNCTION: FORM IMAGE MASK
 
 PURPOSE: FORMS MASK WITH NON-ZERO DEPTH POINTS=1, ZERO DEPTH POINTS=255
 INPUT: DEPTH IMAGE
 OUTPUT: MASK MATRIX
 */
Mat FormImageMask(Mat depth_img)
{
    return (depth_img>10);
    
}



/*
 FUNCTION: FIND GRADIENT
 
 PURPOSE: FIND IMAGE GRADIENT IN A PARTICULAR DIRECTION
 INPUT: SOURCE MATRIX, DESTINATION MATRIX, DIRECTION (x OR y)
 OUTPUT: DESTINATION MATRIX
 */
void FindGradient(Mat Source,Mat& dst, char direction)
{
    
    
    if (direction == 'x')
    {
        
        //float kernelarr = { -1, 0, 1 };
        Mat kernel = (Mat_<float>(1, 3)<< -1,0,1);
        // Gradient X
        //Sobel(src, dst, ddepth=16S, dx=1, dy=0, ksize=1 (for no smoothing), scale=1,delta=0, borderType=BORDER_DEFAULT )∂
        filter2D(Source, dst, CV_16S, kernel);
    }
    
    else if (direction == 'y')
    {
        
        //float kernelarr = { -1, 0, 1 };
        Mat kernel = (Mat_<float>(3,1)<< -1,0,1);
        //Gradient Y
        //Sobel(src, dst, ddepth=16S, dx=0, dy=1, ksize=1 (for no smoothing), scale=1,delta=0, borderType=BORDER_DEFAULT )∂
        filter2D(Source, dst, CV_16S, kernel);
        
    }
    return;
    
}



/*
 FUNCTION: CALCULATE STEEPEST DESCENT
 
 PURPOSE: RETURNS STEEPEST DESCENT MATRIX FOR A FRAME AT A PARTICULAR PYRAMID LEVEL
 INPUTS: DEPTH MATRIX, MASK OF DEPTH MATRIX, GRADIENT IN X DIRECTION OF FRAME, GRADIENT IN Y DIRECTION OF FRAME, 
         NUMBER OF ROWS, NUMBER OF COLUMNS, PYRAMID LEVEL
 OUTPUT: STEEPEST DESCENT MATRIX 
 */

Mat CalculateSteepestDescent(Mat depth, Mat mask, Mat gradx, Mat grady, int nRows, int nCols, int pyrlevel)
{
    
    
    //*******INITIALIZE VAR*******//

    int no_nonzero_mask=countNonZero(mask);
    
    //cout<<"\nNum of nonzero: "<<no_nonzero_mask;
    
    Mat jacobian_top(no_nonzero_mask,6, CV_32FC1);
    Mat jacobian_bottom(no_nonzero_mask,6, CV_32FC1);
    
    int i,j; //to access element
    
    //calculate resized intrinsic focal parameters for pyrlevel
    float* resized_intrinsic= GetIntrinsic(pyrlevel);
    float resized_fx=resized_intrinsic[0];
    float resized_fy=resized_intrinsic[1];
    float resized_cx=resized_intrinsic[2];
    float resized_cy=resized_intrinsic[3];
    
    //cout<<"\nResized cx: "<<resized_cx;
    
    
    //*******INITIALIZE POINTERS*******//
    
    //pointers to access elements
    uchar* depth_ptr;
    
    short* gradx_ptr;
    short* grady_ptr;
    
    float* jacob_top_ptr;
    float* jacob_bottom_ptr;
    
    uchar* mask_ptr;
    
    int jac_rows;
    
    //check if matrix stored continuously
    if(jacobian_top.isContinuous() & jacobian_bottom.isContinuous())
        jac_rows= -1;
    else
         jac_rows= 0;
    
    jacob_top_ptr = jacobian_top.ptr<float>(0);
    jacob_bottom_ptr = jacobian_bottom.ptr<float>(0);
    
    int jac_counter =0;
    
    
    //*******LOOP TO CALCULATE JACOBIAN*******//
    
    
    
    for( i = 0; i < nRows; ++i)
    {
        
        depth_ptr = depth.ptr<uchar>(i);
        
        gradx_ptr = gradx.ptr<short>(i);
        grady_ptr = grady.ptr<short>(i);
        
        mask_ptr=mask.ptr<uchar>(i);
        
        
        for ( j = 0; j < nCols; ++j)
        {
            
            if(mask_ptr[j]==0)
                continue;  //skip loop for non zero depth points
            
            
            //Calculate value of Jacobian for a point
            
            jacob_bottom_ptr[jac_counter]  = grady_ptr[j]*(-(resized_fy + (pow((resized_cy + i),2) / resized_fy)));
            jacob_top_ptr[jac_counter++] = gradx_ptr[j]*(-((resized_cy + i)*(resized_cx + j)) / resized_fy);
            
            jacob_bottom_ptr[jac_counter]=grady_ptr[j]*(((resized_cy + i)*(resized_cx + j)) / resized_fx);
            jacob_top_ptr[jac_counter++] = gradx_ptr[j]*(resized_fx + (pow((resized_cx + j),2) / resized_fx));
            
            jacob_bottom_ptr[jac_counter]=grady_ptr[j]*((resized_fy*(resized_cx + j)) / resized_fx);
            jacob_top_ptr[jac_counter++]  = gradx_ptr[j]*(-(resized_fx*(resized_cy + i) / resized_fy));
            
            jacob_bottom_ptr[jac_counter]=0;
            jacob_top_ptr[jac_counter++]  =gradx_ptr[j]*( resized_fx*(pow(depth_ptr[j],-1)));
            
            jacob_bottom_ptr[jac_counter]= grady_ptr[j]*(resized_fy*(pow(depth_ptr[j],-1)));
            jacob_top_ptr[jac_counter++]  = 0;
            
            jacob_bottom_ptr[jac_counter]= grady_ptr[j]*(-(resized_cx + j) *(pow(depth_ptr[j],-1)));
            jacob_top_ptr[jac_counter++]  = gradx_ptr[j]*(-(resized_cx + j) *(pow(depth_ptr[j],-1)));
            
            
            if(jac_rows>-1)
            {   jac_rows=jac_rows+1;
                jacob_bottom_ptr=jacobian_bottom.ptr<float>(jac_rows);
                jacob_top_ptr=jacobian_top.ptr<float>(jac_rows);
                jac_counter=0;
            }
            
            
            
        }
    }
    
    
    //*******CALCULATE STEEPEST DESCENT*******//
    
    
    Mat steepest_desc(no_nonzero_mask,6, CV_64FC1);
    steepest_desc=jacobian_top+jacobian_bottom;
    
    return steepest_desc;
    
    
   
}






/*FUNCTION: CALCULATE HESSIAN
 
 PURPOSE: RETURNS HESSIAN MATRIX FOR A FRAME
 
 INPUTS: STEEPEST DESCENT  MATRIX
 OUTPUTS: HESSIAN MATRIX
*/

Mat CalculateHessian(Mat steepest_desc)
{
    return steepest_desc.t()*steepest_desc;
}






/*FUNCTION: GET INTRINSIC
 
 PURPOSE: RETURN THE RESIZED INTRINSIC MATRIX [fx, fy, cx, cy]
 INPUT: PYRAMID LEVEL
 OUTPUT: POINTER TO FIRST ELEMENT OF INTRINSIC MATRIX
*/

float* GetIntrinsic(int pyrlevel)
{
    //calculating resized intrinsic parameters  fx, fy, cx, cy
    float resized_fx=orig_fx/pow(2,pyrlevel);
    float resized_fy=orig_fy/pow(2,pyrlevel);
    float resized_cx=orig_cx/pow(2,pyrlevel);
    float resized_cy=orig_cy/pow(2,pyrlevel);
    
    //storing in resized_intrinsic=[fx, fy, cx, cy];
    Mat resized_intrinsic = (Mat_<float>(1,4)<<resized_fx, resized_fy, resized_cx, resized_cy);
    
    //return pointer to the first element
    return resized_intrinsic.ptr<float>(0);
}





