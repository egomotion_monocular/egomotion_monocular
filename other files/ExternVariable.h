
#pragma once

#ifndef odometry_code_3_ExternVariable_h
#define odometry_code_3_ExternVariable_h

#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "opencv2/opencv.hpp"

//*******CONSTANTS*******//

using namespace std;
using namespace cv;
using namespace Eigen;


namespace util
{
    
void  initializeConstants();

    //intrinsic parametrs
    static const int ORIG_ROWS = 1080/4.0f;
    static const int ORIG_COLS=1920/4.0f;

    static const float ORIG_FX =1642.405612f/4.0f;
    static const float ORIG_FY =1636.148027f/4.0f;
    static const float ORIG_CX =892.153028f/4.0f;
    static const float ORIG_CY =483.240652f/4.0f;
    
    static const int KEYFRAME_SWITCH_INTERVAL=8; //keyframe propagation
    static const int MAX_PYRAMID_LEVEL = 4; //coarse to fine
    static const int MAX_ITER=12; //gauss newton optimization
    
    static const float weight[]={100000.0f,100000.0f,100000.0f,10000.0f,10000.0f,10000.0f};
    static const float level_term[]={0.999f,1.0f,1.05f,1.1f,1.1f};//{1.1f,1.07f,1.05f,1.0f,0.999f};
    static const float threshold1= 10.0f;
    static const float switchKeyframeThreshold=5.5f;

    static const float MIN_ABS_GRAD_CREATE=1.0f;
    static const float MIN_ABS_GRAD_DECREASE=5.0f;
    static const int MIN_BLACKLIST=-1;
    
    static const float  MAX_DIFF_CONSTANT =40.0f*40.0f;
    static const float MAX_DIFF_GRAD_MULT = 0.5f*0.5f;
    
    static const float VAR_RANDOM_INIT_INITIAL=0.125f;
    
    
// ============== initial stereo pixel selection ======================
    static const float MIN_EPL_GRAD_SQUARED=(2.0f*2.0f);
    static const float MIN_EPL_LENGTH_SQUARED= (1.0f*1.0f);
    static const float MIN_EPL_ANGLE_SQUARED= (0.3f*0.3f);
   
    
// ============== stereo & gradient calculation ======================
    static const float MIN_DEPTH =0.05f ;// this is the minimal depth tested for stereo., using now! 
    
    // particularely important for initial pixel.
    static const float MAX_EPL_LENGTH_CROP =30.0f; // maximum length of epl to search.
    static const float MIN_EPL_LENGTH_CROP =3.0f; // minimum length of epl to search.
    
    // this is the distance of the sample points used for the stereo descriptor.
    static const float GRADIENT_SAMPLE_DIST =1.0f;  //using now!
    
    // pixel a point needs to be away from border... if too small: segfaults!
    static const float SAMPLE_POINT_TO_BORDER= 7.0f;  //using now ; MADE FLOAT
    
    // pixels with too big an error are definitely thrown out.
    static const float MAX_ERROR_STEREO =1300.0f; // maximal photometric error for stereo to be successful (sum over 5 squared intensity differences)
    static const float MIN_DISTANCE_ERROR_STEREO =1.5f; // minimal multiplicative difference to second-best match to not be considered ambiguous.
    
    // defines how large the stereo-search region is. it is [mean] +/- [std.dev]*STEREO_EPL_VAR_FAC
    static const float STEREO_EPL_VAR_FAC= 2.0f;
    
    static const float DIVISION_EPS =1e-10f;
    
    /// to be understood///////////////////////
    static const int CAMERA_PIXEL_NOISE=4*4;
    static const float VAR=0.5f*0.5f;
    static const int VALIDITY_COUNTER_INITIAL_OBSERVE=5;	// initial validity for first observations
    
    static const float SUCC_VAR_INC_FAC=(1.01f); // before an ekf-update, the variance is increased by this factor.
    static const float FAIL_VAR_INC_FAC=1.1f; // after a failed stereo observation, the variance is increased by this factor.
    static const float MAX_VAR=(0.5f*0.5f); // initial variance on creation - if variance becomes larter than this, hypothesis is removed.

    
    static const float VAR_GT_INIT_INITIAL=0.01f*0.01f;	// initial variance vor Ground Truth Initialization
    
    static const float DIFF_FAC_OBSERVE=(1.0f*1.0f);
    static const float DIFF_FAC_PROP_MERGE= (1.0f*1.0f);

    static const float VALIDITY_COUNTER_MAX=(5.0f);
    static const float VALIDITY_COUNTER_MAX_VARIABLE=(250.0f);
    static const float VALIDITY_COUNTER_DEC=5.0f;
    static const float VALIDITY_COUNTER_INC=5.0f;
    static const float REFERANCE_FRAME_MAX_AGE= 10.0f;

    static const float IDEPTH_MULIPLIER=100;
    
    static const float VAL_SUM_MIN_FOR_CREATE= 30.0f;
    static const float VAL_SUM_MIN_FOR_UNBLACKLIST=100.0f;
    static const float VAL_SUM_MIN_FOR_KEEP=24.0f;
    
    static const float REG_DIST_VAR= 0.075f*0.075f*1.0f*1.0f;
    static const float DIFF_FAC_SMOOTHING=1.0f*1.0f;
    
    static const float CAMERA_PIXEL_NOISE_2=4.0f*4.0f;
    static const float HUBER_D=3.0f;
    
    static const float HISTOGRAM_STEP=0.001f;
    static const float HISTOGRAM_EXTREME=0.500f;
    static const int HISTOGRAM_SIZE=1001;//(((HISTOGRAM_EXTREME/HISTOGRAM_STEP))*2)+1;//
    static const int HISTOGRAM_INIT_FRAMES=1;//200;
    
    static const int YMIN=3;
    static const int YMAX=ORIG_ROWS-3;
    
    //Flags
    static const bool FLAG_DISPLAY_IMAGES=true;
    static const bool FLAG_DISPLAY_DEPTH_MAP=true;
    static const bool FLAG_SAVE_DEPTH_MAP=false;
    static const bool FLAG_WRITE_POSE=true;
    
// ================== MACROS ============================================
    #define UNZERO(val) (val < 0 ? (val > -1e-10 ? -1e-10 : val) : (val < 1e-10 ? 1e-10 : val))
}
    
    
#endif
