
#ifndef odometry_code_3_ExternVariable_h
#define odometry_code_3_ExternVariable_h

#include <cstdio>
#include <cstdlib>
#include <iostream>


//*******CONSTANTS*******//

namespace util
{

static const int MAX_PYRAMID_LEVEL = 5;
static const int ORIG_ROWS = 480;
static const int ORIG_COLS= 640;
static const int MAX_ITER=12;

    static const float ORIG_FX = 524.0f;//535.4f;
    static const float ORIG_FY = 524.0f;//539.2f;
    static const float ORIG_CX = 316.7f;//320.1f;
    static const float ORIG_CY = 238.5f;//247.6f;

static const float weight[]={100000.0f,100000.0f,100000.0f,10000.0f,10000.0f,10000.0f};
static const float threshold1= 10.0f;

}//end namespace util
    
    
#endif
