//
//  DepthFunc.h
//  odometry code 3
//
//  Created by Himani Arora on 25/08/15.
//  Copyright (c) 2015 Himani Arora. All rights reserved.
//

#ifndef __odometry_code_3__DepthFunc__
#define __odometry_code_3__DepthFunc__

#include <stdio.h>

#endif /* defined(__odometry_code_3__DepthFunc__) */
void InitializeRandomly(ImageClass* new_frame, int height, int width, Mat gradx, Mat grady, Mat new_depth);