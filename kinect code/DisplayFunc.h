//
//  DisplayFunc.h

#ifndef __odometry_code_3__DisplayFunc__
#define __odometry_code_3__DisplayFunc__

#include <cstdio>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>

#include "ImageClass.h"

namespace ego{
	void DisplayIterationRes(Mat maskimg, Mat residual, int nRows, int nCols, string name); //for iteration residual
	void DisplayInitialRes(Mat prevdepth, Mat currentframe, Mat prevframe, int nRows, int nCols, string name); //for initial residual
	void DisplayWarpedImg(Mat warpedimg, Mat maskimg, int nRows, int nCols, string name); //for warped image
}//end namespace ego



#endif /* defined(__odometry_code_3__DisplayFunc__) */
