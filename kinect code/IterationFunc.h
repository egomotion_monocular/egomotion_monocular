
#ifndef __odometry_code_3__IterationFunc__
#define __odometry_code_3__IterationFunc__

#include <cstdio>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "ImageClass.h"
#include "ExternVariable.h"
#include <Eigen/Dense>
#include <unsupported/Eigen/MatrixFunctions>

using namespace std;
using namespace cv;

namespace ego{

	//Calculate world points
	void CalculateWorldPoints(Mat& saveimg, Mat& worldpoint, Mat mask, Mat img, Mat depth, int nRows, int nCols, int no_nonzero_mask, int pyrlevel);

	//Calculate warped points for pose vector
	Mat CalculateWarpedPoints(float *pose, Mat worldpoint, int no_nonzero_mask, int pyrlevel);

	//Calculate warped image with inteerpolated intensity values
	Mat CalculateWArpedImage(Mat warpedpoint, Mat img, int nRows, int nCols, int no_nonzero_mask);

	//Update pose estimate and check Iteration Loop Termination Condition
	int UpdatePose(Mat& residual, Mat saveimg, Mat warpedimg, Mat steepdesc, Mat hessian, float* pose, Mat mask,int no_nonzero_mask);

}//end namespace ego

#endif /* defined(__odometry_code_3__IterationFunc__) */







