
#ifndef __odometry_code_3__UserDefinedFunc__
#define __odometry_code_3__UserDefinedFunc__

#include <cstdio>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "ImageClass.h"
#include "ExternVariable.h"
#include <Eigen/Dense>
#include <unsupported/Eigen/MatrixFunctions>

using namespace std;
using namespace cv;

namespace ego{
	//form image mask
	Mat FormImageMask(Mat depth_img);

	//Find image gradient in a particular direction
	void CalculateGradient(Mat Source, Mat& dstx, Mat& desty);

	//returns steepest descent
	Mat CalculateSteepestDescent(Mat depth, Mat mask, Mat gradx, Mat grady, int nRows, int nCols, int pyrlevel, int no_nonzero_mask);

	//returns hessian
	Mat CalculateHessianInverse(Mat steepest_desc);

	//returns intrinsic focal parameters
	vector<float> GetIntrinsic(int pyrlevel);

}//end namespace ego

#endif /* defined(__odometry_code_3__UserDefinedFunc__) */
