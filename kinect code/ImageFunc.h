//
//  ImageFunc.h
//  odometry code 3
//
//  Created by Himani Arora on 13/08/15.
//  Copyright (c) 2015 Himani Arora. All rights reserved.
//

#ifndef __odometry_code_3__ImageFunc__
#define __odometry_code_3__ImageFunc__

#include <cstdio>
#include<ctime>
#include <cstdlib>
#include <Eigen/Dense>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>
#include <fstream>

#include "ImageClass.h"

using namespace std;
using namespace cv;

namespace ego{

	vector<float> GetImagePoseEstimate(ImageClass& PrevFrame, ImageClass& CurrentFrame, ImageClass& PrevDepth, ImageClass& CurrentDepth);

}//end namespace ego





#endif /* defined(__odometry_code_3__ImageFunc__) */
