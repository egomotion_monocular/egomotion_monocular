//
//  ImageFunc.cpp
//  odometry code 3
//
//  Created by Himani Arora on 13/08/15.
//  Copyright (c) 2015 Himani Arora. All rights reserved.
//


#include <cstdio>
#include<ctime>
#include <cstdlib>
#include <Eigen/Dense>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>
#include <fstream>

#include "ImageClass.h"
#include "UserDefinedFunc.h"
#include "IterationFunc.h"
#include "ExternVariable.h"
#include "DisplayFunc.h"


#include "ImageFunc.h"

using namespace std;
using namespace cv;

namespace ego{

	vector<float> GetImagePoseEstimate(ImageClass& prev_frame, ImageClass& current_frame, ImageClass& prev_depth, ImageClass& current_depth)
	{

		float pose[6] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };


		//w1 w2 w3 v1 v2 v3] //se3 pose vector
		//cout<<"\n\n\nInitial pose: "<<pose[0]<<" , "<<pose[1]<<" , "<<pose[2]<<" , "<<pose[3]<<" , "<<pose[4]<<" , "<<pose[5]<<" , ";

		int initial_flag = 0;
		int num_nonzero_mask = 0;
		int nRows, nCols;
		//*******ENTER PYRAMID LOOP*******//

		for (int level_counter = (util::MAX_PYRAMID_LEVEL - 1); level_counter >= 0; level_counter--)
		{

			//*******PRECOMPUTATION BEGINS*******//

			//get number of rows and columns at a particular level
			nRows = prev_depth.GetNumRows(level_counter);
			nCols = prev_depth.GetNumCols(level_counter);

			//Forming image mask
			Mat mask(nRows, nCols, CV_8UC1);//stores depth mask
			mask = FormImageMask(prev_depth.pyramid[level_counter]);

			//calculate number of non-zero depth points
			num_nonzero_mask = countNonZero(mask);

			//calculate gradient
			Mat grad_x, grad_y;//stores x and y gradient
			CalculateGradient(prev_frame.pyramid[level_counter], grad_x, grad_y);

			//calculate steepest descent
			Mat steepest_desc, hessian_inv;//stores steepest descent matrix and hessian matrix
			steepest_desc = CalculateSteepestDescent(prev_depth.pyramid[level_counter], mask, grad_x, grad_y, nRows, nCols, level_counter, num_nonzero_mask);

			//calculate hessian inverse
			hessian_inv = CalculateHessianInverse(steepest_desc);


			//calculate world point
			Mat save_img(1, num_nonzero_mask, CV_32FC1); //save intensity of image at non zero depth points for current pyramid level
			Mat world_point(3, num_nonzero_mask, CV_32FC1); //saves world points to be warped with estimate of e

			CalculateWorldPoints(save_img, world_point, mask, prev_frame.pyramid[level_counter], prev_depth.pyramid[level_counter], nRows, nCols, num_nonzero_mask, level_counter);


			//*******ENTER ITERATION LOOP*******//

			for (int iter_counter = 0; iter_counter < util::MAX_ITER; ++iter_counter)
			{
				//cout<<"\n\nframe: "<<frame_counter<<" , pyramid: "<<level_counter<<" , iteration: "<<iter_counter;
				// cout<<"\nRows : cols "<<nRows<<" ; "<<nCols;

				//calculate warped points
				Mat warped_point = CalculateWarpedPoints(pose, world_point, num_nonzero_mask, level_counter);


				//calculate interpolated warped image
				Mat warped_img = CalculateWArpedImage(warped_point, current_frame.pyramid[level_counter], nRows, nCols, num_nonzero_mask);
				//  cout<<warped_img;

				Mat residual(1, num_nonzero_mask, CV_32FC1);


				//Update pose and check condition for loop termination
				int check_termination = UpdatePose(residual, save_img, warped_img, steepest_desc, hessian_inv, pose,mask, num_nonzero_mask);

				if (check_termination == -1)
				{
					//   cout<<"\nframe: "<<frame_counter<<" , pyramid: "<<level_counter<<" , iteration: "<<iter_counter;
					//    cout<<"\nTerminating iteration..";
					break; //exit iteration loop
				}
				//cout<<"\nupdated pose: "<<pose[0]<<" , "<<pose[1]<<" , "<<pose[2]<<" , "<<pose[3]<<" , "<<pose[4]<<" , "<<pose[5]<<" , ";
				//cout<<"\n\nResidual sum: "<<sum(abs(residual));

                
				//to display current iteration residual
				DisplayIterationRes(mask, residual, nRows, nCols, "Iteration Residual");

				//to display original image residual and original image
				if (initial_flag == 0)
				{
					//display initial residual
					DisplayInitialRes(prev_depth.image, current_frame.image, prev_frame.image, util::ORIG_ROWS, util::ORIG_COLS, "Initial Residual");
					initial_flag = 1;
				}

				//to display warped image
				DisplayWarpedImg(warped_img, mask, nRows, nCols, "Warped Image");
                DisplayWarpedImg(save_img, mask, nRows, nCols, "Original image");

            
				



			} //exit iteration loop


		} //exit pyramid loop
        

		/*
		 //write pose to file
		 if (myfile.is_open())
		 {
		 cout<<"\n\nwriting pose to file..";
		 myfile<<pose[0]<<" , "<<pose[1]<<" , "<<pose[2]<<" , "<<pose[3]<<" , "<<pose[4]<<" , "<<pose[5]<<"\n";
		 }
		 */

		//copy current frame and depth for next capture
		current_frame.ClonePyramid(prev_frame); //copying current frame and pyramids to previous frame and pyramid
		current_depth.ClonePyramid(prev_depth); //copying curent depth and pyramids to previous depth and pyramid

		vector<float> posevec(pose, pose + 6);

		return posevec;


	}
}//end namespace ego