//
//  main.cpp
//  kinect display image
//
//  Created by Himani Arora on 30/09/15.
//  Copyright (c) 2015 Himani Arora. All rights reserved.
//


#include <cstdio>
#include<ctime>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>

using namespace std;
using namespace cv;




int main(int argc, char *argv[])
{
    int row=480;
    int col=640;
    Mat mask=Mat::zeros(row, col, CV_8UC1);
    Mat depth=Mat::zeros(row, col, CV_16UC1);
    Mat new_depth=Mat::zeros(row, col, CV_8UC1);

    Mat rgb=Mat::zeros(row, col, CV_8UC1);

    //capture BGR and depth videos from file
    //VideoCapture bgr_capture("/Users/himaniarora/Desktop/dataset/iitcampus_kinect1/rgb_vid/Image0001.jpg");// open the BGR video sequence file
    Mat rgb_img=imread("/Users/himaniarora/Desktop/dataset/iitcampus_kinect1/rgb_vid/Image0001.jpg", CV_LOAD_IMAGE_COLOR);
    //imshow("rgb image", rgb_img);
    
    Mat ir_depth=imread("/Users/himaniarora/Desktop/dataset/iitcampus_kinect1/depth/Depth0001.ppm", CV_LOAD_IMAGE_ANYDEPTH);
   // imshow("IR depth", ir_depth);
    
    
    
    
    vector<String> filenames; // notice here that we are using the Opencv's embedded "String" class
    String folder ="/Users/himaniarora/desktop/dataset/iitcampus_kinect1/txt/" ;// again we are using the Opencv's embedded " String" class
    
    glob(folder, filenames); // new function that does the job ;-)
    
    
    vector<String> output_filenames; // notice here that we are using the Opencv's embedded "String" class
    String output_folder ="/Users/himaniarora/desktop/dataset/iitcampus_kinect1/rgb_vid_copy/" ;// again we are using the Opencv's embedded " String" class
    glob(output_folder, output_filenames); // new function that does the job ;-)

    
        //uchar* rgb_ptr=rgb.ptr<uchar>(0);
    
    for(size_t i = 1; i < filenames.size(); ++i)
    {
        Mat mask=Mat::zeros(row, col, CV_8UC1);
        Mat depth=Mat::zeros(row, col, CV_16UC1);
        ifstream my_file;
        int x,y, imr,img,imb;
        float X,Y,Z;
        uchar* mask_ptr=mask.ptr<uchar>(0);
        unsigned short* depth_ptr=depth.ptr<unsigned short>(0);

    
    my_file.open(filenames[i]);
        cout<<"\nFile path: "<<filenames[i];
        
        cout<<"\nOutput File path: "<<output_filenames[i];
    
        if(my_file.eof())
        {     cerr << "\nProblem loading image!!!\n" << endl;
            continue;
        }
        
         

        
    while(!my_file.eof())
    {
        my_file>>y>>x;
        x=col-x;
        mask_ptr=mask.ptr<uchar>(y);
        mask_ptr[x]=1;
        
        my_file>>X>>Y>>Z;
        //printf("\nx= %d, y= %d, Z= %f", x,y,Z);

        depth_ptr=depth.ptr<unsigned short>(y);
        depth_ptr[x]=(short(Z*10000));
        //printf(", depth= %d",  depth_ptr[x]);
        
        my_file>>imr>>img>>imb;
        //rgb_ptr=rgb.ptr<uchar>(y);
        
        
        
    }
        
        
        //imshow("depth", depth);
        
        vector<int> compression_params; //vector that stores the compression parameters of the image
        compression_params.push_back(CV_IMWRITE_JPEG_QUALITY); //specify the compression technique
        compression_params.push_back(100); //specify the compression quality
        
        depth.convertTo(new_depth, CV_8UC1,0.00390625);
        //cout<<new_depth;
        imwrite(output_filenames[i], new_depth, compression_params);
        
        Mat see_new_depth=imread(output_filenames[i]);
        //imshow("new depth", see_new_depth);
        
       // waitKey(0);
        
        
    //cout<<mask;
    //waitKey(0);
    //cout<<depth;
    //waitKey(0);
    my_file.close();
    }
    //imshow("depth", depth);
  
    
    //imshow("residual depth", (abs(ir_depth-depth)));
    //waitKey(0);

    return  0;
}