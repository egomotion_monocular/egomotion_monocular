
#include <cstdio>
#include<ctime>
#include <cstdlib>
#include <Eigen/Dense>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>
#include <fstream>

/*

#include "UserDefinedFunc.h"
#include "IterationFunc.h"

#include "DisplayFunc.h"
*/

#include "ImageClass.h"
#include "ImageFunc.h"
#include "ExternVariable.h"

using namespace std;


int main(int argc, char *argv[])
{
	clock_t start, end;

	//ofstream myfile;
	//myfile.open("/Users/himaniarora/Desktop/poses.txt");

	//initialize frame and depth matrices

	ego::ImageClass current_frame;//stores current frame, invokes default constructor
	ego::ImageClass current_depth; //stores current depth map,invokes default constructor

	ego::ImageClass prev_frame; //stores prev frame, invokes default constructor
	ego::ImageClass prev_depth; //stores prev depth map, invokes default constructor

	cv::Mat frame(util::ORIG_ROWS, util::ORIG_COLS, CV_8UC3);
	cv::Mat depth(util::ORIG_ROWS, util::ORIG_COLS, CV_8UC3);

	//*******VIDEO INITIALIZATIONS*******//

	//capture BGR and depth videos from file
	cv::VideoCapture bgr_capture("/Users/himanshuaggarwal/Desktop/egocentric/datasets/iit_kinect_shoot/rgb_vid/%06d.jpg");// open the BGR video sequence file
	cv::VideoCapture depth_capture("/Users/himanshuaggarwal/Desktop/egocentric/datasets/iit_kinect_shoot/corrected_depth/%06d.jpg"); // open the associated depth map file
    
    ifstream my_file;
    my_file.open("/Users/himanshuaggarwal/Desktop/egocentric/datasets/iit_kinect_shoot/ply/%06d.txt");


	//check if successful
	if (!bgr_capture.isOpened()) // check if we succeeded with BGR
		return -1;
	if (!depth_capture.isOpened()) // check if we succeeded with depth
		return -1;

	//*******PARAMETER INITIALIZATIONS*******//

	//get no of frames in video , no of rows and no of columns
	float no_of_frames = depth_capture.get(CV_CAP_PROP_FRAME_COUNT); //get the number of frames in depth
	vector<float>pose_vec;


	//namedWindow( "Iteration Rsidual", WINDOW_AUTOSIZE );// Create a window for display.
	//namedWindow( "Initial Residual", WINDOW_AUTOSIZE );// Create a window for display.

	//*******ENTER FRAME LOOP*******//

	for (int frame_counter = 1; frame_counter <= no_of_frames ; frame_counter++)
	{
		start = clock();
        cout<<"\nFrame Id: "<<frame_counter;

		//captures frame, converts to grayscale and forms pyramid
		current_frame.FrameCapture(bgr_capture, "Frame");
		current_depth.FrameCapture(depth_capture, "Depth");

		// to bootstrap the first image in the sequence
		if (frame_counter == 1)
		{
			//make new copies of current frame and depth
			current_frame.ClonePyramid(prev_frame);
			current_depth.ClonePyramid(prev_depth);

			continue; //jump to next iteration
		}



		pose_vec = ego::GetImagePoseEstimate(prev_frame, current_frame, prev_depth, current_depth);


		end = clock();
		//cout << "\nTime: " << float(end - start) / CLOCKS_PER_SEC << endl;

		 waitKey(1000);
		

	} //exit frame loop    


	return 0;
} //exit main










