
//
//Frame.h
//

#pragma once

#ifndef __odometry__Frame__
#define __odometry__Frame__

#pragma once

#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>

#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "DepthHypothesis.h"


using namespace std;
using namespace cv;

class frame
{
    
public:
    
    //constructors
    frame();
    frame(VideoCapture vidcap_rgb);
    frame(VideoCapture vidcap_rgb,VideoCapture vidcap_depth);
    
    
    int frameId;

    int parentKeyframeId;
    
    bool isKeyframe; 
    
    bool deptharrfirstcall;
    
    bool deptharrpyrfirstcall;
    
    //bool isKeyframe;
    
    frame* keyFrame;
    
    int width,height;
    
    int* correspondingDepthHypothesis;
    
    Mat image;
    
    Mat image_pyramid[5];
    
    Mat depth;
    
    float* deptharr;
    
    float* deptharrpyr[5];
    
    float* depthvararr;
    
    float* depthvararrpyr[5];
    
    Mat depth_pyramid[5];
   
    //Mat depth_var;
    
    //Mat depth_var_pyramid[5];
    
    Mat gradientx;
    
    Mat gradienty;
    
    Mat maxAbsGradient;
    
    Mat mask;
    
    int currentRows,currentCols;
    
    int pyrLevel;
    
    int no_nonZeroDepthPts;

    //depthhypothesis* pixels;
    
    //Pose matrices
    float poseWrtOrigin[6]; //wrt KF
    
    float poseWrtWorld[6];
    
    float rescaleFactor; //wrt KF
    
    Eigen::Matrix4f SE3poseThisWrtOther;
    Eigen::Matrix4f SE3poseOtherWrtThis;
    Eigen::Matrix3f SE3poseThisWrtOther_r;
    Eigen::MatrixXf SE3poseThisWrtOther_t;
    Eigen::Matrix3f SE3poseOtherWrtThis_r;
    Eigen::MatrixXf SE3poseOtherWrtThis_t;
    
    Eigen::Matrix4f K_SE3poseThisWrtOther;
    Eigen::Matrix4f K_SE3poseOtherWrtThis;
    Eigen::Matrix3f K_SE3poseThisWrtOther_r;
    Eigen::MatrixXf K_SE3poseThisWrtOther_t;
    Eigen::Matrix3f K_SE3poseOtherWrtThis_r;
    Eigen::MatrixXf K_SE3poseOtherWrtThis_t;

    //member functions
    //void initializeDepth(bool isFirstFrame);
    
    void constructImagePyramids();
    
    //void constructDepthPyramids();
    
    void calculateGradient();
    
    void initializeRandomly();
    
    void getImgSize(int& row,int& col,int level);
    
    void calculateNonZeroDepthPts();
    
    void initializePose();
    
    void updationOnPyrChange(int level,bool isPrevious=true);
    
    float getInterpolatedElement(float x1,float y1);
    
    float getInterpolatedElement(float x1,float y1,String S);
    
    void calculatePoseWrtOrigin(frame* prev_image,float* poseChangeWrtPrevframe);
    
    void calculatePoseWrtWorld(frame* prev_image,float* poseChangeWrtPrevframe);

    
    //void calculateSE3poseThisWrtOther(frame* other_frame);  // Other To This
    
    void calculateSE3poseOtherWrtThis(frame *other_frame); //This To Other
    
    void calculateSim3poseOtherWrtThis( float scale_factor);

    void buildMaxGradients();

    void buildInvVarDepth(int level);
    
};


#endif /* defined(__odometry__Frame__) */
