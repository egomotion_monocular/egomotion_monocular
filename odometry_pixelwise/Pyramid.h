//
//  Pyramid.h
//  odometry
//
//  Created by Himanshu Aggarwal on 9/4/15.
//  Copyright (c) 2015 Himanshu Aggarwal. All rights reserved.
//

#ifndef __odometry__Pyramid__
#define __odometry__Pyramid__

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "Frame.h"
#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>

#include <cstdio>
#include<ctime>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "ExternVariable.h"
#include <cmath>
#include "DepthPropagation.h"
#include "DepthHypothesis.h"



using namespace cv;

class Pyramid{
    
public:
    
    int level;
    float *pose;
    float lastErr;
    float error;
    float pointUsage;
    float weightedPose;
    int buf_size;
    /*float* trfmworld_x;
    float* trfmworld_y;
    float* trfmworld_z;
    float* warped_u;
    float* warped_v;
    float* warped_intensity;
    float* residual1;
    float* weights1;
    float* gx;
    float* gy;*/
    
    vector<float> trfmworld_x;
    vector<float> trfmworld_y;
    vector<float> trfmworld_z;
    vector<float> warped_u;
    vector<float> warped_v;
    vector<float> warped_intensity;
    vector<float> residual1;
    vector<float> weights1;
    vector<float> gx;
    vector<float> gy;
    vector<int> skippedIdxOrig;
    
    Eigen::Matrix<float, 6, 6> A;
    
    Eigen::Matrix<float, 6, 1> b;
    
    
    Mat steepestDescent;
    Mat hessianInv;
    Mat worldPoints;
    Mat transformedWorldPoints;
    Mat saveImg;
    Mat warpedPoints;
    Mat warpedImage;
    Mat residual;
    Mat warpedGradientx;
    Mat warpedGradienty;
    Mat weights;
   // Mat weight_diag;
    
    
    frame* prev_frame;
    frame* current_frame;
    depthMap* currentDepthMap;
    
public:
    
    Pyramid(frame* prevframe,frame* currentframe,float* pose,depthMap* currDepthMap);
    
    void performPrecomputation();
    float performIterationSteps();
    void calculateSteepestDescent();
    void calculateHessianInv();
    void calculateWorldPoints();
    void calculateWarpedPoints();
    void calculateWarpedImage();
    void updatePose();
    void performPixelWise();
    void calculatePose();
    float calResidualAndWeights();
    
};

#endif /* defined(__odometry__Pyramid__) */
