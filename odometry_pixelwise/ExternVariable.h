
#pragma once

#ifndef odometry_code_3_ExternVariable_h
#define odometry_code_3_ExternVariable_h

#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "opencv2/opencv.hpp"

//*******CONSTANTS*******//

using namespace std;
using namespace cv;
using namespace Eigen;


namespace util
{
    
    typedef Eigen::Matrix<float, 6, 1> Vector6;
    typedef Eigen::Matrix<float, 6, 6> Matrix6x6;
    
    typedef Eigen::Matrix<float, 7, 1> Vector7;
    typedef Eigen::Matrix<float, 7, 7> Matrix7x7;
    
    typedef Eigen::Matrix<float, 4, 1> Vector4;
    typedef Eigen::Matrix<float, 4, 4> Matrix4x4;
    

    
void  initializeConstants();

static const int MAX_PYRAMID_LEVEL = 5;
    static const int ORIG_ROWS = 480;//480; //720;//360;//
    static const int ORIG_COLS=640;//640; //1080;//540;//
static const int MAX_ITER=12;

    static const float ORIG_FX = 520.9f;//1642.405f/2.0f;  //535.4f; //1003.4f/1.0f;//
    static const float ORIG_FY =  521.0f;///2.0f;//539.2f; //1002.4f/1.0f ;//
    static const float ORIG_CX =325.1f; //320.1f; //540.0f/1.0f;//526.4/2.0f;//
    static const float ORIG_CY =249.7f;//247.6f; //360.0f/1.0f;//355.3/2.0f;//

/*
    Mat K;
Mat Kinv;

Matrix<float, Dynamic, Dynamic, RowMajor> K_Eigen;
Matrix<float, Dynamic, Dynamic, RowMajor> Kinv_Eigen; */
    
  /*  float ORIG_FX_INV ;
    float ORIG_FY_INV ;
    float ORIG_CX_INV ;
    float ORIG_CY_INV ; */
    
    
  /*  float* Kinv_ptr=Kinv.ptr<float>(0);
    
    float ORIG_FX_INV = Kinv_ptr[0];
    float ORIG_CX_INV = Kinv_ptr[2];
    
    float* Kinv_ptr2=Kinv.ptr<float>(1);
    float ORIG_FY_INV = Kinv_ptr2[0];
    float ORIG_CY_INV = Kinv_ptr2[2];
    
    */
    
    static const float weight[]={100000.0f,100000.0f,100000.0f,10000.0f,10000.0f,10000.0f};
    static const float level_term[]={0.999f,1.0f,1.05f,1.1f,1.1f};//{1.1f,1.07f,1.05f,1.0f,0.999f};
    static const float threshold1= 10.0f;
    static const float switchKeyframeThreshold=5.5f;

    static const float MIN_ABS_GRAD_CREATE=1.0f;
    static const float MIN_ABS_GRAD_DECREASE=5.0f;
    static const int MIN_BLACKLIST=-1;
    
    static const float  MAX_DIFF_CONSTANT =40.0f*40.0f;
    static const float MAX_DIFF_GRAD_MULT = 0.5f*0.5f;
    
    static const float VAR_RANDOM_INIT_INITIAL=0.125f;
    
    
// ============== initial stereo pixel selection ======================
    static const float MIN_EPL_GRAD_SQUARED=(2.0f*2.0f);
    static const float MIN_EPL_LENGTH_SQUARED= (1.0f*1.0f);
    static const float MIN_EPL_ANGLE_SQUARED= (0.3f*0.3f);
   
    
// ============== stereo & gradient calculation ======================
    static const float MIN_DEPTH =0.05f ;// this is the minimal depth tested for stereo., using now! 
    
    // particularely important for initial pixel.
    static const float MAX_EPL_LENGTH_CROP =30.0f; // maximum length of epl to search.
    static const float MIN_EPL_LENGTH_CROP =3.0f; // minimum length of epl to search.
    
    // this is the distance of the sample points used for the stereo descriptor.
    static const float GRADIENT_SAMPLE_DIST =1.0f;  //using now!
    
    // pixel a point needs to be away from border... if too small: segfaults!
    static const float SAMPLE_POINT_TO_BORDER= 7.0f;  //using now ; MADE FLOAT
    
    // pixels with too big an error are definitely thrown out.
    static const float MAX_ERROR_STEREO =1300.0f; // maximal photometric error for stereo to be successful (sum over 5 squared intensity differences)
    static const float MIN_DISTANCE_ERROR_STEREO =1.5f; // minimal multiplicative difference to second-best match to not be considered ambiguous.
    
    // defines how large the stereo-search region is. it is [mean] +/- [std.dev]*STEREO_EPL_VAR_FAC
    static const float STEREO_EPL_VAR_FAC= 2.0f;
    
    static const float DIVISION_EPS =1e-10f;
    
    /// to be understood///////////////////////
    static const int CAMERA_PIXEL_NOISE=4*4;
    static const float VAR=0.5f*0.5f;
    static const int VALIDITY_COUNTER_INITIAL_OBSERVE=5;	// initial validity for first observations
    
    static const float SUCC_VAR_INC_FAC=(1.01f); // before an ekf-update, the variance is increased by this factor.
    static const float FAIL_VAR_INC_FAC=1.1f; // after a failed stereo observation, the variance is increased by this factor.
    static const float MAX_VAR=(0.5f*0.5f); // initial variance on creation - if variance becomes larter than this, hypothesis is removed.

    
    static const float VAR_GT_INIT_INITIAL=0.01f*0.01f;	// initial variance vor Ground Truth Initialization
    
    static const float DIFF_FAC_OBSERVE=(1.0f*1.0f);
    static const float DIFF_FAC_PROP_MERGE= (1.0f*1.0f);

    static const float VALIDITY_COUNTER_MAX=(5.0f);
    static const float VALIDITY_COUNTER_MAX_VARIABLE=(250.0f);
    static const float VALIDITY_COUNTER_DEC=5.0f;
    static const float VALIDITY_COUNTER_INC=5.0f;
    static const float REFERANCE_FRAME_MAX_AGE= 10.0f;

    static const float IDEPTH_MULIPLIER=100;
    //static const float IDEPTH_MULIPLIER=70;
    
    static const float VAL_SUM_MIN_FOR_CREATE= 30.0f;
    static const float VAL_SUM_MIN_FOR_UNBLACKLIST=100.0f;
    static const float VAL_SUM_MIN_FOR_KEEP=24.0f;
    
    static const float REG_DIST_VAR= 0.075f*0.075f*1.0f*1.0f;
    static const float DIFF_FAC_SMOOTHING=1.0f*1.0f;
    
    static const float CAMERA_PIXEL_NOISE_2=4.0f*4.0f;
    static const float HUBER_D=3.0f;
    
    



// ================== MACROS ============================================
    #define UNZERO(val) (val < 0 ? (val > -1e-10 ? -1e-10 : val) : (val < 1e-10 ? 1e-10 : val))
}
    
    
#endif
