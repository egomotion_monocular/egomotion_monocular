//
//  Pyramid.cpp
//  odometry
//
//  Created by Himanshu Aggarwal on 9/4/15.
//  Copyright (c) 2015 Himanshu Aggarwal. All rights reserved.
//

#include "Pyramid.h"
#include "UserDefinedFunc.h"
#include "ExternVariable.h"
#include "DisplayFunc.h"

using namespace cv;

Pyramid::Pyramid(frame* prevframe,frame* currentframe,float* pose,depthMap* currDepthMap){

    currentDepthMap=currDepthMap;
    prev_frame=prevframe;
    current_frame=currentframe;
    this->pose=pose;
    steepestDescent=Mat(prev_frame->no_nonZeroDepthPts,6,CV_32FC1);//steepest descent matrix
    saveImg= Mat(1, prev_frame->no_nonZeroDepthPts, CV_32FC1); //save intensity of image at non zero depth points for current pyramid level
    worldPoints=Mat(3,prev_frame->no_nonZeroDepthPts,CV_32FC1); //saves world points to be warped with estimate of e
    transformedWorldPoints=Mat(3,prev_frame->no_nonZeroDepthPts,CV_32FC1); //saves world points to be warped with estimate of e
    warpedPoints= Mat(2,prev_frame->no_nonZeroDepthPts, CV_32FC1);//stores warped points
    warpedImage=Mat::zeros(1,prev_frame->no_nonZeroDepthPts,CV_32FC1);
    warpedGradientx=Mat(1,prev_frame->no_nonZeroDepthPts, CV_32FC1);//gradientx for current img
    warpedGradienty=Mat(1,prev_frame->no_nonZeroDepthPts, CV_32FC1);//gradienty for current img
    //weight_diag=Mat::zeros(prevframe->no_nonZeroDepthPts, prev_frame->no_nonZeroDepthPts, CV_32FC1); //diag matrix of wts.
    weights=cv::Mat(1,prev_frame->no_nonZeroDepthPts, CV_32FC1);//weights for residual
    
    // FOR PIXEL WISE
    
    trfmworld_x.reserve(prev_frame->no_nonZeroDepthPts);//[prev_frame->no_nonZeroDepthPts];
    trfmworld_y.reserve(prev_frame->no_nonZeroDepthPts);
    trfmworld_z.reserve(prev_frame->no_nonZeroDepthPts);
    warped_u.reserve(prev_frame->no_nonZeroDepthPts);
    warped_v.reserve(prev_frame->no_nonZeroDepthPts);
    residual1.reserve(prev_frame->no_nonZeroDepthPts);
    weights1.reserve(prev_frame->no_nonZeroDepthPts);
    gx.reserve(prev_frame->no_nonZeroDepthPts);
    gy.reserve(prev_frame->no_nonZeroDepthPts);

}

/*
 FUNCTION: CALCULATE STEEPEST DESCENT
 
 PURPOSE: RETURNS STEEPEST DESCENT MATRIX FOR A FRAME AT A PARTICULAR PYRAMID LEVEL AND POPULATES WORLD POINT MATRIX WITH IMAGE COORDINATES         AND DEPTH
 INPUTS: DEPTH MATRIX, MASK OF DEPTH MATRIX, GRADIENT IN X DIRECTION OF FRAME, GRADIENT IN Y DIRECTION OF FRAME, WORLD POINT MATRIX
 NUMBER OF ROWS, NUMBER OF COLUMNS, PYRAMID LEVEL
 OUTPUT: STEEPEST DESCENT MATRIX
 */

void Pyramid::calculateSteepestDescent()
{
    PRINTF("\nCalculating Steepest Descent for frame: %d ", prev_frame->frameId);
    int nRows=prev_frame->currentRows;
    int nCols=prev_frame->currentCols;
    
    //*******INITIALIZE VAR*******//
    
    Mat jacobian_top(prev_frame->no_nonZeroDepthPts,6, CV_32FC1);
    Mat jacobian_bottom(prev_frame->no_nonZeroDepthPts,6, CV_32FC1);
    
    //cout<<"\n\n\nGRADIENT POINTER:  "<<(gradx.type()==CV_32FC1);
    
    
    //calculate resized intrinsic focal parameters for pyrlevel
    vector<float> resized_intrinsic= GetIntrinsic(prev_frame->pyrLevel);
    float resized_fx=resized_intrinsic[0];
    float resized_fy=resized_intrinsic[1];
    float resized_cx=resized_intrinsic[2];
    float resized_cy=resized_intrinsic[3];
    
    
    //*******INITIALIZE POINTERS*******//
    
    //pointers to access elements
    float* depth_ptr;
    float* jacob_top_ptr;
    float* jacob_bottom_ptr;
    uchar* mask_ptr;
    
    
    int jac_rows;
    //check if matrix stored continuously
    if(jacobian_top.isContinuous() & jacobian_bottom.isContinuous())
        jac_rows= -1;
    else
        jac_rows= 0;
    
    int jac_counter =0;
    
    jacob_top_ptr = jacobian_top.ptr<float>(0);
    jacob_bottom_ptr = jacobian_bottom.ptr<float>(0);
    
    //*******LOOP TO CALCULATE JACOBIAN*******//
    
    int i,j; //loop variables
    int idx=0;
    float* x_warped=warpedPoints.ptr<float>(0);
    float* y_warped=warpedPoints.ptr<float>(1);
    float* gradxwarp=warpedGradientx.ptr<float>(0);
    float* gradywarp=warpedGradienty.ptr<float>(0);

    float gradx,grady;
    for( i = 0; i < nRows; ++i)
    {
        
        depth_ptr = prev_frame->depth_pyramid[prev_frame->pyrLevel].ptr<float>(i);
        
        
        mask_ptr=prev_frame->mask.ptr<uchar>(i);
        
        for ( j = 0; j < nCols; ++j)
        {
        
            
            if(mask_ptr[j]==0)
                continue;  //skip loop for non zero depth points
            gradx=current_frame->getInterpolatedElement(x_warped[idx], y_warped[idx], "gradx");
            grady=current_frame->getInterpolatedElement(x_warped[idx], y_warped[idx], "grady");
            
            gradxwarp[idx]=gradx;
            gradywarp[idx]=grady;
            //Calculate value of Jacobian for a point
            
            jacob_bottom_ptr[jac_counter]  = grady*(-(resized_fy + (pow((-resized_cy + i),2) / resized_fy)));
            jacob_top_ptr[jac_counter++] = gradx*(-((-resized_cy + i)*(-resized_cx + j)) / resized_fy);
            
            jacob_bottom_ptr[jac_counter]=grady*(((-resized_cy + i)*(-resized_cx + j)) / resized_fx);
            jacob_top_ptr[jac_counter++] = gradx*(resized_fx + (pow((-resized_cx + j),2) / resized_fx));
            
            jacob_bottom_ptr[jac_counter]=grady*((resized_fy*(-resized_cx + j)) / resized_fx);
            jacob_top_ptr[jac_counter++]  = gradx*(-(resized_fx*(-resized_cy + i) / resized_fy));
            
            jacob_bottom_ptr[jac_counter]=0;
            jacob_top_ptr[jac_counter++]  =gradx*( resized_fx*(pow(depth_ptr[j],-1)));
            
            jacob_bottom_ptr[jac_counter]= grady*(resized_fy*(pow(depth_ptr[j],-1)));
            jacob_top_ptr[jac_counter++]  = 0;
            
            jacob_bottom_ptr[jac_counter]= grady*(-(-resized_cy + i) *(pow(depth_ptr[j],-1)));
            jacob_top_ptr[jac_counter++]  = gradx*(-(-resized_cx + j) *(pow(depth_ptr[j],-1)));
            
            
            if(jac_rows>-1)
            {   jac_rows=jac_rows+1;
                jacob_bottom_ptr=jacobian_bottom.ptr<float>(jac_rows);
                jacob_top_ptr=jacobian_top.ptr<float>(jac_rows);
                jac_counter=0;
            }
            idx++;
        }
    }
    //*******CALCULATE STEEPEST DESCENT*******//
    
   
    steepestDescent=jacobian_top+jacobian_bottom;
    //cout<<"\nJacobian Top: \n"<<jacobian_top;
    //cout<<"\nJacobian bottom: \n"<<jacobian_bottom;
    
    return;
    
}



/*
 FUNCTION: CALCULATE HESSIAN INVERSE
 
 PURPOSE: RETURNS HESSIAN INVERSE MATRIX FOR A FRAME
 INPUTS: STEEPEST DESCENT  MATRIX
 OUTPUTS: HESSIAN INVERSE MATRIX
 */

void Pyramid::calculateHessianInv()
{
    PRINTF("\nCalculating Hessian ");
    float* weight_ptr=weights.ptr<float>(0);
    /*
    float* weight_diag_ptr=weight_diag.ptr<float>(0);

    
    for(int y=0; y<prev_frame->no_nonZeroDepthPts; y++)
    {
        weight_diag_ptr=weight_diag.ptr<float>(y);
        weight_diag_ptr[y]=weight_ptr[y];

    }*/ 
    
    Mat temp= steepestDescent.t();
    
    weight_ptr=weights.ptr<float>(0);
    float* temp_ptr0=temp.ptr<float>(0);
    float* temp_ptr1=temp.ptr<float>(1);
    float* temp_ptr2=temp.ptr<float>(2);
    float* temp_ptr3=temp.ptr<float>(3);
    float* temp_ptr4=temp.ptr<float>(4);
    float* temp_ptr5=temp.ptr<float>(5);
    
    for(int y=0; y<prev_frame->no_nonZeroDepthPts; y++)
    {
       
       /* if(current_frame->frameId ==9 && current_frame->pyrLevel==2 && y==3523)
        {
            printf("\nWeights : \n  %f", weight_ptr[y] );
            printf("\nTemp: \n  %f, %f, %f, %f, %f, %f",  temp_ptr0[y],  temp_ptr1[y],  temp_ptr2[y],  temp_ptr3[y],  temp_ptr4[y],  temp_ptr5[y]);
        } */

        
        temp_ptr0[y]=temp_ptr0[y]*weight_ptr[y];
        temp_ptr1[y]=temp_ptr1[y]*weight_ptr[y];
        temp_ptr2[y]=temp_ptr2[y]*weight_ptr[y];
        temp_ptr3[y]=temp_ptr3[y]*weight_ptr[y];
        temp_ptr4[y]=temp_ptr4[y]*weight_ptr[y];
        temp_ptr5[y]=temp_ptr5[y]*weight_ptr[y];
        
        
        
    }
    
   // cout<<"\nWeight matrix: \n"<<weights;
    
    
    
    Mat hessian = Mat::zeros(6, 6, CV_32FC1);
    temp=temp.t();
    
    //cout<<"\nNO OF NON ZER: "<<prev_frame->no_nonZeroDepthPts;
    
    float* temp_ptr = temp.ptr<float>(0);
    float* steep_ptr=steepestDescent.ptr<float>(0);
    for(int y=0; y<prev_frame->no_nonZeroDepthPts; y++ )
    {
        temp_ptr = temp.ptr<float>(y);
        steep_ptr=steepestDescent.ptr<float>(y);
        Mat temp_T= (Mat_<float>(6,1)<<temp_ptr[0], temp_ptr[1],temp_ptr[2], temp_ptr[3], temp_ptr[4], temp_ptr[5] );
        Mat steep_T=(Mat_<float>(1,6)<<steep_ptr[0], steep_ptr[1], steep_ptr[2], steep_ptr[3], steep_ptr[4], steep_ptr[5]);
        hessian+= (temp_T*steep_T);
        
    }
    
    hessianInv=hessian.inv();
    
    /*
    Mat hessian =temp*steepestDescent;
    hessianInv= hessian.inv();*/
    
    //cout<<"\n HESSIAN : \n"<<hessian;
    return;
    
}

void Pyramid::calculateWorldPoints()
{
    PRINTF("\nCalculating World Points for previous image: %d", prev_frame->frameId);
    int pyrlevel=prev_frame->pyrLevel;
   int nRows=prev_frame->currentRows;
    int nCols=prev_frame->currentCols;
    //*******INITIALIZE POINTERS*******//
    
    //worldpoint pointers
    float* worldpoint_ptr0; //pointer to access row 0
    float* worldpoint_ptr1; //pointer to access row 1
    float* worldpoint_ptr2; //pointer to access row 2
    uchar* img_ptr;
    float* saveimg_ptr;
    uchar* mask_ptr;
    float* depth_ptr;
    
    worldpoint_ptr0=worldPoints.ptr<float>(0); //initialize to row0
    worldpoint_ptr1=worldPoints.ptr<float>(1); //initialize to row1
    worldpoint_ptr2=worldPoints.ptr<float>(2); //initialize to row2
    
    saveimg_ptr=saveImg.ptr<float>(0); //initialize to row 0 (single row Mat)
    
    //*******CALCULATE RESIZED INTRINSIC PARAMETERS*******//
    
    vector<float> resized_intrinsic= GetIntrinsic(prev_frame->pyrLevel);
    float resized_fx=resized_intrinsic[0];
    float resized_fy=resized_intrinsic[1];
    float resized_cx=resized_intrinsic[2];
    float resized_cy=resized_intrinsic[3];
    
    //*******INITIALIZE COUNTERS*******//
    
    int world_rows0=0; //counter to access columns in row 0
    int world_rows1=0; //counter to access columns in row 1
    int world_rows2=0; //counter to access columns in row 2
    int saveimg_row=0; //counter to store in ro0 of saveimg
    
    int i,j;//loop variables
    for(i = 0; i < nRows; ++i)
    {
        
        //get pointer for current row i;
        img_ptr = prev_frame->image_pyramid[pyrlevel].ptr<uchar>(i);
        mask_ptr=prev_frame->mask.ptr<uchar>(i);
        depth_ptr = prev_frame->depth_pyramid[pyrlevel].ptr<float>(i);
        
        for (j = 0; j < nCols; ++j)
            
        {   //check for zero depth point
            if(mask_ptr[j]==0)
                continue;  //skip loop for non zero depth points
            
            //save image intensity at non zero depth point
            saveimg_ptr[saveimg_row++]=img_ptr[j];
            
            //populating world point with homogeneous coordinate ( X ;Y ;depth ; 1)
            worldpoint_ptr0[world_rows0++]=(j-resized_cx)*depth_ptr[j]/resized_fx; //X-coordinate
            worldpoint_ptr1[world_rows1++]=(i-resized_cy)*depth_ptr[j]/resized_fy; //Y-coordinate
            worldpoint_ptr2[world_rows2++]=depth_ptr[j]; //depth of point at (u,v)
            //worldpoint_ptr3[world_rows3++]=1; //1 for making homogeneous
            
        }
    }
    return;
}

/*
 FUNCTION: CALCULATE WARPED POINTS
 
 PURPOSE: CALCULATES THE POSE MATRIX AND WARPED POINTS FOR THE POSE
 INPUT: POSE VECTOR, WORLDPOINT MATRIX, NUMBER OF NON ZERO DEPTH POINTS, PYRAMID LEVEL
 OUTPUT: WARPED POINT MATRIX
 */

void Pyramid::calculateWarpedPoints ()
{
    PRINTF("\nCalculating warped points for previous frame: %d ", prev_frame->frameId);
    //*******CALCULATE POSE MATRIX *******//
    
    int nonZeroPts=prev_frame->no_nonZeroDepthPts;
    
    //Create matrix in OpenCV
    Mat se3=(Mat_<float>(4, 4) << 0,-pose[2],pose[1],pose[3], pose[2],0,-pose[0],pose[4], -pose[1],pose[0],0,pose[5],0,0,0,0);
    
    // Map the OpenCV matrix with Eigen:
    Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> se3_Eigen(se3.ptr<float>(), se3.rows, se3.cols);
    
    // Take exp in Eigen and store in new Eigen matrix
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> SE3_Eigen = se3_Eigen.exp();
    
    // create an OpenCV Mat header for the Eigen data:
    Mat SE3(4, 4, CV_32FC1, SE3_Eigen.data());
    
    //cout<<"\n\nSE3"<<SE3;
    
    
       
    //cout<<"\n\nSE3:  "<<SE3;
    
    //*******INITIALIZE POINTERS*******//
    
    //warpedpoint pointers
    float* warpedpoint_ptrx; //pointer to access row 0=> x image coordinate
    float* warpedpoint_ptry; //pointer to access row 1=> y image coordinate
    //worldpoint pointers
    float* worldpoint_ptrX; //pointer to access row 0=> X
    float* worldpoint_ptrY; //pointer to access row 1=> Y
    float* worldpoint_ptrZ; //pointer to access row 2=> Z
    float* trfm_worldpoint_ptrX;
    float* trfm_worldpoint_ptrY;
    float* trfm_worldpoint_ptrZ;

    
    warpedpoint_ptrx=warpedPoints.ptr<float>(0); //initialize to row0
    warpedpoint_ptry=warpedPoints.ptr<float>(1); //initialize to row1
    
    worldpoint_ptrX=worldPoints.ptr<float>(0); //initialize to row0
    worldpoint_ptrY=worldPoints.ptr<float>(1); //initialize to row1
    worldpoint_ptrZ=worldPoints.ptr<float>(2); //initialize to row2
    
    trfm_worldpoint_ptrX=transformedWorldPoints.ptr<float>(0); //initialize to row0
    trfm_worldpoint_ptrY=transformedWorldPoints.ptr<float>(1); //initialize to row1
    trfm_worldpoint_ptrZ=transformedWorldPoints.ptr<float>(2); //initialize to row2

    
    //*******CALCULATE RESIZED INTRINSIC PARAMETERS*******//
    
    vector<float> resized_intrinsic= GetIntrinsic(prev_frame->pyrLevel);
    float resized_fx=resized_intrinsic[0];
    float resized_fy=resized_intrinsic[1];
    float resized_cx=resized_intrinsic[2];
    float resized_cy=resized_intrinsic[3];
    
    //*******STORE SE3 PARAMETERS*******//
    
    float* SE3_ptr;
    float SE3_vec[12];  //r11, r12, r13, t1, r21 r22, r23, t2, r31, r32, r33, t3
    int vec_counter=0;
    int i; //loop variables
    
    for(i=0; i<3; ++i)
    {
        SE3_ptr=SE3.ptr<float>(i); //get pointer to first row of SE3
        
        SE3_vec[vec_counter++]=SE3_ptr[0];
        SE3_vec[vec_counter++]=SE3_ptr[1];
        SE3_vec[vec_counter++]=SE3_ptr[2];
        SE3_vec[vec_counter++]=SE3_ptr[3];
        
    }
    
    // cout<<"\n\nSE3 VECTOR  "<<SE3_vec[0]<<" : "<< SE3_vec[1]<<" : "<<SE3_vec[2]<<" : "<<SE3_vec[3]<<" : "<<SE3_vec[4]<<" : "<<SE3_vec[5]<<" : "<<SE3_vec[6]<<" : "<<SE3_vec[7]<<" : "<<SE3_vec[8]<<" : "<<SE3_vec[9]<<" : "<<SE3_vec[10]<<" : "<<SE3_vec[11];
    
    //enter loop to calculate warped points
    int j;
    
    for( j = 0; j < nonZeroPts; ++j)
    {
        
        
        if (SE3_vec[1]==0)
        {
            trfm_worldpoint_ptrX[j]=((SE3_vec[0]*worldpoint_ptrX[j])+(SE3_vec[1]*worldpoint_ptrY[j])+(SE3_vec[2]*worldpoint_ptrZ[j])+(SE3_vec[3]));
            trfm_worldpoint_ptrY[j]=((SE3_vec[4]*worldpoint_ptrX[j])+(SE3_vec[5]*worldpoint_ptrY[j])+(SE3_vec[6]*worldpoint_ptrZ[j])+(SE3_vec[7]));
            trfm_worldpoint_ptrZ[j]=((SE3_vec[8]*worldpoint_ptrX[j])+(SE3_vec[9]*worldpoint_ptrY[j])+(SE3_vec[10]*worldpoint_ptrZ[j])+(SE3_vec[11]));
            
            warpedpoint_ptrx[j]=((trfm_worldpoint_ptrX[j]/trfm_worldpoint_ptrZ[j])*resized_fx)+resized_cx;
            
            warpedpoint_ptry[j]=((trfm_worldpoint_ptrY[j]/trfm_worldpoint_ptrZ[j])*resized_fy)+resized_cy;
            
        }
        
        else
        {
            trfm_worldpoint_ptrX[j]=(float(SE3_vec[0]*worldpoint_ptrX[j])+float(SE3_vec[1]*worldpoint_ptrY[j])+float(SE3_vec[2]*worldpoint_ptrZ[j])+float(SE3_vec[3]));
            trfm_worldpoint_ptrY[j]=(float((SE3_vec[4]*worldpoint_ptrX[j]))+float((SE3_vec[5]*worldpoint_ptrY[j]))+float((SE3_vec[6]*worldpoint_ptrZ[j]))+float((SE3_vec[7])));
            trfm_worldpoint_ptrZ[j]=(float(SE3_vec[8]*worldpoint_ptrX[j])+float(SE3_vec[9]*worldpoint_ptrY[j])+float(SE3_vec[10]*worldpoint_ptrZ[j])+float(SE3_vec[11]));
            
            warpedpoint_ptrx[j]=float(((trfm_worldpoint_ptrX[j]/trfm_worldpoint_ptrZ[j])*resized_fx)+resized_cx);
            
            warpedpoint_ptry[j]=float(((trfm_worldpoint_ptrY[j]/trfm_worldpoint_ptrZ[j])*resized_fy)+resized_cy);
            

            
        }
        
    }
    
    
    return;
}


/*
 FUNCTION: CALCULATE WARPED IMAGE
 
 PUROSE: CALCULATES THE INTERPOLATED WAARPED IMAGE POINTS
 INPUT: WARPEDPOINT MATRIX, ORIGINAL IMAGE MATRIX, NUMBER OF ROWS, NUMBER OF COLUMNS, NUMBER OF NON ZERO DEPTH POINTS
 OUTPUT: WARPED IMAGE MATRIX
 */

void Pyramid::calculateWarpedImage()
{
    PRINTF("\nCalculating Warped Image for previous frame: %d, current frame: %d", prev_frame->frameId, current_frame->frameId);
    int nonZeroPts=prev_frame->no_nonZeroDepthPts;
    int pyrlevel=prev_frame->pyrLevel;
    
    
    //*******INITIALIZE POINTERS*******//
    
    //initialize pointers
    uchar* img_ptr0; //pointer0 to access original image
    uchar* img_ptr1; //pointer1 to acess original image
    float* warpedimg_ptr; //pointer to store in warped image
    float* warpedpoint_ptrx; //pointer to acess warped point x => column
    float* warpedpoint_ptry; //pointer to access warped point y=> row
    
    warpedpoint_ptrx=warpedPoints.ptr<float>(0); //initialize to row0 to acess x coordinate
    warpedpoint_ptry=warpedPoints.ptr<float>(1); //initialize to row1 to acess y coordinate
    warpedimg_ptr=warpedImage.ptr<float>(0); //initialize to store in row0 of warped image
    
    //*******DECLARE VARIABLES*******//
    
    //cout<<"\n\nWARPED POINTS   "<<warpedpoint;
    
    
    int j; //for loop variables
    float yx[2]; //to store warped points for current itertion
    float wt[2]; //to store weight
    float y,x; //to store x and y coordinate to acess original image
    uchar pixVal1, pixVal2; //to store intensity value
    float interTop, interBtm;
    
    int nCols=prev_frame->currentCols-1; //maximum valid x coordinate
    int nRows=prev_frame->currentRows-1; //maximum valid y coordinate
    
    for(j=0; j<nonZeroPts; j++ )
    {
        int boundCheck=0;
        yx[0]=warpedpoint_ptry[j]; //store current warped point y
        yx[1]=warpedpoint_ptrx[j]; //store warped point x
        
        
        wt[0]=yx[0]-floor(yx[0]); //weight for y
        wt[1]=yx[1]-floor(yx[1]); //weight for x
        
        //Case 1
        y=floor(yx[0]); //floor of y
        x=floor(yx[1]); //floor of x
        
        //cout<<"\n\n"<<int(x);
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        { pixVal1=0; //if outside boundary pixel value is 0
            boundCheck++;
        }
        else
        {
            img_ptr0=current_frame->image_pyramid[pyrlevel].ptr<uchar>(y); //initialize image pointer0 to row floor(y)
            pixVal1=img_ptr0[int(x)]; //move pointer to get value at pixel (floor(x), floor(y))
        }
        
        
        
        x=yx[1]; //warped point x
        
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        {pixVal2=0; //if outside boundary pixel value is 0
            boundCheck++;
        }
        else
        {
            img_ptr0=current_frame->image_pyramid[pyrlevel].ptr<uchar>(y); //initialize image pointer0 to row floor(y)
            pixVal2=img_ptr0[int(ceil(x))]; //move pointer to get value at pixel (ceil(x), floor(y))
        }
        
        interTop=((1-wt[1])*pixVal1)+(wt[1]*pixVal2);
        
        
        //Case 2
        y=yx[0]; //warped point y
        
        x=floor(yx[1]); //floor of x
        
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        { pixVal1=0; //if outside boundary pixel value is 0
            boundCheck++;
            
        }
        else
        {
            img_ptr1=current_frame->image_pyramid[pyrlevel].ptr<uchar>(ceil(y)); //initialize image pointer1 to row ceil(y)
            pixVal1=img_ptr1[int(x)]; //move pointer to get value at pixel (floor(x), ceil(y))
        }
        
        x=yx[1]; //warped point x
        
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        {pixVal2=0; //if outside boundary pixel value is 0
            boundCheck++;
        }
        else
        {
            img_ptr1=current_frame->image_pyramid[pyrlevel].ptr<uchar>(ceil(y)); //initialize image pointer1 to row ceil(y)
            pixVal2=img_ptr1[int(ceil(x))]; //move pointer to get value at pixel (ceil(x), ceil(y))
        }
       /*
        if (boundCheck==4) {
            warpedimg_ptr[j]=-1.0f;
        }
       
        else{
        */
        interBtm=((1-wt[1])*pixVal1)+(wt[1]*pixVal2);
        
        warpedimg_ptr[j]=((1-wt[0])*interTop)+(wt[0]*interBtm); //calculate interpolated value to get warped image intensity
            cout<<"\n"<<warpedimg_ptr[j];
        //}
    }
    //cout<<warpedImage<<endl<<endl;
    //waitKey(0);
    
    return;
    
}

/*
 FUNCTION: UPDATE POSE
 
 PURPOSE: CALCULATE RESIDUAL,  UPDATE CURRENT ESTIMATE OF POSE VECTOR AND CHECK CONDITION FOR TERMINATION OF ITERATION LOOP
 INPUT: SAVED ORIGINAL IMAGE OF NON ZERO DEPTH POINTS, WARPED IMAGE , STEEPEST DESCENT MATRIX, POSE POINTER, NUMBER OF NON ZERO DEPTH POINTS,
 WEIGHT POINTER OF POSE PARAMETERS, THRESHOLD VALUE TO TERMINATE LOOP
 OUTPUT: RETURNS -1 IF LOOP NEEDS TO BE TERMINATED OTHERWISE 0
 */

void Pyramid::updatePose()
{
    PRINTF("\nUpdating pose ");
    //residual=warpedImage-saveImg; //calculate residual
    
    //cout<<"\n\n\n"<<residual;
    
    
    Mat temp=residual.mul(weights);
    
    Mat sd_param= temp*steepestDescent; //calculate steepest descnet parameters
    
    
    //cout<<"\nsd param: \n "<<sd_param;
    
    //cout<<steepdesc;
    Mat deltapose= ((hessianInv*(sd_param.t())).t()); //calculate delta pose (change in pose)
    deltapose=-deltapose;
    
    float* deltapose_ptr=deltapose.ptr<float>(0); //initialize pointer to delta pose
    
    //printf("\nDelta Pose: %f , %f , %f , %f , %f , %f", deltapose_ptr[0],deltapose_ptr[1],deltapose_ptr[2],deltapose_ptr[3],deltapose_ptr[4],deltapose_ptr[5]);
    
    //cout<<"\nWeighted Pose in func "<<abs(deltapose_ptr[0]*util::weight[0])<<" , "<<abs(deltapose_ptr[1]*util::weight[1])<<" , "<<abs(deltapose_ptr[2]*util::weight[2])<<" , "<<abs(deltapose_ptr[3]*util::weight[3])<<" , "<<abs(deltapose_ptr[4]*util::weight[4])<<" , "<<abs(deltapose_ptr[5]*util::weight[5])<<" , ";
    
    
    //calculate weighted pose value
    float weighted_pose = abs(deltapose_ptr[0]*util::weight[0])+abs(deltapose_ptr[1]*util::weight[1])+abs(deltapose_ptr[2]*util::weight[2])+abs(deltapose_ptr[3]*util::weight[3])+abs(deltapose_ptr[4]*util::weight[4])+abs(deltapose_ptr[5]*util::weight[5]);
    
    //printf("Total Weighted Pose: %f ",weighted_pose);
    weightedPose=weighted_pose;
    
    //check condition for termination
    /*if( weighted_pose < util::threshold1)
    {
        PRINTF("\nWeighted pose below threshold! Terminating");
        return -1;  //terminate iteration loop
    }*/
    
    
    //update current estimate of pose
    pose[0]+=real(deltapose_ptr[0]);
    pose[1]+=real(deltapose_ptr[1]);
    pose[2]+=real(deltapose_ptr[2]);
    pose[3]+=real(deltapose_ptr[3]);
    pose[4]+=real(deltapose_ptr[4]);
    pose[5]+=real(deltapose_ptr[5]);
    
    
    
    // cout<<"\n\nPose in func "<<pose[0]<<" , "<<pose[1]<<" , "<<pose[2]<<" , "<<pose[3]<<" , "<<pose[4]<<" , "<<pose[5]<<" , ";
    //cout<<"\nDelta pose: "<<deltapose;
    return;
    
}




float Pyramid::calResidualAndWeights()
{
    PRINTF("\nCalculating weights and residual");
    vector<float> resized_intrinsic= GetIntrinsic(prev_frame->pyrLevel);
    float resized_fx=resized_intrinsic[0];
    float resized_fy=resized_intrinsic[1];
    float resized_cx=resized_intrinsic[2];
    float resized_cy=resized_intrinsic[3];
    
    float weightedSumOfResidual=0.0f;
    Mat warpedImgmask=warpedImage>=0.0f;
    warpedImgmask=warpedImgmask/255;
    
    //cout<<warpedImgmask<<endl;
    
    //waitKey(0);
    residual=warpedImage-saveImg;//calculate residual
    //cout<<residual<<endl;
    //waitKey(0);
    PRINTF("Finished printing unmodified residual\n\n\n");
    //residual=residual*warpedImgmask;
    residual.mul(warpedImgmask);
    //cout<<residual<<endl;
    float* trfmpoint_x=transformedWorldPoints.ptr<float>(0);
    float* trfmpoint_y=transformedWorldPoints.ptr<float>(1);
    float* trfmpoint_z=transformedWorldPoints.ptr<float>(2);
    
    uchar* maskptr=prev_frame->mask.ptr<uchar>(0);
    
    float* weightptr=weights.ptr<float>(0);
    float* residualptr=residual.ptr<float>(0);
    float* depth_ptr=prev_frame->depth_pyramid[prev_frame->pyrLevel].ptr<float>(0);
    
    float* gradxwarp=warpedGradientx.ptr<float>(0);
    float* gradywarp=warpedGradienty.ptr<float>(0);
    
    int idx=0;
   
    Mat se3=(Mat_<float>(4, 4) << 0,-pose[2],pose[1],pose[3], pose[2],0,-pose[0],pose[4], -pose[1],pose[0],0,pose[5],0,0,0,0);
    
    // Map the OpenCV matrix with Eigen:
    Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> se3_Eigen(se3.ptr<float>(), se3.rows, se3.cols);
    
    // Take exp in Eigen and store in new Eigen matrix
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> SE3_Eigen = se3_Eigen.exp();
    
    
    
    //float tx=current_frame->SE3poseThisWrtOther_t(0,0);
    //float ty=current_frame->SE3poseThisWrtOther_t(1,0);
    //float tz=current_frame->SE3poseThisWrtOther_t(2,0);
    
    float tx=SE3_Eigen(0,3);
    float ty=SE3_Eigen(1,3);
    float tz=SE3_Eigen(2,3);
    
    int width= prev_frame->currentCols;
    
    float usageCount=0;
    for(int y=0;y<prev_frame->currentRows;y++)
    {
        maskptr=prev_frame->mask.ptr<uchar>(y);
        depth_ptr=prev_frame->depth_pyramid[prev_frame->pyrLevel].ptr<float>(y);
        
        for (int x=0; x<prev_frame->currentCols; x++)
        {
            if(maskptr[x]==0)continue;
            
            float px = trfmpoint_x[idx];	// x'
            float py = trfmpoint_y[idx];	// y'
            float pz = trfmpoint_z[idx];	// z'
            float d = 1.0f/depth_ptr[x];	// d
            float rp = residualptr[idx]; // r_p
            float gx = resized_fx * gradxwarp[idx];	// \delta_x I
            float gy = resized_fy * gradywarp[idx];  // \delta_y I
            float s = 1.0f * (*(currentDepthMap->depthvararrptr[prev_frame->pyrLevel]+x+width*y));	// \sigma_d^2
            // calc dw/dd (first 2 components):
            
            float g0 = (tx * pz - tz * px) / (pz*pz*d);
            float g1 = (ty * pz - tz * py) / (pz*pz*d);
            
            
            // calc w_p
            float drpdd = gx * g0 + gy * g1;	// ommitting the minus
            float w_p = 1.0f / (util::CAMERA_PIXEL_NOISE_2 + s * drpdd * drpdd);
            float weighted_rp = fabs(rp*sqrtf(w_p));
            
            float wh = fabs(weighted_rp < (util::HUBER_D/2) ? 1 : (util::HUBER_D/2) / weighted_rp);
            
            //sumRes += wh * w_p * rp*rp;
            
            weightptr[idx] = wh * w_p;
            weightedSumOfResidual+=weightptr[idx]*rp*rp;
            
            
            idx++;
            float depthChange = 1.0f/ d*pz;	// if depth becomes larger: pixel becomes "smaller", hence count it less.
            usageCount += depthChange < 1 ? depthChange : 1;
            
        }
        
        
    }
    pointUsage=usageCount/float(prev_frame->no_nonZeroDepthPts);
    PRINTF("\nFinished calculating weights and residual");
    
    return weightedSumOfResidual/float(prev_frame->no_nonZeroDepthPts);
}



void Pyramid::performPrecomputation(){
    
    PRINTF("\nPerforming pre-computation..");
    
    calculateWorldPoints();
    calculateWarpedPoints();
    calculateWarpedImage();
    calculateSteepestDescent();
    lastErr=calResidualAndWeights();

    return;
}

float Pyramid::performIterationSteps(){
    PRINTF("\nPerforming Iteration steps..");
    calculateHessianInv();
    updatePose();
    calculateWarpedPoints();
    calculateWarpedImage();
    calculateSteepestDescent();
    error=calResidualAndWeights();
    //calculateHessianInv();
    //return updatePose();
    //if(error<lastErr)return 0.0f;
    float temp=lastErr;
    lastErr=error;
    return error/temp;
}

void Pyramid::performPixelWise()
{
        A.setZero();
       b.setZero();
    
    float* worldpoint_ptrX; //pointer to access row 0=> X
    float* worldpoint_ptrY; //pointer to access row 1=> Y
    float* worldpoint_ptrZ; //pointer to access row 2=> Z
    


    worldpoint_ptrX=worldPoints.ptr<float>(0); //initialize to row0
    worldpoint_ptrY=worldPoints.ptr<float>(1); //initialize to row1
    worldpoint_ptrZ=worldPoints.ptr<float>(2); //initialize to row2

    
    float* depth_ptr=prev_frame->depth_pyramid[prev_frame->pyrLevel].ptr<float>(0);

    int nonZeroPts=prev_frame->no_nonZeroDepthPts;
    int pyrlevel=prev_frame->pyrLevel;
    int nCols=prev_frame->currentCols-1; //maximum valid x coordinate
    int nRows=prev_frame->currentRows-1; //maximum valid y coordinate

    
    float* saveimg_ptr=saveImg.ptr<float>(0); //initialize to row 0 (single row Mat)
    
    uchar* maskptr=prev_frame->mask.ptr<uchar>(0);
    
    //Create matrix in OpenCV
    Mat se3=(Mat_<float>(4, 4) << 0,-pose[2],pose[1],pose[3], pose[2],0,-pose[0],pose[4], -pose[1],pose[0],0,pose[5],0,0,0,0);
    
    // Map the OpenCV matrix with Eigen:
    Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> se3_Eigen(se3.ptr<float>(), se3.rows, se3.cols);
    
    // Take exp in Eigen and store in new Eigen matrix
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> SE3_Eigen = se3_Eigen.exp();
    
    // create an OpenCV Mat header for the Eigen data:
    Mat SE3(4, 4, CV_32FC1, SE3_Eigen.data());
    
    vector<float> resized_intrinsic= GetIntrinsic(prev_frame->pyrLevel);
    float resized_fx=resized_intrinsic[0];
    float resized_fy=resized_intrinsic[1];
    float resized_cx=resized_intrinsic[2];
    float resized_cy=resized_intrinsic[3];
    
    //*******STORE SE3 PARAMETERS*******//
    
    float* SE3_ptr;
    float SE3_vec[12];  //r11, r12, r13, t1, r21 r22, r23, t2, r31, r32, r33, t3
    int vec_counter=0;
    int i; //loop variables
    
    for(i=0; i<3; ++i)
    {
        SE3_ptr=SE3.ptr<float>(i); //get pointer to first row of SE3
        
        SE3_vec[vec_counter++]=SE3_ptr[0];
        SE3_vec[vec_counter++]=SE3_ptr[1];
        SE3_vec[vec_counter++]=SE3_ptr[2];
        SE3_vec[vec_counter++]=SE3_ptr[3];
        
    }
   
    /*vector<float> trfmworld_x;
    vector<float> trfmworld_y;
    vector<float> trfmworld_z;
    trfmworld_x.reserve(nonZeroPts);
    trfmworld_y.reserve(nonZeroPts);
    trfmworld_z.reserve(nonZeroPts);
    vector<float> warped_u;
    vector<float> warped_v;
    warped_u.reserve(nonZeroPts);
    warped_v.reserve(nonZeroPts);
    vector<float> warped_intensity;
    warped_intensity.reserve(nonZeroPts);

    vector<float> residual;
    residual.reserve(nonZeroPts);
    vector<float> weights;
    weights.reserve(nonZeroPts);
    vector<float> gx;//gradient in x * resized_fx
    vector<float> gy;//gradient in y * resized_fy
    gx.reserve(nonZeroPts);
    gy.reserve(nonZeroPts);*/
    
    vector<int> skippedIdx;
    skippedIdx.reserve(1000);
    
    int idx_orig=0;
    int idx=0;
    
    float working_trfm_x;
    float working_trfm_y;
    float working_trfm_z;
    float working_residual;
    float working_weight;
    float u_new;
    float v_new;
    float gradx_at_warp;
    float grady_at_warp;
    float intensity_at_warp;
    
    for(int y=0;y<prev_frame->currentRows;y++)
    {
        maskptr=prev_frame->mask.ptr<uchar>(y);
        depth_ptr=prev_frame->depth_pyramid[prev_frame->pyrLevel].ptr<float>(y);
        
        for (int x=0; x<prev_frame->currentCols; x++)
        
    {
        if(maskptr[x]==0)continue;
        
        if (SE3_vec[1]==0)
        {
            working_trfm_x=((SE3_vec[0]*worldpoint_ptrX[idx_orig])+(SE3_vec[1]*worldpoint_ptrY[idx_orig])+(SE3_vec[2]*worldpoint_ptrZ[idx_orig])+(SE3_vec[3]));
            working_trfm_y=((SE3_vec[4]*worldpoint_ptrX[idx_orig])+(SE3_vec[5]*worldpoint_ptrY[idx_orig])+(SE3_vec[6]*worldpoint_ptrZ[idx_orig])+(SE3_vec[7]));
            working_trfm_z=((SE3_vec[8]*worldpoint_ptrX[idx_orig])+(SE3_vec[9]*worldpoint_ptrY[idx_orig])+(SE3_vec[10]*worldpoint_ptrZ[idx_orig])+(SE3_vec[11]));
            
            u_new=((working_trfm_x/working_trfm_z)*resized_fx)+resized_cx;
            v_new=((working_trfm_y/working_trfm_z)*resized_fy)+resized_cy;
        }
        
        else
        {
            working_trfm_x=(float(SE3_vec[0]*worldpoint_ptrX[idx_orig])+float(SE3_vec[1]*worldpoint_ptrY[idx_orig])+float(SE3_vec[2]*worldpoint_ptrZ[idx_orig])+float(SE3_vec[3]));
            working_trfm_y=(float((SE3_vec[4]*worldpoint_ptrX[idx_orig]))+float((SE3_vec[5]*worldpoint_ptrY[idx_orig]))+float((SE3_vec[6]*worldpoint_ptrZ[idx_orig]))+float((SE3_vec[7])));
            working_trfm_z=(float(SE3_vec[8]*worldpoint_ptrX[idx_orig])+float(SE3_vec[9]*worldpoint_ptrY[idx_orig])+float(SE3_vec[10]*worldpoint_ptrZ[idx_orig])+float(SE3_vec[11]));
            
            u_new=((working_trfm_x/working_trfm_z)*resized_fx)+resized_cx;
            v_new=((working_trfm_y/working_trfm_z)*resized_fy)+resized_cy;
           
        }
        
        if(!(u_new > 0 && v_new > 0 && u_new <= nCols && v_new <= nRows))
        {
            skippedIdx.push_back(idx_orig);
            idx_orig++;
            continue;
        }
        
        
        
        trfmworld_x.push_back(working_trfm_x);
        trfmworld_y.push_back(working_trfm_y);
        trfmworld_z.push_back(working_trfm_z);
        warped_u.push_back(u_new);
        warped_v.push_back(v_new);

        intensity_at_warp=current_frame->getInterpolatedElement(u_new, v_new);
        gradx_at_warp=current_frame->getInterpolatedElement(u_new, v_new, "gradx");
        grady_at_warp=current_frame->getInterpolatedElement(u_new, v_new, "grady");
        working_residual=saveimg_ptr[idx_orig]-intensity_at_warp;
        residual1.push_back(working_residual);
        gradx_at_warp*=resized_fx;
        grady_at_warp*=resized_fy;
        warped_intensity[idx]=(intensity_at_warp);
        gx.push_back(gradx_at_warp);
        gy.push_back(grady_at_warp);
        float s = 1.0f * (*(currentDepthMap->depthvararrptr[prev_frame->pyrLevel]+x+prev_frame->currentCols*y));	// \sigma_d^2
        float d=worldpoint_ptrZ[idx_orig];

        // calc dw/dd (first 2 components):
        float g0=(SE3_vec[3]*working_trfm_z-SE3_vec[11]*working_trfm_x)/(working_trfm_z*working_trfm_z*d);
        float g1=(SE3_vec[7]*working_trfm_z-SE3_vec[11]*working_trfm_y)/(working_trfm_z*working_trfm_z*d);
        
        // calc w_p
        float drpdd = gradx_at_warp * g0 + grady_at_warp * g1;	// ommitting the minus
        float w_p = 1.0f / (util::CAMERA_PIXEL_NOISE_2 + s * drpdd * drpdd);
        float weighted_rp = fabs(working_residual*sqrtf(w_p));
        
        float wh = fabs(weighted_rp < (util::HUBER_D/2) ? 1 : (util::HUBER_D/2) / weighted_rp);
        working_weight=wh*w_p;
        weights1[idx]=(working_weight);
        
        float z = 1.0f / working_trfm_z;
        float z_sqr = 1.0f / (working_trfm_z*working_trfm_z);
        
        Eigen::Matrix<float,6,1> v;
        v[0] = z*gradx_at_warp + 0;
        v[1] = 0 +         z*grady_at_warp;
        v[2] = (-working_trfm_x * z_sqr) * gradx_at_warp +(-working_trfm_y * z_sqr) * grady_at_warp;
        v[3] = (-working_trfm_x * working_trfm_y * z_sqr) * gradx_at_warp +(-(1.0 + working_trfm_y * working_trfm_y * z_sqr)) * grady_at_warp;
        v[4] = (1.0 + working_trfm_x * working_trfm_x * z_sqr) * gradx_at_warp +(working_trfm_x * working_trfm_y * z_sqr) * grady_at_warp;
        v[5] = (-working_trfm_y * z) * gradx_at_warp +(working_trfm_x * z) * grady_at_warp;
        
        A.noalias() += v * v.transpose() * working_weight;
        b.noalias() -= v * (working_residual * working_weight);
        idx++;
        idx_orig++;

    }
    
  }
    skippedIdxOrig=skippedIdx;
    buf_size=idx;
    
   // util::Vector6 g = -b;
    
    //for(int i=0;i<6;i++) A(i,i) *= 1+LM_lambda;
    //util::Vector6 inc = A.ldlt().solve(g);

    return ;

}

void Pyramid::calculatePose()
{
    util::Vector6 g=-b;
    util::Vector6 inc = A.ldlt().solve(g);
    
}
