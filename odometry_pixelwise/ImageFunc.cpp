//
//  ImageFunc.cpp
//  odometry code 3
//
//  Created by Himani Arora on 13/08/15.
//  Copyright (c) 2015 Himani Arora. All rights reserved.
//

#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>

#include <cstdio>
#include<ctime>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
#include <sstream>

#include "UserDefinedFunc.h"
#include "IterationFunc.h"
#include "ExternVariable.h"
#include "DisplayFunc.h"
#include "Pyramid.h"


#include "ImageFunc.h"

using namespace std;
using namespace cv;


vector<float> GetImagePoseEstimate(frame* prev_frame, frame* current_frame, int frame_num, ofstream *myfile,depthMap* currDepthMap)
{
    PRINTF("\nCalculating Image Pose Estimate using current frame: %d and previous frame: %d", current_frame->frameId ,prev_frame->frameId);
    
    
    //cout<<"\nFrame id: prev , current = "<<prev_frame->frameId<<" , "<<current_frame->frameId;
    
    //current pose
    float pose[6]={0.0f,0.0f,0.0f,0.0f,0.0f,0.0f};


    //w1 w2 w3 v1 v2 v3] //se3 pose vector
    PRINTF("\nInitial Pose estimate: %f , %f , %f , %f , %f , %f", pose[0], pose[1], pose[2], pose[3], pose[4], pose[5]);
    
    //initializing flags for image display
    int display_initial_img_flag=0;
    int display_orig_img_flag=0;
    
    int iter_counter;
    
   /* if (myfile->is_open())
    {
        cout<<"\nyooooo";
        *myfile<<"\n\n\n\n frame: "<<frame_num;
    }
    */
//*******ENTER PYRAMID LOOP*******//

for (int level_counter=(util::MAX_PYRAMID_LEVEL-1); level_counter>=0; level_counter--)
{
    
    //*******PRECOMPUTATION BEGINS*******//

    
    PRINTF("\n\n\nPyramid level: %d",level_counter);
    
    prev_frame->updationOnPyrChange(level_counter);
    current_frame->updationOnPyrChange(level_counter,false);
    // create imstance of Pyramid class
    Pyramid workingPyramid(prev_frame, current_frame, pose,currDepthMap);
    
    //Perform precomputation--Calculation of steepest descent,hessian inverse & world points.
    workingPyramid.performPrecomputation();
    
    display_orig_img_flag=0; //reset flag
    
    
    //*******ENTER ITERATION LOOP*******//
    
    for(iter_counter=0; iter_counter<util::MAX_ITER;  ++iter_counter)
    {
        
        PRINTF("\n\nIteration: %d ",iter_counter);
        
        //Perform iteration steps-- Calculation of warped points,warped image & residual. Also updates pose and checks condition for loop termination.
        
        float check_termination=workingPyramid.performIterationSteps();
        
        if(workingPyramid.weightedPose<1.0f)iter_counter=util::MAX_ITER-1;
        
        //PRINTF("\nupdated pose: %f , %f , %f , %f , %f , %f ",pose[0],pose[1],pose[2],pose[3],pose[4],pose[5]);
        
        /*(if(check_termination== -1)
        {
            iter_counter=iter_counter-1; //since current iteration has not been used to update pose
            break; //exit iteration loop
        }*/
        //cout<<"iteration "<<iter_counter<<endl;
        /*if(check_termination>0.999f && check_termination <1.0f){
            cout<<"terminating !!!!!!!  "<<check_termination<<" check "<<0.99999<< "iter "<<iter_counter<<endl;
        iter_counter=util::MAX_ITER-1;}*/
    
        
         //to display current iteration residual
       DisplayIterationRes(prev_frame, workingPyramid.residual, "Iteration Residual", frame_num);
         
         //to display original image residual and original image
         if(display_initial_img_flag==0)
         {
             //display initial residual
             DisplayInitialRes(current_frame,prev_frame,"Initial Residual", frame_num);
             
             display_initial_img_flag=1; //set flag
         }
        
        //diplay original image
        if(display_orig_img_flag==0)
        {
            DisplayOriginalImg(workingPyramid.saveImg,prev_frame,"Original Image", frame_num);
            display_orig_img_flag=1; //set flag
        }
         
         //to display warped image
         DisplayWarpedImg(workingPyramid.warpedImage, prev_frame, "Warped Image", frame_num);
     
    
         //waitKey(0);
    
    
    } //exit iteration loop
    

    PRINTF("\nSummary: For previous frameId: %d, current frameId: %d ", prev_frame->frameId, current_frame->frameId);
    PRINTF("\nAt Pyramid Level: %d , Max Iteration: %d ",level_counter,iter_counter+1);
    
    /*if (myfile->is_open())
           *myfile<<"\npyramid level: "<<level_counter<<" , max iteration: "<<iter_counter+1; */
    
} //exit pyramid loop

    
//write pose to file
/*
if (myfile->is_open())
{
    *myfile<<"\n"<<pose[0]<<" , "<<pose[1]<<" , "<<pose[2]<<" , "<<pose[3]<<" , "<<pose[4]<<" , "<<pose[5]<<"\n";
}
*/

    PRINTF("\nUpdated pose: %f ,%f , %f , %f , %f , %f ",pose[0],pose[1],pose[2],pose[3],pose[4],pose[5]);
    
       
    //calculate new pose Wrt origin for the current image
    current_frame->calculatePoseWrtOrigin(prev_frame,pose);
    current_frame->calculatePoseWrtWorld(prev_frame,pose);
    
    if (myfile->is_open())
    {
        *myfile<<"\n"<<current_frame->poseWrtWorld[0]<<" , "<<current_frame->poseWrtWorld[1]<<" , "<<current_frame->poseWrtWorld[2]<<" , "<<current_frame->poseWrtWorld[3]<<" , "<<current_frame->poseWrtWorld[4]<<" , "<<current_frame->poseWrtWorld[5]<<" , "<<prev_frame->rescaleFactor;
    }

    //cout<<"\npose: "<<pose[0]<<" , "<<pose[1]<<" , "<<pose[2]<<" , "<<pose[3]<<" , "<<pose[4]<<" , "<<pose[5];

    PRINTF("\nExiting frame loop..");
    vector<float> posevec(pose, pose+6);
    
    return posevec;
    
}

