 
#include <cstdio>
#include<ctime>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
#include "DepthPropagation.h"


#include "DisplayFunc.h"
#include "ImageFunc.h"
#include "ExternVariable.h"
#include "Frame.h"

using namespace std;
using namespace cv;

ofstream my_file;

int main(int argc, char *argv[])
{
    clock_t start, end;
    my_file.open("/Users/himanshuaggarwal/Desktop/poses.txt");

    if (my_file.is_open())
        cout<<"yppppp";
    
    vector<frame*> frameptr_vector;// vector of pointers to each frame currently in memory
    //*******VIDEO INITIALIZATIONS*******//
    
    //capture BGR and depth videos from file
    //VideoCapture bgr_capture("/Users/himaniarora/Desktop/dataset/rgbd_dataset_freiburg2_xyz/rgb/%06d.png");// open the BGR video sequence file
    VideoCapture bgr_capture("/Users/himanshuaggarwal/Downloads/IMG_1474.MP4");// open the BGR video sequence file

    VideoCapture depth_capture("/Users/himanshuaggarwal/Desktop/Xcode_projects/rgbd_vids/depth.avi"); // open the associated depth map file
    
    //check if successful
    if (!bgr_capture.isOpened()) // check if we succeeded with BGR
        return -1;
    if (!depth_capture.isOpened()) // check if we succeeded with depth
        return -1;
    
    //*******PARAMETER INITIALIZATIONS*******//
    
    //get no of frames in video , no of rows and no of columns
    float no_of_frames = depth_capture.get(CV_CAP_PROP_FRAME_COUNT); //get the number of frames in depth
    
    vector<float>pose_vec;
    
    printf("\nNo of frames:%f",no_of_frames);
    
    frame* activeKeyFrame=NULL;
        
    depthMap* currentDepthMap=new depthMap();
    
    //*******ENTER FRAME LOOP*******//
    
    for (int frame_counter = 1; frame_counter <= 3288; frame_counter++)
    {
        start = clock();
        printf("\n\n\n\n NEW FRAME ID: %d \n",frame_counter);
        //cout<<"\n\n\nframe id  d: "<<frame_counter;
        
        //captures frame, converts to grayscale and forms pyramid
   
        //frameptr_vector.push_back(new frame(bgr_capture, depth_capture));
        
        frameptr_vector.push_back(new frame(bgr_capture));
        //cout<<"\nFront frame id: "<<frameptr_vector.front()->frameId<<endl;
        
        // to bootstrap the first image in the sequence
        if(frame_counter==1)
        {
            
           activeKeyFrame=frameptr_vector.back();
           currentDepthMap->formDepthMap(frameptr_vector.back());
           currentDepthMap->updateDepthImage();
           continue;
        }
                
        pose_vec=GetImagePoseEstimate( activeKeyFrame, frameptr_vector.back(), frame_counter, &my_file, currentDepthMap);

        
        float weighted_pose_for_keyframe_switching = (abs(pose_vec[0]*util::weight[0])+abs(pose_vec[1]*util::weight[1])+abs(pose_vec[2]*util::weight[2])+abs(pose_vec[3]*util::weight[3])+abs(pose_vec[4]*util::weight[4])+abs(pose_vec[5]*util::weight[5]));
        
        //cout<<"\nweighted pose "<<weighted_pose_for_keyframe_switching  /1000.0f<<endl;
        

        //logging the pose to file
        /*
        if (my_file.is_open())
        {
            cout<<"\nHIIIII";
           // my_file<<"\n"<<pose_vec[0]<<" , "<<pose_vec[1]<<" , "<<pose_vec[2]<<" , "<<pose_vec[3]<<" , "<<pose_vec[4]<<" , "<<pose_vec[5];
            
            
            my_file<<"\nHIiiiii"; 
        }
         */
        
        
        //updating pointer to vectors of frames pointers for the current image
        PRINTF("\n\n\nDEPTH MAP:");
        
       // waitKey(0);
        
    
        
     
        currentDepthMap->formDepthMap(frameptr_vector.back());
        
        //// Checking for keyframe propagation
       if((weighted_pose_for_keyframe_switching/1000.f)>util::switchKeyframeThreshold)
        //if(frame_counter%4==0)
        {
            printf("\nSwitching to new keyframe with id: %d",frameptr_vector.back()->frameId);
            currentDepthMap->finaliseKeyframe();
            currentDepthMap->createKeyFrame(frameptr_vector.back());
            activeKeyFrame=frameptr_vector.back();
            int size=frameptr_vector.size();
            for (int i = 0; i < size-1; ++i) {
                delete frameptr_vector  [i]; // Calls ~object and deallocates *tmp[i]
            }
            frameptr_vector.erase(frameptr_vector.begin(),frameptr_vector.begin()+size-1);
            cout<<"\n\nNEW KF!!!";
            continue;
        }
        currentDepthMap->updateKeyFrame();
        currentDepthMap->observeDepthRow();
        currentDepthMap->doRegularization();
        currentDepthMap->updateDepthImage();
        
        
        //cout<<currentDepthMap->keyFrame->depth<<endl;

        
    
        
        //currentDepthMap
        //DisplayOriginalImg(currentDepthMap->currentFrame->depth,currentDepthMap->currentFrame,"Original Depth", frame_counter);*/
        
        end = clock();
        // if (my_file.is_open())
        // {
        //    my_file<<" , "<<float( end - start) / CLOCKS_PER_SEC<<endl;
        // }
        
        
        cout <<"\nTime: "<<float( end - start) / CLOCKS_PER_SEC<<endl;
        
        //waitKey(0);
        
    } //exit frame loop
    
    my_file.close();
    
    
    return 0;
} //exit main










