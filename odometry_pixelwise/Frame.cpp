
//
//Frame.cpp
//

#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>

#include "Frame.h"
#include <cstdio>
#include<ctime>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include<complex>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
#include "ExternVariable.h"
#include "DisplayFunc.h"
#include "UserDefinedFunc.h"
#include "EigenInitialization.h"

//using namespace cv;

static int numberOfInstances;
using namespace std;
using namespace cv;

frame::frame() //default constructor
{
    PRINTF("\nDeafult Constructor for Frame Id: %d", frameId)
}


frame::frame(VideoCapture vidcap_rgb)
{
    frameId=++numberOfInstances; //frame iId allocated, starts from frameId = 1
    
    
    PRINTF("\nConstructing Frame with Frame ID: %d", frameId);
    
    deptharrfirstcall=false;
    deptharrpyrfirstcall=false;
    

    /*Mat big_image;
    vidcap_rgb>>big_image;
    cvtColor(big_image, big_image, CV_BGR2GRAY);
    pyrDown(big_image, image);*/

    

    vidcap_rgb >>image;
    imshow("dee", image);
    waitKey(0);
   cvtColor(image, image, CV_BGR2GRAY);
 

   
    
    /*
    if (frameId !=1)
    {
    for(int i=1; i<5; i++)
    {vidcap_rgb >> image;
        cout<<"\nHIIIII";
    }// get a new frame from bgr
    } */ 
    
    // ConvertImageToGray(image);
    
    
    width=image.cols;
    height=image.rows;
    
    printf("\nSIZE rows %d, cols %d", height, width);
    pyrLevel=0;
    
    currentCols=width;
    currentRows=height; 
    poseWrtOrigin[0]=0.0f; //initialize to 0 
    poseWrtOrigin[1]=0.0f;
    poseWrtOrigin[2]=0.0f;
    poseWrtOrigin[3]=0.0f;
    poseWrtOrigin[4]=0.0f;
    poseWrtOrigin[5]=0.0f;
    
    
    poseWrtWorld[0]=0.0f;
    poseWrtWorld[1]=0.0f;
    poseWrtWorld[2]=0.0f;
    poseWrtWorld[3]=0.0f;
    poseWrtWorld[4]=0.0f;
    poseWrtWorld[5]=0.0f;
    
    rescaleFactor=1.0f;
    
    //make image pyramids
    constructImagePyramids();
    
    //parentKeyframeId=frameId;
    
    calculateGradient();
    buildMaxGradients();
    
    depth=Mat::zeros(height,width,CV_32FC1); //initializing depth map to 0
    
    depth_pyramid[0]=Mat::zeros(height, width, CV_32FC1);
    depth_pyramid[1]=Mat::zeros(height>>1, width>>1, CV_32FC1);
    depth_pyramid[2]=Mat::zeros(height>>2, width>>2, CV_32FC1);
    depth_pyramid[3]=Mat::zeros(height>>3, width>>3, CV_32FC1);
    depth_pyramid[4]=Mat::zeros(height>>4, width>>4, CV_32FC1);
    
   
}


frame::frame(VideoCapture vidcap_rgb,VideoCapture vidcap_depth)
{
    
    
    frameId=++numberOfInstances;
    
    vidcap_rgb >> image; // get a new frame from bgr
    
    // ConvertImageToGray(image);
    cvtColor(image, image, CV_BGR2GRAY);
    
    Mat depth2= Mat::zeros(util::ORIG_ROWS, util::ORIG_COLS, CV_32FC1);
    
    vidcap_depth >> depth2; // get a new frame from bgr
    
    // ConvertImageToGray(image);
    cvtColor(depth2, depth2, CV_BGR2GRAY);
    
    depth2.convertTo(depth, CV_32FC1);
    
    depth=depth/20.0f;
    
    width=image.cols;
    height=image.rows;
    
    pyrLevel=0;
    
    
    currentCols=width;
    currentRows=height;
    poseWrtOrigin[0]=0.0f; //initialize to 0
    poseWrtOrigin[1]=0.0f;
    poseWrtOrigin[2]=0.0f;
    poseWrtOrigin[3]=0.0f;
    poseWrtOrigin[4]=0.0f;
    poseWrtOrigin[5]=0.0f;
    
 
    //make image pyramids
    constructImagePyramids();
   // constructDepthPyramids();
    
    
    calculateGradient();
    buildMaxGradients();

    
    
}




void frame::constructImagePyramids()
{   PRINTF("\nConstructing Image Pyramids for Frame Id: %d", frameId);

    image_pyramid[0] = image.clone();
    
    pyrDown(image_pyramid[0], image_pyramid[1]);
    
    pyrDown(image_pyramid[1], image_pyramid[2]);
    
    pyrDown(image_pyramid[2], image_pyramid[3]);
    
    pyrDown(image_pyramid[3], image_pyramid[4]);

}

/*
void frame::constructDepthPyramids(){
    PRINTF("\nConstructing Depth Pyramids for Frame Id: %d", frameId);
    
    depth_pyramid[0] = depth.clone();
    
    pyrDown(depth_pyramid[0], depth_pyramid[1]);
    
    pyrDown(depth_pyramid[1], depth_pyramid[2]);
    
    pyrDown(depth_pyramid[2], depth_pyramid[3]);
    
    pyrDown(depth_pyramid[3], depth_pyramid[4]);
    
}
 */ 

/*
void frame::initializeDepth(bool isFirstFrame)
{

    depth=Mat::zeros(height,width,CV_32FC1);

}
*/


//void frame::initializeRandomly()
//{
//    PRINTF("\nInitializing Depth Randomly for Frame Id: %d", frameId);
    /*
     activeKeyFramelock = new_frame->getActiveLock();
     activeKeyFrame = new_frame;
     activeKeyFrameImageData = activeKeyFrame->image(0);
     activeKeyFrameIsReactivated = false;
     const float* maxGradients = new_frame->maxGradients();
     */
    
/*    float* gradx_ptr;
    float* grady_ptr;
    uchar* new_depth_ptr;
    
    for(int y=1;y<height-1;y++)
    {
        gradx_ptr=gradientx.ptr<float>(y);
        grady_ptr=gradienty.ptr<float>(y);
        new_depth_ptr=depth.ptr<uchar>(y);
        
        for(int x=1;x<width-1;x++)
        {
            if(max(gradx_ptr[x],grady_ptr[x]) > util::MIN_ABS_GRAD_CREATE)
            {
                float idepth = 0.5f + 1.0f * ((rand() % 100001) / 100000.0f);
                new_depth_ptr[x]=1/idepth; //depth
                //new_depth_ptr[x+1]=idepth; //inverse depth
                //new_depth_ptr[x+2]=util::VAR_RANDOM_INIT_INITIAL; //variance
            }
            
            else
            {
                new_depth_ptr[x]=0.0f;
            }
        }
    }
    
    
}*/

void frame::calculateGradient()
{
    PRINTF("\nConstructing Gradient function 2 for frame: %d and pyramid level: %d", frameId, pyrLevel);
    
    //float kernelarr = { -1, 0, 1 };
    /*
    Mat kernelx = (Mat_<float>(1, 3)<< -1.0f,0,1.0f);
    // Gradient X
    
    //Sobel(src, dst, ddepth=16S, dx=1, dy=0, ksize=1 (for no smoothing), scale=1,delta=0, borderType=BORDER_DEFAULT )∂
    filter2D(image_pyramid[pyrLevel], gradientx, CV_32FC1, kernelx); 
    
    //float kernelarr = { -1, 0, 1 };
    Mat kernely = (Mat_<float>(3,1)<< -1.0f,0,1.0f);
    //Gradient Y
    //Sobel(src, dst, ddepth=16S, dx=0, dy=1, ksize=1 (for no smoothing), scale=1,delta=0, borderType=BORDER_DEFAULT )∂
    filter2D(image_pyramid[pyrLevel], gradienty, CV_32FC1, kernely); 
     */
    
    gradientx=Mat::zeros(currentRows, currentCols, CV_32FC1);
    gradienty=Mat::zeros(currentRows, currentCols, CV_32FC1);

    
    uchar* img_ptr=image_pyramid[pyrLevel].ptr<uchar>(0);
    uchar* img_ptr_top=image_pyramid[pyrLevel].ptr<uchar>(0);
    uchar* img_ptr_bottom=image_pyramid[pyrLevel].ptr<uchar>(0);

    float* gradx_ptr=gradientx.ptr<float>(0);
    float* grady_ptr=gradienty.ptr<float>(0);
    
    
    int y,x;
    
    //CASE 1
    
    for(y=1; y<currentRows-1; y++) // for internal elements with border size =1
    {
        img_ptr=image_pyramid[pyrLevel].ptr<uchar>(y);
        gradx_ptr=gradientx.ptr<float>(y);
        grady_ptr=gradienty.ptr<float>(y);
        img_ptr_top=image_pyramid[pyrLevel].ptr<uchar>(y-1);
        img_ptr_bottom=image_pyramid[pyrLevel].ptr<uchar>(y+1);

        for(x=1; x<currentCols-1; x++)
        {
            gradx_ptr[x]=0.5f*(float(img_ptr[x+1])-float(img_ptr[x-1]));
            grady_ptr[x]=0.5f*(float(img_ptr_bottom[x])-float(img_ptr_top[x]));
        }
    }
        
        //CASE 2
        y=0; //top row
        img_ptr=image_pyramid[pyrLevel].ptr<uchar>(y);
        gradx_ptr=gradientx.ptr<float>(y);
        grady_ptr=gradienty.ptr<float>(y);
        img_ptr_bottom=image_pyramid[pyrLevel].ptr<uchar>(y+1);
    
        x=0; //left top corner element
        gradx_ptr[x]=(float(img_ptr[x+1])-float(img_ptr[x]));//
        grady_ptr[x]=(float(img_ptr_bottom[x])-float(img_ptr[x]));//

        for(x=1; x<currentCols-1; x++) //top row , starting from 2nd element upto 2nd last
        {   gradx_ptr[x]=0.5f*(float(img_ptr[x+1])-float(img_ptr[x-1]));
            grady_ptr[x]=(float(img_ptr_bottom[x])-float(img_ptr[x]));//
        }
        
        x=currentCols-1; //right top corner element
        gradx_ptr[x]=(float(img_ptr[x])-float(img_ptr[x-1]));//
        grady_ptr[x]=(float(img_ptr_bottom[x])-float(img_ptr[x]));//
        
        //CASE 3
        y=currentRows-1; //bottom row
        img_ptr=image_pyramid[pyrLevel].ptr<uchar>(y);
        gradx_ptr=gradientx.ptr<float>(y);
        grady_ptr=gradienty.ptr<float>(y);
        img_ptr_top=image_pyramid[pyrLevel].ptr<uchar>(y-1);
        
        x=0; //left bottom corner element
        gradx_ptr[x]=(float(img_ptr[x+1])-float(img_ptr[x]));//
        grady_ptr[x]=(float(img_ptr[x])-float(img_ptr_top[x]));//
        
        for(x=1; x<currentCols-1; x++) //bottom row , starting from 2nd element upto 2nd last
        {   gradx_ptr[x]=0.5f*(float(img_ptr[x+1])-float(img_ptr[x-1]));
            grady_ptr[x]=(float(img_ptr[x])-float(img_ptr_top[x]));//
        }
        
        x=currentCols-1; //right bottom corner element
        gradx_ptr[x]=(float(img_ptr[x])-float(img_ptr[x-1]));//
        grady_ptr[x]=(float(img_ptr[x])-float(img_ptr_top[x]));//
        
        //CASE 4
        
        for(y=1; y<currentRows-1;y++) //left border and right border, starting from 2nd element upto 2nd last
        {
            img_ptr=image_pyramid[pyrLevel].ptr<uchar>(y);
            
            gradx_ptr=gradientx.ptr<float>(y);
            grady_ptr=gradienty.ptr<float>(y);
            
            img_ptr_top=image_pyramid[pyrLevel].ptr<uchar>(y-1);
            img_ptr_bottom=image_pyramid[pyrLevel].ptr<uchar>(y+1);
            
            x=0;    //left border
            gradx_ptr[x]=(float(img_ptr[x+1])-float(img_ptr[x]));//
            grady_ptr[x]=0.5f*(float(img_ptr_bottom[x])-float(img_ptr_top[x]));
            
            x=currentCols-1; //right border
            gradx_ptr[x]=(float(img_ptr[x])-float(img_ptr[x-1]));//
            grady_ptr[x]=0.5f*(float(img_ptr_bottom[x])-float(img_ptr_top[x]));
            
            
            
        }
    
  //  printf("\nlevel %d, currentRows %d, currentCols %d", pyrLevel, currentRows, currentCols);
   // cout<<"GRAD x \n"<<gradientx;
   // cout<<"\n\n\n\nGRAD y\n"<<gradienty;
    //waitKey(0);
    
}

void frame::getImgSize(int& row, int& col,int level)
{
    PRINTF("\nCalculating Image Size for Frame Id: %d and Pyramid Level: %d", frameId, level);
    row=height/pow(2, level);
    col=width/pow(2, level);

}

void frame::calculateNonZeroDepthPts()
{
    PRINTF("\nCalculating Mask and Non-Zero Depth Points for Frame Id: %d", frameId);
    mask=depth_pyramid[pyrLevel]>0.0f;
    no_nonZeroDepthPts=countNonZero(mask);
    //cout<<"\nNON ZERO: "<<no_nonZeroDepthPts;
}

void frame::initializePose()
{
    PRINTF("\nInitializing Pose Wrt Origin for Frame Id: %d", frameId);
    poseWrtOrigin[0]=0.0f;
    poseWrtOrigin[1]=0.0f;
    poseWrtOrigin[2]=0.0f;
    poseWrtOrigin[3]=0.0f;
    poseWrtOrigin[4]=0.0f;
    poseWrtOrigin[5]=0.0f;

}

void frame::updationOnPyrChange(int level,bool isPrevious)
{
    PRINTF("\nUpdating parameters on Pyramid change for Frame Id: %d, level: %d", frameId, level);
    pyrLevel=level;
    
    currentRows=height/pow(2, level);
    currentCols=width/pow(2, level);

    if(isPrevious)calculateNonZeroDepthPts(); //calculating new mask
    calculateGradient();

}

void frame::calculatePoseWrtOrigin(frame *prev_image, float *poseChangeWrtPrevframe)
{
    //PRINTF("\nCalculating Pose Wrt Origin for Frame Id: %d using pose change Wrt Prev Frame with Frame Id: %d", frameId, prev_image->frameId);
    //PRINTF("\nother_frame->poseWrtOrigin: %f ", prev_image->poseWrtOrigin[0] );
    
     poseWrtOrigin[0]=prev_image->poseWrtOrigin[0]+poseChangeWrtPrevframe[0];
     poseWrtOrigin[1]=prev_image->poseWrtOrigin[1]+poseChangeWrtPrevframe[1];
     poseWrtOrigin[2]=prev_image->poseWrtOrigin[2]+poseChangeWrtPrevframe[2];
     poseWrtOrigin[3]=prev_image->poseWrtOrigin[3]+poseChangeWrtPrevframe[3];
     poseWrtOrigin[4]=prev_image->poseWrtOrigin[4]+poseChangeWrtPrevframe[4];
     poseWrtOrigin[5]=prev_image->poseWrtOrigin[5]+poseChangeWrtPrevframe[5];
    //PRINTF("\ncurrent_frame->poseWrtOrigin: %f ", poseWrtOrigin[0]);
    return;


}



void frame::calculatePoseWrtWorld(frame *prev_image, float *poseChangeWrtPrevframe)
{
    //PRINTF("\nCalculating Pose Wrt Origin for Frame Id: %d using pose change Wrt Prev Frame with Frame Id: %d", frameId, prev_image->frameId);
    //PRINTF("\nother_frame->poseWrtOrigin: %f ", prev_image->poseWrtOrigin[0] );
    
    poseWrtWorld[0]=prev_image->poseWrtWorld[0]+poseChangeWrtPrevframe[0];
    poseWrtWorld[1]=prev_image->poseWrtWorld[1]+poseChangeWrtPrevframe[1];
    poseWrtWorld[2]=prev_image->poseWrtWorld[2]+poseChangeWrtPrevframe[2];
    poseWrtWorld[3]=prev_image->poseWrtWorld[3]+poseChangeWrtPrevframe[3];
    poseWrtWorld[4]=prev_image->poseWrtWorld[4]+poseChangeWrtPrevframe[4];
    poseWrtWorld[5]=prev_image->poseWrtWorld[5]+poseChangeWrtPrevframe[5];
    //PRINTF("\ncurrent_frame->poseWrtOrigin: %f ", poseWrtOrigin[0]);
    return;
    
    
}

float frame::getInterpolatedElement(float x1, float y1)
{   //PRINTF("\nCalulating Interpolated Image Intensity for frame Id: %d and points(%f,%f) ", frameId, x1, y1 );
    
    //initialize pointers
    uchar* img_ptr0; //pointer0 to access original image
    uchar* img_ptr1; //pointer1 to acess original image
    
    //*******DECLARE VARIABLES*******//
    
    float yx[2]; //to store warped points for current itertion
    float wt[2]; //to store weight
    float y,x;
    uchar pixVal1, pixVal2; //to store intensity value
    float interTop, interBtm;
    
    int nCols=currentCols-1; //maximum valid x coordinate
    int nRows=currentRows-1; //maximum valid y coordinate
    

        yx[0]=y1; //store current warped point y
        yx[1]=x1; //store warped point x
        
        wt[0]=yx[0]-floor(yx[0]); //weight for y
        wt[1]=yx[1]-floor(yx[1]); //weight for x
        
        //Case 1
        y=floor(yx[0]); //floor of y
        x=floor(yx[1]); //floor of x
    
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
            pixVal1=0; //if outside boundary pixel value is 0
        else
        {
            img_ptr0=image_pyramid[pyrLevel].ptr<uchar>(y); //initialize image pointer0 to row floor(y)
            pixVal1=img_ptr0[int(x)]; //move pointer to get value at pixel (floor(x), floor(y))
        }
        
        x=yx[1]; //warped point x
        
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
            pixVal2=0; //if outside boundary pixel value is 0
        else
        {
            img_ptr0=image_pyramid[pyrLevel].ptr<uchar>(y); //initialize image pointer0 to row floor(y)
            pixVal2=img_ptr0[int(ceil(x))]; //move pointer to get value at pixel (ceil(x), floor(y))
        }
        
        interTop=((1-wt[1])*pixVal1)+(wt[1]*pixVal2);
        
        //Case 2
        y=yx[0]; //warped point y
        
        x=floor(yx[1]); //floor of x
        
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
            pixVal1=0; //if outside boundary pixel value is 0
        else
        {
            img_ptr1=image_pyramid[pyrLevel].ptr<uchar>(ceil(y)); //initialize image pointer1 to row ceil(y)
            pixVal1=img_ptr1[int(x)]; //move pointer to get value at pixel (floor(x), ceil(y))
        }
        
        x=yx[1]; //warped point x
        
        if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
            pixVal2=0; //if outside boundary pixel value is 0
        else
        {
            img_ptr1=image_pyramid[pyrLevel].ptr<uchar>(ceil(y)); //initialize image pointer1 to row ceil(y)
            pixVal2=img_ptr1[int(ceil(x))]; //move pointer to get value at pixel (ceil(x), ceil(y))
        }
        
        interBtm=((1-wt[1])*pixVal1)+(wt[1]*pixVal2);
        
        float warpedvalue=((1-wt[0])*interTop)+(wt[0]*interBtm); //calculate interpolated value to get warped image intensity
    
    return warpedvalue;
    
}

float frame::getInterpolatedElement(float x1, float y1,String s)
{
    //PRINTF("\nCalculating Interpolated Gradient(x or y) Intensity for Frame Id: %d and points(%f,%f) ", frameId, x1, y1);
    //initialize pointers
    float* img_ptr0; //pointer0 to access original image
    float* img_ptr1; //pointer1 to acess original image
    
    Mat image1;
    if (s=="gradx") {
       image1=gradientx;
    }
    if (s=="grady") {
    image1=gradienty;
    }
       //*******DECLARE VARIABLES*******//
    
    //cout<<"\n\nWARPED POINTS   "<<warpedpoint;
  
    float yx[2]; //to store warped points for current itertion
    float wt[2]; //to store weight
    float y,x;
    float pixVal1, pixVal2; //to store intensity value
    float interTop, interBtm;
    
    int nCols=currentCols-1; //maximum valid x coordinate
    int nRows=currentRows-1; //maximum valid y coordinate
    
    
    yx[0]=y1; //store current warped point y
    yx[1]=x1; //store warped point x
    
    wt[0]=yx[0]-floor(yx[0]); //weight for y
    wt[1]=yx[1]-floor(yx[1]); //weight for x
    
    //Case 1
    y=floor(yx[0]); //floor of y
    x=floor(yx[1]); //floor of x
    
    //cout<<"\n\n"<<int(x);
    if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        pixVal1=0; //if outside boundary pixel value is 0
    else
    {
        img_ptr0=image1.ptr<float>(y); //initialize image pointer0 to row floor(y)
        pixVal1=img_ptr0[int(x)]; //move pointer to get value at pixel (floor(x), floor(y))
    }
    
    x=yx[1]; //warped point x
    
    if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        pixVal2=0; //if outside boundary pixel value is 0
    else
    {
        img_ptr0=image1.ptr<float>(y); //initialize image pointer0 to row floor(y)
        pixVal2=img_ptr0[int(ceil(x))]; //move pointer to get value at pixel (ceil(x), floor(y))
    }
    
    interTop=((1-wt[1])*pixVal1)+(wt[1]*pixVal2);
    
    
    //Case 2
    y=yx[0]; //warped point y
    
    x=floor(yx[1]); //floor of x
    
    if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        pixVal1=0; //if outside boundary pixel value is 0
    else
    {
        img_ptr1=image1.ptr<float>(ceil(y)); //initialize image pointer1 to row ceil(y)
        pixVal1=img_ptr1[int(x)]; //move pointer to get value at pixel (floor(x), ceil(y))
    }
    
    x=yx[1]; //warped point x
    
    if ((x<0)||(x>nCols)||(y<0)||(y>nRows))
        pixVal2=0; //if outside boundary pixel value is 0
    else
    {
        img_ptr1=image1.ptr<float>(ceil(y)); //initialize image pointer1 to row ceil(y)
        pixVal2=img_ptr1[int(ceil(x))]; //move pointer to get value at pixel (ceil(x), ceil(y))
    }
    
    interBtm=((1-wt[1])*pixVal1)+(wt[1]*pixVal2);
    
    float warpedvalue=((1-wt[0])*interTop)+(wt[0]*interBtm); //calculate interpolated value to get warped image intensity
    
    return warpedvalue;
    
    
}

/* DONT USE THIS!!!
void frame::calculateSE3poseThisWrtOther(frame *other_frame) //other to this
{
    PRINTF("\nHIIIIIIICalculating SE3 pose for This Frame with Id: %d Wrt to Other Frame with Id: %d", frameId, other_frame->frameId);
    float poseWrtOther[6];
    poseWrtOther[0]=poseWrtOrigin[0]-other_frame->poseWrtOrigin[0];
    poseWrtOther[1]=poseWrtOrigin[1]-other_frame->poseWrtOrigin[1];
    poseWrtOther[2]=poseWrtOrigin[2]-other_frame->poseWrtOrigin[2];
    poseWrtOther[3]=poseWrtOrigin[3]-other_frame->poseWrtOrigin[3];
    poseWrtOther[4]=poseWrtOrigin[4]-other_frame->poseWrtOrigin[4];
    poseWrtOther[5]=poseWrtOrigin[5]-other_frame->poseWrtOrigin[5];
    
    Mat SE3pose= CalculateTransformationMatrix(poseWrtOther);
    
    Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> se3_Eigen(SE3pose.ptr<float>(), SE3pose.rows, SE3pose.cols);
    
    //updating pose matrices
    SE3poseThisWrtOther=se3_Eigen;
    SE3poseThisWrtOther_r=SE3poseThisWrtOther.block(0, 0, 3, 3);
    SE3poseThisWrtOther_t=SE3poseThisWrtOther.block(0, 3, 3, 1);
 
    SE3poseOtherWrtThis=SE3poseThisWrtOther.inverse();
    SE3poseOtherWrtThis_r=SE3poseOtherWrtThis.block(0, 0, 3, 3);
    SE3poseOtherWrtThis_t=SE3poseOtherWrtThis.block(0, 3, 3, 1);
    
    K_SE3poseThisWrtOther_r=util::K_Eigen*SE3poseThisWrtOther_r;
    K_SE3poseThisWrtOther_t=util::K_Eigen*SE3poseThisWrtOther_t;
    
    K_SE3poseOtherWrtThis_r=util::K_Eigen*SE3poseOtherWrtThis_r;
    K_SE3poseOtherWrtThis_t=util::K_Eigen*SE3poseOtherWrtThis_t;
    
    PRINTF("\n!!!!!SE3poseThisWrtOther: %f ", SE3poseThisWrtOther(0,0));
}
*/


//USE THIS!!! 
void frame::calculateSE3poseOtherWrtThis(frame *other_frame) // this to other 
{
    PRINTF("\nCalculating SE3 pose for This Frame with Id: %d Wrt to Other Frame with Id: %d", frameId, other_frame->frameId);
    float poseWrtThis[6];
    
    poseWrtThis[0]=other_frame->poseWrtOrigin[0]-poseWrtOrigin[0]; //other wrt this
    poseWrtThis[1]=other_frame->poseWrtOrigin[1]-poseWrtOrigin[1];
    poseWrtThis[2]=other_frame->poseWrtOrigin[2]-poseWrtOrigin[2];
    poseWrtThis[3]=other_frame->poseWrtOrigin[3]-poseWrtOrigin[3];
    poseWrtThis[4]=other_frame->poseWrtOrigin[4]-poseWrtOrigin[4];
    poseWrtThis[5]=other_frame->poseWrtOrigin[5]-poseWrtOrigin[5];
    
    /*
    PRINTF("\ncurrent_frame->poseWrtOrigin: %f ", poseWrtOrigin[0]);
    PRINTF("\ncurrent_frame->poseWrtOrigin: %f ", poseWrtOrigin[1]);
    PRINTF("\ncurrent_frame->poseWrtOrigin: %f ", poseWrtOrigin[2]);
    PRINTF("\ncurrent_frame->poseWrtOrigin: %f ", poseWrtOrigin[3]);
    PRINTF("\ncurrent_frame->poseWrtOrigin: %f ", poseWrtOrigin[4]);
    PRINTF("\ncurrent_frame->poseWrtOrigin: %f ", poseWrtOrigin[5]); */
    
    //Mat SE3pose= CalculateTransformationMatrix(poseWrtThis); //other wrt this
    
    
    //Create matrix in OpenCV
    Mat se3=(Mat_<float>(4, 4) << 0,-poseWrtThis[2],poseWrtThis[1],poseWrtThis[3], poseWrtThis[2],0,-poseWrtThis[0],poseWrtThis[4], -poseWrtThis[1],poseWrtThis[0],0,poseWrtThis[5],0,0,0,0);
    
    // Map the OpenCV matrix with Eigen:
    Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> se3_Eigen(se3.ptr<float>(), se3.rows, se3.cols);
    
    // Take exp in Eigen and store in new Eigen matrix
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> SE3_Eigen = se3_Eigen.exp(); //4x4 pose of Other wrt This (eigen)
    
    // create an OpenCV Mat header for the Eigen data:
    Mat SE3(4, 4, CV_32FC1, SE3_Eigen.data()); //4x4 pose of Other wrt This (open cv)
    //cout<<"\n\nSE3: \n"<<SE3;
    //cout<<"\nSE3_Eigen: \n"<<SE3_Eigen<<endl;
    
    //PRINTF("\n\nSE3_Eigen : %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f ", SE3_Eigen(0,0), SE3_Eigen(0,1), SE3_Eigen(0,2), SE3_Eigen(0,3), SE3_Eigen(1,0), SE3_Eigen(1,1), SE3_Eigen(1,2), SE3_Eigen(1,3) ,SE3_Eigen(2,0) , SE3_Eigen(2,1), SE3_Eigen(2,2), SE3_Eigen(2,3),SE3_Eigen(3,0), SE3_Eigen(3,1),SE3_Eigen(3,2), SE3_Eigen(3,3));
    
    
    //updating pose matrices
    SE3poseOtherWrtThis=SE3_Eigen;
    SE3poseOtherWrtThis_r=SE3poseOtherWrtThis.block(0, 0, 3, 3);
    SE3poseOtherWrtThis_t=SE3poseOtherWrtThis.block(0, 3, 3, 1);
    
    SE3poseThisWrtOther=SE3poseOtherWrtThis.inverse();
    SE3poseThisWrtOther_r=SE3poseThisWrtOther.block(0, 0, 3, 3);
    SE3poseThisWrtOther_t=SE3poseThisWrtOther.block(0, 3, 3, 1);

   // K_SE3poseThisWrtOther=util::K_Eigen*SE3poseThisWrtOther;
    K_SE3poseThisWrtOther_r=util::K_Eigen*SE3poseThisWrtOther_r;
    K_SE3poseThisWrtOther_t=util::K_Eigen*SE3poseThisWrtOther_t;
    
    
    //K_SE3poseOtherWrtThis=util::K_Eigen*SE3poseOtherWrtThis;
    K_SE3poseOtherWrtThis_r=util::K_Eigen*SE3poseOtherWrtThis_r;
    K_SE3poseOtherWrtThis_t=util::K_Eigen*SE3poseOtherWrtThis_t;
    
}





void frame::calculateSim3poseOtherWrtThis(float scale_factor)
{
    PRINTF("\nCalculating Sim3 pose for This Frame with Id: %d Wrt to Other Frame with recale factor: %f", frameId, scale_factor);
    //Mat scale_mat=(Mat_<float>(3,3)<<scale_factor, 0, 0, 0, scale_factor, 0, 0, 0, scale_factor);
    rescaleFactor=scale_factor; 
    
    Eigen::Matrix3f scale_mat;
    scale_mat<<scale_factor, 0, 0, 0, scale_factor, 0, 0, 0, scale_factor;
    
    SE3poseThisWrtOther_r=scale_mat*SE3poseThisWrtOther_r;    
    SE3poseOtherWrtThis_r=SE3poseThisWrtOther_r.inverse();
    
    K_SE3poseThisWrtOther_r=util::K_Eigen*SE3poseThisWrtOther_r;
    K_SE3poseOtherWrtThis_r=util::K_Eigen*SE3poseOtherWrtThis_r;
    
    //translation remains the same
    
    SE3poseThisWrtOther<<SE3poseThisWrtOther_r(0,0), SE3poseThisWrtOther_r(0,1), SE3poseThisWrtOther_r(0,2), SE3poseThisWrtOther_t(0,0),SE3poseThisWrtOther_r(1,0), SE3poseThisWrtOther_r(1,1), SE3poseThisWrtOther_r(1,2), SE3poseThisWrtOther_t(1,0), SE3poseThisWrtOther_r(2,0), SE3poseThisWrtOther_r(2,1), SE3poseThisWrtOther_r(2,2), SE3poseThisWrtOther_t(2,0), 0 , 0 , 0 , 1;
    
    SE3poseOtherWrtThis=SE3poseThisWrtOther.inverse();
    
   // K_SE3poseOtherWrtThis=util::K_Eigen*SE3poseOtherWrtThis;
   // K_SE3poseThisWrtOther=util::K_Eigen*SE3poseThisWrtOther;
    
}


void frame::buildMaxGradients()
{
    PRINTF("\nCalculating Max Gradients for frame Id: %d", frameId);
    // 1. write abs gradients in real data.
    
    
    maxAbsGradient=Mat::zeros(height,width,CV_32FC1);
    
    
    Mat sqrGradX;
    Mat sqrGradY;
    multiply(gradientx,gradientx,sqrGradX);
    multiply(gradienty,gradienty,sqrGradY);
    
    add(sqrGradX, sqrGradY, maxAbsGradient);
    sqrt(maxAbsGradient,maxAbsGradient);
    
    /*
    float* maxabs_grad_pt=maxAbsGradient.ptr<float>(0);
    
    float* gradx_ptr=gradientx.ptr<float>(0);
    
    float* grady_ptr=gradienty.ptr<float>(0);
    
    for(int y=0; y< util::ORIG_ROWS;y++)
    {
        maxabs_grad_pt=maxAbsGradient.ptr<float>(y);
        
        gradx_ptr=gradientx.ptr<float>(y);
        
        grady_ptr=gradienty.ptr<float>(y);
        
        for(int x=0;x<util::ORIG_COLS;x++)
        {
            maxabs_grad_pt[x]=(gradx_ptr[x]*gradx_ptr[x])+(grady_ptr[x]*grady_ptr[x]);
            maxabs_grad_pt[x]=sqrt(maxabs_grad_pt[x]);
        }
    }
    */

    Mat maxGradTemp=Mat::zeros(height,width,CV_32FC1);
    
    // 2. smear up/down direction into temp buffer
    float* maxgrad_centre_pt=maxAbsGradient.ptr<float>(0);
    float* maxgrad_up_pt=maxAbsGradient.ptr<float>(0);
    float* maxgrad_down_pt=maxAbsGradient.ptr<float>(0);
    float* maxgrad_t_pt=maxGradTemp.ptr<float>(0);
    
    for (int y=1;y<height-1; y++)
    {
        maxgrad_centre_pt=maxAbsGradient.ptr<float>(y);
        maxgrad_up_pt=maxAbsGradient.ptr<float>(y-1);
        maxgrad_down_pt=maxAbsGradient.ptr<float>(y+1);
        maxgrad_t_pt=maxGradTemp.ptr<float>(y);
        for (int x=0; x<width; x++)
        {
            float g1=max(maxgrad_centre_pt[x],maxgrad_up_pt[x]);
            maxgrad_t_pt[x]=max(g1,maxgrad_down_pt[x]);
        }

    }
    
    // 2. smear left/right direction into real data
    for (int y=1;y<height-1; y++)
    {
        maxgrad_centre_pt=maxAbsGradient.ptr<float>(y);
        
        maxgrad_t_pt=maxGradTemp.ptr<float>(y);
        for (int x=1; x<width-1; x++)
        {
            float g1=max(maxgrad_t_pt[x-1],maxgrad_t_pt[x]);
            maxgrad_centre_pt[x]=max(g1,maxgrad_t_pt[x+1]);
        }
        
    }
    
    //cout<<"\n\n\n"<<maxAbsGradient;

    /* // 2. smear up/down direction into temp buffer
    maxgrad_pt = data.maxGradients[level] + width+1;
    maxgrad_pt_max = data.maxGradients[level] + width*(height-1)-1;
    float* maxgrad_t_pt = maxGradTemp + width+1;
    for(;maxgrad_pt<maxgrad_pt_max; maxgrad_pt++, maxgrad_t_pt++)
    {
        float g1 = maxgrad_pt[-width];
        float g2 = maxgrad_pt[0];
        if(g1 < g2) g1 = g2;
        float g3 = maxgrad_pt[width];
        if(g1 < g3)
            *maxgrad_t_pt = g3;
        else
            *maxgrad_t_pt = g1;
    }
    
    float numMappablePixels = 0;
    // 2. smear left/right direction into real data
    maxgrad_pt = data.maxGradients[level] + width+1;
    maxgrad_pt_max = data.maxGradients[level] + width*(height-1)-1;
    maxgrad_t_pt = maxGradTemp + width+1;
    for(;maxgrad_pt<maxgrad_pt_max; maxgrad_pt++, maxgrad_t_pt++)
    {
        float g1 = maxgrad_t_pt[-1];
        float g2 = maxgrad_t_pt[0];
        if(g1 < g2) g1 = g2;
        float g3 = maxgrad_t_pt[1];
        if(g1 < g3)
        {
            *maxgrad_pt = g3;
            if(g3 >= MIN_ABS_GRAD_CREATE)
                numMappablePixels++;
        }
        else
        {
            *maxgrad_pt = g1;
            if(g1 >= MIN_ABS_GRAD_CREATE)
                numMappablePixels++;
        }
    }
    
    if(level==0)
        this->numMappablePixels = numMappablePixels;
    
    FrameMemory::getInstance().returnBuffer(maxGradTemp);
    
    data.maxGradientsValid[level] = true;*/
}

