#pragma once

#ifndef __odometry_code_3__UserDefinedFunc__
#define __odometry_code_3__UserDefinedFunc__

#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <unsupported/Eigen/MatrixFunctions>

#include <cstdio>
#include <cstdlib>
#include"opencv2/opencv.hpp"
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "ExternVariable.h"
#include "Frame.h"

using namespace std;
using namespace cv;


//form image mask
Mat FormImageMask(Mat depth_img);

//Find image gradient in a particular direction
void CalculateGradient(Mat Source,Mat& dstx, Mat& desty) ;

//returns steepest descent
Mat CalculateSteepestDescent( frame* image_frame);

//returns hessian
Mat CalculateHessianInverse(Mat steepest_desc);

//returns intrinsic focal parameters
inline vector<float> GetIntrinsic(int pyrlevel)
{
    //PRINTF("\nCalculating Intrinsic parameters for level: %d", pyrlevel);
    vector<float> resized_intrinsic(4);
    
    //calculating resized intrinsic parameters  fx, fy, cx, cy
    resized_intrinsic[0] = (util::ORIG_FX / pow(2, pyrlevel));
    resized_intrinsic[1] = (util::ORIG_FY / pow(2, pyrlevel));
    resized_intrinsic[2] = (util::ORIG_CX / pow(2, pyrlevel));
    resized_intrinsic[3] = (util::ORIG_CY / pow(2, pyrlevel));
    
    //storing in resized_intrinsic=[fx, fy, cx, cy];
    // Mat resized_intrinsic = (Mat_<float>(1,4)<<resized_fx, resized_fy, resized_cx, resized_cy);
    
    //return pointer to the first element
    
    return resized_intrinsic;
};


Mat CalculateTransformationMatrix(float *pose);

#endif /* defined(__odometry_code_3__UserDefinedFunc__) */
